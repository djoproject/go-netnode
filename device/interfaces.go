package device

import (
	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/utils/pool"
)

// L1Interface interface represents the required methods of a physical
// interface
type L1Interface interface {
	Open() error
	Close() error
	WaitClosing() error
	GetName() (string, error)
	PostDispatch(process.Object)
	Post(*pool.ReusableBuffer) error
}

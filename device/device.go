package device

import (
	"fmt"

	"gitlab.com/go-netnode/process/l2/unknown"
	"gitlab.com/go-netnode/startup/device"
	"gitlab.com/go-netnode/utils/buffer"
	"gitlab.com/go-netnode/utils/physical/tap"
	"gitlab.com/go-netnode/utils/pool"
)

// Okay, so basic configuration has been moved to a config package
//
//	* TODO need to implement ipv6 process and icmpv6 process (ndp)
//
//	* TODO need a runtime config storage, everything that only exist at runtime
//		e.g. multicast ipv6 host and its mac, broadcast addres, etc.
//
//		TODO need a package name: runtime ? state ? memory ?
//
//	* TODO still needs a way to periodic sending that is not
//	compatible with the current process design
//
//		arp, ndp, lldp, etc.
//
//		Why is it not compatible ? because current process answer to a receive frame
//		and extract answer data from the data received.
//

// Device object represent a device that is going to print everything if sees
type Device struct {
	pool               *pool.Pool
	physicalInterfaces map[string]L1Interface
}

// GetBufferPool returns the pool of buffer used on this device.
func (d Device) GetBufferPool() *pool.Pool {
	return d.pool
}

// NewDevice function initialize a new Device object
func NewDevice(config *device.Config) (*Device, error) {
	// TODO checks
	//	at least one interface
	//  buffer count must be bigger than buffer per interface * interface count

	// Buffers
	pool, err := pool.NewPool(config.TotalBufferCount)
	if err != nil {
		return nil, err
	}

	biggestMTU, err := config.GetBiggestMTU()

	if err != nil {
		return nil, err
	}

	for i := 0; i < config.TotalBufferCount; i++ {
		buff, err := buffer.NewSimpleBuffer(int(biggestMTU))

		if err != nil {
			return nil, err
		}

		err = pool.AddBuffer(buff)

		if err != nil {
			return nil, err
		}
	}

	// device
	device := &Device{
		pool:               pool,
		physicalInterfaces: make(map[string]L1Interface),
	}

	// process
	// TODO need to define wanted process somewhere
	unknownProcess := &unknown.Process{}

	// interfaces
	for name, iface := range config.Interfaces {
		device.physicalInterfaces[name] = tap.NewInterface(
			config.InterfaceNameFormat,
			int(iface.MTU),
			unknownProcess,
			device,
			config.InterfaceBufferCount,
		)
	}

	return device, nil
}

// Start method runs a device
func (d Device) Start() error {
	// Let's roll !

	for _, iface := range d.physicalInterfaces {
		err := iface.Open()
		if err != nil {
			return fmt.Errorf("Error while opening tap interface: %w", err)
		}
	}

	return nil
}

// Stop method stops a device
func (d Device) Stop() error {
	for _, iface := range d.physicalInterfaces {
		err := iface.Close()
		if err != nil {
			return fmt.Errorf("Error while closing tap interface: %w", err)
		}

		err = iface.WaitClosing()
		if err != nil {
			return fmt.Errorf("Error while waiting tap interface to close: %w", err)
		}
	}
	return nil
}

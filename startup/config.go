package startup

// Config interface contains the methods needed to be a Config object
type Config interface {
	Clone() Config
	Validate(prefix string, list *[]string)
}

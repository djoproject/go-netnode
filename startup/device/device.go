package device

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/go-netnode/startup"
	"gitlab.com/go-netnode/startup/interfaces"
)

// Config struct represents a network device
type Config struct {
	InterfaceNameFormat  string                        `json:"interfaceNameFormat"`
	InterfaceBufferCount int                           `json:"interfaceBufferCount"`
	Interfaces           map[string]*interfaces.Config `json:"interfaces"`
	TotalBufferCount     int                           `json:"totalBufferCount"`
}

// NewConfig function allow to create a Config object.
func NewConfig(interfaceNameFormat string, interfaceBufferCount, totalBufferCount int) *Config {
	return &Config{
		InterfaceNameFormat:  interfaceNameFormat,
		InterfaceBufferCount: interfaceBufferCount,
		Interfaces:           make(map[string]*interfaces.Config),
		TotalBufferCount:     totalBufferCount,
	}
}

// GetBiggestMTU method returns the biggest MTU set on every existing interfaces
func (c Config) GetBiggestMTU() (uint16, error) {
	if len(c.Interfaces) == 0 {
		return 0, errors.New("no interface defined on this device")
	}

	var mtu uint16

	for _, v := range c.Interfaces {
		if v.MTU > mtu {
			mtu = v.MTU
		}
	}

	return mtu, nil
}

// Validate method allows to check if the device configuration is valid.
func (c Config) Validate(prefix string, issues *[]string) {
	if len(c.InterfaceNameFormat) == 0 {
		*issues = append(*issues, fmt.Sprintf("%s.InterfaceNameFormat: is empty", prefix))
	} else {
		if !strings.Contains(c.InterfaceNameFormat, "%d") {
			*issues = append(*issues, fmt.Sprintf("%s.InterfaceNameFormat: does not contains format flag '%%d'", prefix))
		}
	}

	if c.InterfaceBufferCount <= 0 {
		*issues = append(*issues, fmt.Sprintf("%s.InterfaceBufferCount: must be bigger than 0", prefix))
	}

	if c.TotalBufferCount <= 0 {
		*issues = append(*issues, fmt.Sprintf("%s.TotalBufferCount: must be bigger than 0", prefix))
	} else {
		if len(c.Interfaces)*c.InterfaceBufferCount > c.TotalBufferCount {
			*issues = append(*issues, fmt.Sprintf("%s.TotalBufferCount: must be bigger or equal than InterfaceBufferCount mutliple by the number of interfaces", prefix))
		}
	}

	if len(c.Interfaces) == 0 {
		*issues = append(*issues, fmt.Sprintf("%s.Interfaces is empty", prefix))
	}

	for name, iface := range c.Interfaces {
		iface.Validate(fmt.Sprintf("%s.Interfaces[%s].", prefix, name), issues)
	}
}

// Clone method allows to clone the device configuration
func (c Config) Clone() startup.Config {
	device := NewConfig(c.InterfaceNameFormat, c.InterfaceBufferCount, c.TotalBufferCount)

	for name, iface := range c.Interfaces {
		value := iface.Clone()
		ifaceClone, _ := value.(*interfaces.Config)
		device.Interfaces[name] = ifaceClone
	}

	return device
}

// Load method allows to retrieve a configuration from file
func (c *Config) Load(path string) error {
	jsonFile, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("Error while opening config file: %w", err)
	}
	defer jsonFile.Close()

	byteValue, err := ioutil.ReadAll(jsonFile)

	if err != nil {
		return fmt.Errorf("Error while reading config file: %w", err)
	}

	err = json.Unmarshal(byteValue, c)

	if err != nil {
		return fmt.Errorf("Error while parsing config file: %w", err)
	}

	return nil
}

// Save method allows to save a configuration to a file
func (c Config) Save(path string) error {
	file, err := json.MarshalIndent(c, "", "    ")

	if err != nil {
		return fmt.Errorf("Error while preparing config: %w", err)
	}

	err = ioutil.WriteFile(path, file, 0644)

	if err != nil {
		return fmt.Errorf("Error while writing config file: %w", err)
	}

	return nil
}

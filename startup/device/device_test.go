package device_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/startup/device"
	"gitlab.com/go-netnode/startup/diff"
	"gitlab.com/go-netnode/startup/interfaces"
)

func TestNewConfig(t *testing.T) {
	cfg := device.NewConfig("tun%d", 15, 30)
	assert.Equal(t, "tun%d", cfg.InterfaceNameFormat)
	assert.Equal(t, 15, cfg.InterfaceBufferCount)
	assert.Equal(t, 30, cfg.TotalBufferCount)
	assert.NotNil(t, cfg.Interfaces)
	assert.Empty(t, 0, len(cfg.Interfaces))
}

func TestGetBiggestMTUNoInterface(t *testing.T) {
	cfg := device.NewConfig("tun%d", 1, 1)
	mtu, err := cfg.GetBiggestMTU()
	assert.EqualError(t, err, "no interface defined on this device")
	assert.Equal(t, uint16(0), mtu)
}

func TestGetBiggestMTU(t *testing.T) {
	cfg := device.NewConfig("tun%d", 1, 1)
	cfg.Interfaces["tun0"] = interfaces.NewConfig(1400, interfaces.ConfigModeNone)
	cfg.Interfaces["tun1"] = interfaces.NewConfig(1500, interfaces.ConfigModeNone)
	mtu, err := cfg.GetBiggestMTU()
	assert.NoError(t, err)
	assert.Equal(t, uint16(1500), mtu)
}

func TestValidateWithoutInterface(t *testing.T) {
	tests := map[string]struct {
		interfaceNameFormat  string
		interfaceBufferCount int
		totalBufferCount     int
		issue                string
	}{
		"TestValidateEmptyInterfaceFormat": {
			interfaceNameFormat:  "",
			interfaceBufferCount: 1,
			totalBufferCount:     1,
			issue:                "device.InterfaceNameFormat: is empty",
		},
		"TestValidateInterfaceFormatMissingD": {
			interfaceNameFormat:  "tun",
			interfaceBufferCount: 1,
			totalBufferCount:     1,
			issue:                "device.InterfaceNameFormat: does not contains format flag '%d'",
		},
		"TestValidateNoInterface": {
			interfaceNameFormat:  "tun%d",
			interfaceBufferCount: 1,
			totalBufferCount:     1,
			issue:                "device.Interfaces is empty",
		},
		"TestValidateInterfaceBufferCount0": {
			interfaceNameFormat:  "tun%d",
			interfaceBufferCount: 0,
			totalBufferCount:     1,
			issue:                "device.InterfaceBufferCount: must be bigger than 0",
		},
		"TestValidateInterfaceTotalBufferCount0": {
			interfaceNameFormat:  "tun%d",
			interfaceBufferCount: 1,
			totalBufferCount:     0,
			issue:                "device.TotalBufferCount: must be bigger than 0",
		},
	}

	for testName, parameters := range tests {
		t.Run(testName, func(t *testing.T) {
			cfg := device.NewConfig(parameters.interfaceNameFormat, parameters.interfaceBufferCount, parameters.totalBufferCount)
			issues := make([]string, 0)
			cfg.Validate("device", &issues)
			require.NotEmpty(t, issues)
			assert.Equal(t, parameters.issue, issues[0])

		})
	}
}

func TestValidateWithInterface(t *testing.T) {
	tests := map[string]struct {
		interfaceNameFormat  string
		interfaceBufferCount int
		totalBufferCount     int
		issue                string
	}{
		"TestValidateInterfaceTotalBufferCountTooSmall": {
			interfaceNameFormat:  "tun%d",
			interfaceBufferCount: 1,
			totalBufferCount:     1,
			issue:                "device.TotalBufferCount: must be bigger or equal than InterfaceBufferCount mutliple by the number of interfaces",
		},
		"TestValidateOk": {
			interfaceNameFormat:  "tun%d",
			interfaceBufferCount: 1,
			totalBufferCount:     2,
			issue:                "",
		},
	}

	for testName, parameters := range tests {
		t.Run(testName, func(t *testing.T) {
			cfg := device.NewConfig(parameters.interfaceNameFormat, parameters.interfaceBufferCount, parameters.totalBufferCount)
			cfg.Interfaces["tun0"] = interfaces.NewConfig(1400, interfaces.ConfigModeNone)
			cfg.Interfaces["tun1"] = interfaces.NewConfig(1500, interfaces.ConfigModeNone)
			issues := make([]string, 0)
			cfg.Validate("device", &issues)
			if len(parameters.issue) == 0 {
				require.Empty(t, issues)
			} else {
				require.NotEmpty(t, issues)
				assert.Equal(t, parameters.issue, issues[0])
			}

		})
	}
}

func TestClone(t *testing.T) {
	cfg := device.NewConfig("tun%d", 1, 1)
	cfg.Interfaces["tun0"] = interfaces.NewConfig(1400, interfaces.ConfigModeNone)
	cpy := cfg.Clone()

	cfgDiff, err := diff.NewDiff(cfg, cpy)
	require.NoError(t, err)
	assert.True(t, cfgDiff.IsEmpty())
}

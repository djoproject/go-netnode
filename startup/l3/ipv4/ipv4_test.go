package ipv4_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/startup/diff"
	"gitlab.com/go-netnode/startup/l3/ipv4"
)

func TestConfig(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	assert.True(t, cfg.EnabledynamicLinkLocal)
	assert.True(t, cfg.EnableHostMulticast)
	assert.True(t, cfg.AllowGatewayMulticast)
	assert.True(t, cfg.EnableLimitedBroadcast)
	assert.Nil(t, cfg.PrimaryAddress)
	assert.Empty(t, cfg.SecondaryAddresses)
}

func TestValidateNoAddress(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	issues := make([]string, 0)
	cfg.Validate("ipv4", &issues)
	assert.Empty(t, issues)
}

func TestValidatePrimaryAddressOk(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.SetPrimaryAddress("192.168.1.1/24")

	issues := make([]string, 0)
	cfg.Validate("ipv4", &issues)
	assert.Empty(t, issues)
}

func TestUnsetPrimaryAddress(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.SetPrimaryAddress("192.168.1.1/24")
	cfg.UnsetPrimaryAddress()
	assert.Nil(t, cfg.PrimaryAddress)
}

func TestValidatePrimaryAddressInvalid(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.SetPrimaryAddress("toto")

	issues := make([]string, 0)
	cfg.Validate("ipv4", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ipv4.PrimaryAddres: invalid CIDR address: toto", issues[0])
}

func TestValidatePrimaryAddressNotAnIPv4(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.SetPrimaryAddress("fe80::1/64")

	issues := make([]string, 0)
	cfg.Validate("ipv4", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ipv4.PrimaryAddres: not an IPv4: fe80::1/64", issues[0])
}

func TestValidatePrimaryAddressIsNetId(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.SetPrimaryAddress("192.168.1.0/24")

	issues := make([]string, 0)
	cfg.Validate("ipv4", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ipv4.PrimaryAddres: network id not allowed: 192.168.1.0/24", issues[0])
}

func TestValidatePrimaryAddressIsBroadcast(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.SetPrimaryAddress("192.168.1.255/24")

	issues := make([]string, 0)
	cfg.Validate("ipv4", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ipv4.PrimaryAddres: broadcast address is not allowed: 192.168.1.255/24", issues[0])
}

func TestValidatePrimaryAddressIsNetIdWithSlash30(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.SetPrimaryAddress("192.168.1.0/30")

	issues := make([]string, 0)
	cfg.Validate("ipv4", &issues)
	require.Empty(t, issues)
}

func TestValidatePrimaryAddressIsBroadcastWithSlash30(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.SetPrimaryAddress("192.168.1.4/30")

	issues := make([]string, 0)
	cfg.Validate("ipv4", &issues)
	require.Empty(t, issues)
}

func TestSecondaryAddressSetButNotPrimary(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.AddSecondaryAddress("192.168.1.1/24")

	issues := make([]string, 0)
	cfg.Validate("ipv4", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ipv4.PrimaryAddress can not be nil if secondary addresses exist", issues[0])
}

func TestRemoveSecondaryAddress(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.AddSecondaryAddress("192.168.1.1/24")
	cfg.RemoveSecondaryAddress("192.168.1.1/24")
	assert.Empty(t, cfg.SecondaryAddresses)
}

func TestRemoveSecondaryAddressiNotInList(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.AddSecondaryAddress("192.168.1.1/24")
	cfg.RemoveSecondaryAddress("192.168.1.2/24")

	require.NotEmpty(t, cfg.SecondaryAddresses)
	assert.Len(t, cfg.SecondaryAddresses, 1)
	assert.Equal(t, "192.168.1.1/24", cfg.SecondaryAddresses[0])
}

func TestClone(t *testing.T) {
	cfg := ipv4.NewConfig(true, true, true, true)
	cfg.SetPrimaryAddress("192.168.1.1/24")
	cfg.AddSecondaryAddress("10.0.0.1/16")

	cpy := cfg.Clone()
	cfgDiff, err := diff.NewDiff(cfg, cpy)
	require.NoError(t, err)
	assert.True(t, cfgDiff.IsEmpty())
}

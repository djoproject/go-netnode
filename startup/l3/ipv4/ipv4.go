package ipv4

import (
	"encoding/binary"
	"fmt"
	"net"

	"gitlab.com/go-netnode/startup"
)

// https://stackoverflow.com/questions/36166791/how-to-get-broadcast-address-of-ipv4-net-ipnet
func getBroadcast(n *net.IPNet) net.IP {
	ip := make(net.IP, len(n.IP.To4()))
	netidUint32 := binary.BigEndian.Uint32(n.IP.To4())
	netmaskUint32 := binary.BigEndian.Uint32(net.IP(n.Mask).To4())
	binary.BigEndian.PutUint32(ip, netidUint32|^netmaskUint32)
	return ip
}

// Config object represents the ipv4 configuration of an interface
type Config struct {
	EnabledynamicLinkLocal bool     `json:"enable_dynamic_link_local"`
	EnableHostMulticast    bool     `json:"enable_host_multicast"`
	AllowGatewayMulticast  bool     `json:"enable_gateway_multicast"`
	EnableLimitedBroadcast bool     `json:"enable_limited_broadcast"`
	PrimaryAddress         *string  `json:"primary_address"`
	SecondaryAddresses     []string `json:"secondary_addresses"`
}

// NewConfig function creates an ipv4 Config object.
func NewConfig(linkLocal, host, gateway, limitedBroadcast bool) *Config {
	return &Config{
		EnabledynamicLinkLocal: linkLocal,
		EnableHostMulticast:    host,
		AllowGatewayMulticast:  gateway,
		EnableLimitedBroadcast: limitedBroadcast,
		SecondaryAddresses:     make([]string, 0),
	}
}

// SetPrimaryAddress method allows to set the primary address of the interface
func (c *Config) SetPrimaryAddress(address string) {
	c.PrimaryAddress = &address
}

// UnsetPrimaryAddress method allows to unset the primary address of the interface
func (c *Config) UnsetPrimaryAddress() {
	c.PrimaryAddress = nil
}

// AddSecondaryAddress method allows to add secondary address
func (c *Config) AddSecondaryAddress(newAddress string) {
	c.SecondaryAddresses = append(c.SecondaryAddresses, newAddress)
}

// RemoveSecondaryAddress method allows to remove secondary address
func (c *Config) RemoveSecondaryAddress(oldAddress string) {
	newList := make([]string, 0, len(c.SecondaryAddresses))
	for _, addr := range c.SecondaryAddresses {
		if addr == oldAddress {
			continue
		}

		newList = append(newList, addr)
	}

	c.SecondaryAddresses = newList
}

func checkAddress(address, prefix string, issues *[]string) {
	ip, network, err := net.ParseCIDR(address)

	if err != nil {
		*issues = append(*issues, fmt.Sprintf("%s: %s", prefix, err.Error()))
		return
	}

	if ip.To4() == nil {
		*issues = append(*issues, fmt.Sprintf("%s: not an IPv4: %s", prefix, address))
		return
	}

	ones, _ := network.Mask.Size()

	if ones >= 30 {
		return
	}

	if network.IP.Equal(ip) {
		*issues = append(*issues, fmt.Sprintf("%s: network id not allowed: %s", prefix, address))
		return
	}

	ipB := getBroadcast(network)
	if ip.Equal(ipB) {
		*issues = append(*issues, fmt.Sprintf("%s: broadcast address is not allowed: %s", prefix, address))
	}
}

// Validate method allows to check if the ipv4 configuration is valid.
func (c Config) Validate(prefix string, issues *[]string) {
	if len(c.SecondaryAddresses) > 0 && c.PrimaryAddress == nil {
		*issues = append(*issues, fmt.Sprintf("%s.PrimaryAddress can not be nil if secondary addresses exist", prefix))
	}

	if c.PrimaryAddress != nil {
		checkAddress(*c.PrimaryAddress, fmt.Sprintf("%s.PrimaryAddres", prefix), issues)
	}

	for index, address := range c.SecondaryAddresses {
		checkAddress(address, fmt.Sprintf("%s.SecondaryAddresses[%d]", prefix, index), issues)
	}
}

// Clone method allows to clone the ipv4 configuration
func (c Config) Clone() startup.Config {
	newConfig := NewConfig(
		c.EnabledynamicLinkLocal,
		c.EnableHostMulticast,
		c.AllowGatewayMulticast,
		c.EnableLimitedBroadcast,
	)

	if c.PrimaryAddress != nil {
		primary := *c.PrimaryAddress
		newConfig.PrimaryAddress = &primary
	}

	newConfig.SecondaryAddresses = append(newConfig.SecondaryAddresses, c.SecondaryAddresses...)

	return newConfig
}

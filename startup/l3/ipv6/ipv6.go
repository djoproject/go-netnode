package ipv6

import (
	"fmt"
	"net"

	"gitlab.com/go-netnode/startup"
)

// Config object represents the ipv6 configuration of an interface
type Config struct {
	Addresses              []string `json:"addresses"`
	Prefixes               []string `json:"prefixes"`
	LinkLocalEnabled       bool     `json:"linkLocalEnabled"`
	EUI64                  bool     `json:"eui64"`
	TemporaryGlobalUnicast bool     `json:"temporaryGlobalUnicast"`
}

// NewConfig function creates an IPv6 Config object
func NewConfig(linkLocalEnabled, EUI64, temporaryGlobalUnicast bool) *Config {
	config := &Config{
		Addresses:              make([]string, 0),
		Prefixes:               make([]string, 0),
		LinkLocalEnabled:       linkLocalEnabled,
		EUI64:                  EUI64,
		TemporaryGlobalUnicast: temporaryGlobalUnicast,
	}

	return config
}

// AddAddress method allows to add an ipv6 address to the interface
func (c *Config) AddAddress(addr string) {
	c.Addresses = append(c.Addresses, addr)
}

// RemoveAddress method allows to remove an ipv6 address to the interface
func (c *Config) RemoveAddress(oldAddr string) {
	newList := make([]string, 0, len(c.Addresses))
	for _, addr := range c.Addresses {
		if addr == oldAddr {
			continue
		}

		newList = append(newList, addr)
	}

	c.Addresses = newList
}

// AddPrefix method allows to add an ipv6 prefix to the interface
func (c *Config) AddPrefix(prefix string) {
	c.Prefixes = append(c.Prefixes, prefix)
}

// RemovePrefix method allows to remove an ipv6 prefix to the interface
func (c *Config) RemovePrefix(oldPrefix string) {
	newList := make([]string, 0, len(c.Prefixes))
	for _, addr := range c.Prefixes {
		if addr == oldPrefix {
			continue
		}

		newList = append(newList, addr)
	}

	c.Prefixes = newList
}

// Validate method allows to check if the ipv6 configuration is valid.
func (c Config) Validate(prefix string, issues *[]string) {
	for index, address := range c.Addresses {
		ip, _, err := net.ParseCIDR(address)

		if err != nil {
			*issues = append(*issues, fmt.Sprintf("%s.Addresses[%d]: %s", prefix, index, err.Error()))
			continue
		}

		if ip.To4() != nil {
			*issues = append(*issues, fmt.Sprintf("%s.Addresses[%d]: not an IPv6 address: %s", prefix, index, address))
			continue
		}
	}

	for index, prefixAddr := range c.Prefixes {
		ip, network, err := net.ParseCIDR(prefixAddr)

		if err != nil {
			*issues = append(*issues, fmt.Sprintf("%s.Prefixes[%d]: %s", prefix, index, err.Error()))
			continue
		}

		if ip.To4() != nil {
			*issues = append(*issues, fmt.Sprintf("%s.Prefixes[%d]: not an IPv6 prefix: %s", prefix, index, prefixAddr))
			continue
		}

		ones, _ := network.Mask.Size()

		if ones > 64 {
			*issues = append(*issues, fmt.Sprintf("%s.Prefixes[%d]: prefix bigger than 64 not allowed", prefix, index))
			continue
		}

		if !network.IP.Equal(ip) {
			*issues = append(*issues, fmt.Sprintf("%s.Prefixes[%d]: host part has bit(s) sets", prefix, index))
			return
		}
	}
}

// Clone method allows to clone the ipv6 configuration
func (c Config) Clone() startup.Config {
	config := NewConfig(c.LinkLocalEnabled, c.EUI64, c.TemporaryGlobalUnicast)
	config.Addresses = append(config.Addresses, c.Addresses...)
	config.Prefixes = append(config.Prefixes, c.Prefixes...)
	return config
}

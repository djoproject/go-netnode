package ipv6_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/startup/diff"
	"gitlab.com/go-netnode/startup/l3/ipv6"
)

func TestNewConfig(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	assert.Empty(t, cfg.Addresses)
	assert.Empty(t, cfg.Prefixes)
	assert.True(t, cfg.LinkLocalEnabled)
	assert.True(t, cfg.EUI64)
	assert.True(t, cfg.TemporaryGlobalUnicast)
}

func TestValidateEmptyOk(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)

	issues := make([]string, 0)
	cfg.Validate("ipv6", &issues)
	assert.Empty(t, issues)
}

func TestValidateEmptyInvalidAddress(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	cfg.AddAddress("toto")

	issues := make([]string, 0)
	cfg.Validate("ipv6", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ipv6.Addresses[0]: invalid CIDR address: toto", issues[0])
}

func TestValidateEmptyAddressNotAnIPv6(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	cfg.AddAddress("192.168.1.1/24")

	issues := make([]string, 0)
	cfg.Validate("ipv6", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ipv6.Addresses[0]: not an IPv6 address: 192.168.1.1/24", issues[0])
}

func TestValidateEmptyInvalidPrefix(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	cfg.AddPrefix("toto")

	issues := make([]string, 0)
	cfg.Validate("ipv6", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ipv6.Prefixes[0]: invalid CIDR address: toto", issues[0])
}

func TestValidateEmptyPrefixNotAnIPv6(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	cfg.AddPrefix("192.168.1.1/24")

	issues := make([]string, 0)
	cfg.Validate("ipv6", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ipv6.Prefixes[0]: not an IPv6 prefix: 192.168.1.1/24", issues[0])
}

func TestValidateEmptyPrefixBiggerThan64(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	cfg.AddPrefix("fe80::/72")

	issues := make([]string, 0)
	cfg.Validate("ipv6", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ipv6.Prefixes[0]: prefix bigger than 64 not allowed", issues[0])
}

func TestValidateEmptyPrefixHostBitsSet(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	cfg.AddPrefix("fe80::1/64")

	issues := make([]string, 0)
	cfg.Validate("ipv6", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ipv6.Prefixes[0]: host part has bit(s) sets", issues[0])
}

func TestClone(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	cfg.AddAddress("fe80::1/64")
	cfg.AddPrefix("2001:DB8::/32")

	cpy := cfg.Clone()
	cfgDiff, err := diff.NewDiff(cfg, cpy)
	require.NoError(t, err)
	assert.True(t, cfgDiff.IsEmpty())
}

func TestRemoveAddress(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	cfg.AddAddress("fe80::1/64")
	cfg.RemoveAddress("fe80::1/64")
	assert.Empty(t, cfg.Addresses)
}

func TestRemoveAddressUnknown(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	cfg.AddAddress("fe80::1/64")
	cfg.RemoveAddress("fe00::1/64")
	assert.NotEmpty(t, cfg.Addresses)
}

func TestRemovePrefix(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	cfg.AddPrefix("fe80::/64")
	cfg.RemovePrefix("fe80::/64")
	assert.Empty(t, cfg.Prefixes)
}

func TestRemovePrefixUnknown(t *testing.T) {
	cfg := ipv6.NewConfig(true, true, true)
	cfg.AddPrefix("fe80::/64")
	cfg.RemovePrefix("fe00::/64")
	assert.NotEmpty(t, cfg.Prefixes)
}

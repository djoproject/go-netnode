package diff_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/startup/diff"
)

type invalidToDiff struct {
	m map[string]uintptr
}

func TestNewDiffInvalidA(t *testing.T) {
	i := invalidToDiff{
		m: make(map[string]uintptr),
	}

	_, err := diff.NewDiff(i, nil)
	assert.Error(t, err)
}

func TestNewDiffInvalidiB(t *testing.T) {
	i := invalidToDiff{
		m: make(map[string]uintptr),
	}

	_, err := diff.NewDiff(nil, i)
	assert.Error(t, err)
}

type innerToDiff struct {
	i int
}

type toDiff struct {
	a string
	s innerToDiff
}

func TestNewDiff(t *testing.T) {
	a := toDiff{
		a: "plop",
		s: innerToDiff{
			i: 25,
		},
	}

	b := toDiff{
		a: "plip",
	}

	diff, err := diff.NewDiff(a, b)
	require.NoError(t, err)
	assert.False(t, diff.IsEmpty())

	// diffs
	require.Len(t, diff.Diffs, 1)
	d := diff.Diffs[0]
	assert.Equal(t, "a", d.Name)
	require.NotNil(t, d.Old)
	assert.Equal(t, "plop", *d.Old)
	require.NotNil(t, d.New)
	assert.Equal(t, "plip", *d.New)

	// sub object
	require.Len(t, diff.SubObjects, 1)
	sub, exists := diff.SubObjects["s"]
	require.True(t, exists)
	require.Len(t, sub.Diffs, 1)
	d = sub.Diffs[0]
	assert.Equal(t, "i", d.Name)
	require.NotNil(t, d.Old)
	assert.Equal(t, "25", *d.Old)
	require.NotNil(t, d.New)
	assert.Equal(t, "0", *d.New)
}

func TestNewDiffAIsNil(t *testing.T) {
	b := toDiff{
		a: "plip",
	}

	diff, err := diff.NewDiff(nil, b)
	require.NoError(t, err)
	assert.False(t, diff.IsEmpty())

	require.Len(t, diff.Diffs, 1)
	d := diff.Diffs[0]
	assert.Equal(t, "a", d.Name)
	require.Nil(t, d.Old)
	require.NotNil(t, d.New)
	assert.Equal(t, "plip", *d.New)

	// sub object
	require.Len(t, diff.SubObjects, 1)
	sub, exists := diff.SubObjects["s"]
	require.True(t, exists)
	require.Len(t, sub.Diffs, 1)
	d = sub.Diffs[0]
	assert.Equal(t, "i", d.Name)
	assert.Nil(t, d.Old)
	require.NotNil(t, d.New)
	assert.Equal(t, "0", *d.New)
}

func TestNewDiffBIsNil(t *testing.T) {
	a := toDiff{
		a: "plop",
	}

	diff, err := diff.NewDiff(a, nil)
	require.NoError(t, err)
	assert.False(t, diff.IsEmpty())

	require.Len(t, diff.Diffs, 1)
	d := diff.Diffs[0]
	assert.Equal(t, "a", d.Name)
	require.NotNil(t, d.Old)
	assert.Equal(t, "plop", *d.Old)
	assert.Nil(t, d.New)

	// sub object
	require.Len(t, diff.SubObjects, 1)
	sub, exists := diff.SubObjects["s"]
	require.True(t, exists)
	require.Len(t, sub.Diffs, 1)
	d = sub.Diffs[0]
	assert.Equal(t, "i", d.Name)
	require.NotNil(t, d.Old)
	assert.Equal(t, "0", *d.Old)
	assert.Nil(t, d.New)
}

func TestNewDiffBothAreNil(t *testing.T) {
	diff, err := diff.NewDiff(nil, nil)
	require.NoError(t, err)
	assert.True(t, diff.IsEmpty())
}

type toDiff2 struct {
}

func TestOnlyExistOnLeft(t *testing.T) {
	a := toDiff{
		a: "plop",
	}

	b := toDiff2{}

	diff, err := diff.NewDiff(a, b)
	require.NoError(t, err)
	assert.False(t, diff.IsEmpty())

	require.Len(t, diff.Diffs, 1)
	d := diff.Diffs[0]
	assert.Equal(t, "a", d.Name)
	require.NotNil(t, d.Old)
	assert.Equal(t, "plop", *d.Old)
	require.Nil(t, d.New)

	// sub object
	require.Len(t, diff.SubObjects, 1)
	sub, exists := diff.SubObjects["s"]
	require.True(t, exists)
	require.Len(t, sub.Diffs, 1)
	d = sub.Diffs[0]
	assert.Equal(t, "i", d.Name)
	require.NotNil(t, d.Old)
	assert.Equal(t, "0", *d.Old)
	assert.Nil(t, d.New)
}

func TestOnlyExistOnRight(t *testing.T) {
	a := toDiff{
		a: "plop",
	}

	b := toDiff2{}

	diff, err := diff.NewDiff(b, a)
	require.NoError(t, err)
	assert.False(t, diff.IsEmpty())

	require.Len(t, diff.Diffs, 1)
	d := diff.Diffs[0]
	assert.Equal(t, "a", d.Name)
	require.Nil(t, d.Old)
	require.NotNil(t, d.New)
	assert.Equal(t, "plop", *d.New)

	// sub object
	require.Len(t, diff.SubObjects, 1)
	sub, exists := diff.SubObjects["s"]
	require.True(t, exists)
	require.Len(t, sub.Diffs, 1)
	d = sub.Diffs[0]
	assert.Equal(t, "i", d.Name)
	assert.Nil(t, d.Old)
	require.NotNil(t, d.New)
	assert.Equal(t, "0", *d.New)
}

func TestIsEmptyTrue(t *testing.T) {
	a := toDiff{
		a: "plop",
	}

	diff, err := diff.NewDiff(a, a)
	require.NoError(t, err)
	assert.True(t, diff.IsEmpty())
}

func TestIsNotEmptyBecauseASubObject(t *testing.T) {
	a := toDiff{
		s: innerToDiff{
			i: 25,
		},
	}

	b := toDiff{}

	diff, err := diff.NewDiff(a, b)
	require.NoError(t, err)
	assert.False(t, diff.IsEmpty())
}

func TestDiffString(t *testing.T) {
	a := toDiff{
		a: "plop",
		s: innerToDiff{
			i: 25,
		},
	}

	b := toDiff{
		a: "plip",
	}

	diff, err := diff.NewDiff(a, b)
	require.NoError(t, err)

	output := diff.String()
	assert.Equal(t, "- a=plop\n+ a=plip\ns\n-  i=25\n+  i=0\n", output)
}

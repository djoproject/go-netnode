package diff

import (
	"bytes"
	"fmt"
)

// Diff object represents a field and its value change
type Diff struct {
	Name string
	Old  *string
	New  *string
}

// String method allows to write a string representation of a diff
func (d *Diff) String(prefix string) string {
	var buffer bytes.Buffer
	if d.Old != nil {
		buffer.WriteString(fmt.Sprintf("-%s %s=%s\n", prefix, d.Name, *d.Old))
	}

	if d.New != nil {
		buffer.WriteString(fmt.Sprintf("+%s %s=%s\n", prefix, d.Name, *d.New))
	}
	return buffer.String()
}

// ObjectDiff object represents all the diff between two objects
type ObjectDiff struct {
	Diffs      []*Diff
	SubObjects map[string]*ObjectDiff
}

func (d *ObjectDiff) addDiff(name string, old, neww *string) {
	d.Diffs = append(d.Diffs, &Diff{
		Name: name,
		Old:  old,
		New:  neww,
	})
}

func newDiffOneIsNil(c *Object, aIsNil bool) *ObjectDiff {
	diff := &ObjectDiff{
		Diffs:      make([]*Diff, 0),
		SubObjects: make(map[string]*ObjectDiff),
	}

	// Fields
	fields := c.GetFields()

	for k, value := range fields {
		if aIsNil {
			diff.addDiff(k, nil, &value)
		} else {
			diff.addDiff(k, &value, nil)
		}
	}

	// Sub objects
	objects := c.GetSubObjects()

	for k, object := range objects {
		if aIsNil {
			diff.SubObjects[k] = newDiff(nil, object)
		} else {
			diff.SubObjects[k] = newDiff(object, nil)
		}
	}

	return diff
}

// NewDiff function creates a diff representation of two objects
func NewDiff(a, b interface{}) (*ObjectDiff, error) {
	var aObject, bObject *Object
	var err error

	if a != nil {
		aObject, err = NewObject(a)

		if err != nil {
			return nil, err
		}
	}

	if b != nil {
		bObject, err = NewObject(b)

		if err != nil {
			return nil, err
		}
	}

	return newDiff(aObject, bObject), nil
}

func newDiff(a, b *Object) *ObjectDiff {
	if a == nil && b == nil {
		return nil
	}

	if a == nil {
		return newDiffOneIsNil(b, true)
	}

	if b == nil {
		return newDiffOneIsNil(a, false)
	}

	diff := &ObjectDiff{
		Diffs:      make([]*Diff, 0),
		SubObjects: make(map[string]*ObjectDiff),
	}

	// Fields
	aFields := a.GetFields()
	bFields := b.GetFields()

	for k, aValue := range aFields {
		bValue, exists := bFields[k]

		if !exists {
			diff.addDiff(k, &aValue, nil)
			continue
		}

		if aValue != bValue {
			diff.addDiff(k, &aValue, &bValue)
		}
	}

	for k, bValue := range bFields {
		_, exists := aFields[k]

		if !exists {
			diff.addDiff(k, nil, &bValue)
		}
	}

	// Sub objects
	aObjects := a.GetSubObjects()
	bObjects := b.GetSubObjects()

	for k, aObject := range aObjects {
		bObject, exists := bObjects[k]

		if !exists {
			diff.SubObjects[k] = newDiff(aObject, nil)
			continue
		}

		diff.SubObjects[k] = newDiff(aObject, bObject)
	}

	for k, bObject := range bObjects {
		_, exists := aObjects[k]

		if !exists {
			diff.SubObjects[k] = newDiff(nil, bObject)
		}
	}

	return diff
}

func (d *ObjectDiff) innerPrintDiff(writer *bytes.Buffer, prefix string) {
	for _, diff := range d.Diffs {
		writer.WriteString(diff.String(prefix))
	}

	for name, subObject := range d.SubObjects {
		fmt.Fprintf(writer, "%s%s\n", prefix, name)
		subObject.innerPrintDiff(writer, " "+prefix)
	}
}

// String methods write a representation of all the diff between two objets into
// an io stream
func (d *ObjectDiff) String() string {
	var buffer bytes.Buffer
	d.innerPrintDiff(&buffer, "")
	return buffer.String()
}

// IsEmpty method return True if there is no diff at all, false otherwise
func (d *ObjectDiff) IsEmpty() bool {
	if d == nil {
		return true
	}

	if len(d.Diffs) > 0 {
		return false
	}

	for _, v := range d.SubObjects {
		if len(v.Diffs) > 0 {
			return false
		}
	}

	return true
}

package diff_test

import (
	"testing"
	"unsafe"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/startup/diff"
)

type objectStructTest struct {
	i  int
	u  uint
	b  bool
	f  float32
	c  complex64
	s  string
	a  [1]bool
	sl []bool
	m  map[string]int
}

func TestNewObjectNotAStruct(t *testing.T) {
	_, err := diff.NewObject("struct")
	assert.EqualError(t, err, "Expected a struct, got a string")
}

func TestNewNilObject(t *testing.T) {
	obj, err := diff.NewObject(nil)
	require.NoError(t, err)
	assert.NotNil(t, obj)
}

func TestNewObject(t *testing.T) {
	stru := objectStructTest{
		i:  0,
		u:  0,
		b:  false,
		f:  0,
		c:  0,
		s:  "",
		a:  [1]bool{false},
		sl: nil,
		m:  nil,
	}
	stru.sl = make([]bool, 1)
	stru.m = make(map[string]int)
	stru.m["a"] = 1

	obj, err := diff.NewObject(stru)
	require.NoError(t, err)
	assert.NotNil(t, obj)

	fields := obj.GetFields()

	value, exists := fields["i"]
	assert.True(t, exists)
	assert.Equal(t, "0", value)

	value, exists = fields["u"]
	assert.True(t, exists)
	assert.Equal(t, "0", value)

	value, exists = fields["b"]
	assert.True(t, exists)
	assert.Equal(t, "false", value)

	value, exists = fields["f"]
	assert.True(t, exists)
	assert.Equal(t, "0", value)

	value, exists = fields["c"]
	assert.True(t, exists)
	assert.Equal(t, "(0+0i)", value)

	value, exists = fields["s"]
	assert.True(t, exists)
	assert.Equal(t, "", value)

	value, exists = fields["a[0]"]
	assert.True(t, exists)
	assert.Equal(t, "false", value)

	value, exists = fields["sl[0]"]
	assert.True(t, exists)
	assert.Equal(t, "false", value)

	value, exists = fields["m[a]"]
	assert.True(t, exists)
	assert.Equal(t, "1", value)
}

type invalidStruct struct {
	pointer uintptr
}

func TestInvalidField(t *testing.T) {
	_, err := diff.NewObject(invalidStruct{
		pointer: 0,
	})
	assert.EqualError(t, err, "fields pointer is of unsuported type: uintptr")
}

type invalidMapKey struct {
	m map[*string]int
}

func TestMapInvallidKey(t *testing.T) {
	stru := invalidMapKey{}
	stru.m = make(map[*string]int)
	plop := "plop"
	stru.m[&plop] = 55

	_, err := diff.NewObject(stru)
	assert.EqualError(t, err, "got an error while processing field m: not allowed key type: ptr")
}

type arrayMapKey struct {
	m map[[2]int]int
}

func TestMapArrayKey(t *testing.T) {
	stru := arrayMapKey{}
	stru.m = make(map[[2]int]int)
	stru.m[[2]int{1, 2}] = 22

	obj, err := diff.NewObject(stru)
	require.NoError(t, err)
	assert.NotNil(t, obj)

	fields := obj.GetFields()

	value, exists := fields["m[[]int{1, 2}]"]
	require.True(t, exists)
	assert.Equal(t, "22", value)
}

type mapInvalidValue struct {
	m map[string]uintptr
}

func TestMapInvalidValue(t *testing.T) {
	stru := mapInvalidValue{}
	stru.m = make(map[string]uintptr)
	var s string
	stru.m["plop"] = uintptr(unsafe.Pointer(&s))

	_, err := diff.NewObject(stru)
	require.EqualError(t, err, "got an error while processing field m: not allowed value type: uintptr")
}

type mapPointerValue struct {
	m map[string]*int
}

func TestMapPointerValue(t *testing.T) {
	stru := mapPointerValue{}
	stru.m = make(map[string]*int)
	var i int
	stru.m["plop"] = &i

	obj, err := diff.NewObject(stru)
	require.NoError(t, err)
	assert.NotNil(t, obj)

	fields := obj.GetFields()

	value, exists := fields["m[plop]"]
	require.True(t, exists)
	assert.Equal(t, "0", value)
}

type emptySubStruct struct {
	m map[string][][]int
}

func TestEmptySubStruct(t *testing.T) {
	stru := emptySubStruct{}
	stru.m = make(map[string][][]int)
	sl := make([][]int, 0)
	stru.m["plop"] = sl

	obj, err := diff.NewObject(stru)
	require.NoError(t, err)
	assert.NotNil(t, obj)

	fields := obj.GetFields()
	assert.Empty(t, fields)
}

type innerStruct struct {
	s string
}

type outerStruct struct {
	i innerStruct
}

func TestInnerStruct(t *testing.T) {
	stru := outerStruct{
		i: innerStruct{
			s: "",
		},
	}

	obj, err := diff.NewObject(stru)
	require.NoError(t, err)
	assert.NotNil(t, obj)

	subObjects := obj.GetSubObjects()
	require.NotEmpty(t, subObjects)

	subObj, exists := subObjects["i"]
	require.True(t, exists)

	fields := subObj.GetFields()

	value, exists := fields["s"]
	require.True(t, exists)
	assert.Equal(t, "", value)
}

type invalidInnerStruct struct {
	m map[string]uintptr
}

type invalidOuterStruct struct {
	i invalidInnerStruct
}

func TestInvalidInnerStruct(t *testing.T) {
	stru := invalidOuterStruct{}
	stru.i.m = make(map[string]uintptr)
	var s string
	stru.i.m["plop"] = uintptr(unsafe.Pointer(&s))

	_, err := diff.NewObject(stru)
	require.EqualError(t, err, "got an error while processing field i: got an error while processing field m: not allowed value type: uintptr")
}

type invalidInnerMapStruct struct {
	im map[string]uintptr
}

type invalidOuterMapStruct struct {
	om map[string]invalidInnerMapStruct
}

func TestInvalidInnerMapStruct(t *testing.T) {
	stru := invalidOuterMapStruct{}
	stru.om = make(map[string]invalidInnerMapStruct)

	i := invalidInnerMapStruct{}
	i.im = make(map[string]uintptr)
	stru.om["plip"] = i

	var s string
	i.im["plop"] = uintptr(unsafe.Pointer(&s))

	_, err := diff.NewObject(stru)
	require.EqualError(t, err, "got an error while processing field om[plip]: got an error while processing field im: not allowed value type: uintptr")
}

type structWithNilField struct {
	s *string
}

func TestStructWithNilField(t *testing.T) {
	stru := structWithNilField{
		s: nil,
	}
	obj, err := diff.NewObject(stru)
	require.NoError(t, err)
	assert.NotNil(t, obj)

	fields := obj.GetFields()
	assert.Empty(t, fields)
}

type printStruct struct {
	s string
	i innerStruct
}

func TestObjectString(t *testing.T) {
	stru := printStruct{
		s: "",
		i: innerStruct{
			s: "",
		},
	}
	obj, err := diff.NewObject(stru)
	require.NoError(t, err)

	output := obj.String("")
	assert.Equal(t, "s: \ni:\n    s: \n", output)
}

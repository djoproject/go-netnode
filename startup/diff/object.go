package diff

import (
	"bytes"
	"fmt"
	"reflect"
	"strings"
)

func checkMapKey(keyType reflect.Type) error {
	keyKind := keyType.Kind()
	switch keyKind {
	// Those types should not be used as key in a map in a configuration file.
	case reflect.Invalid, reflect.Uintptr, reflect.Chan, reflect.Func, reflect.Interface, reflect.Ptr, reflect.Struct, reflect.UnsafePointer:
		return fmt.Errorf("not allowed key type: %s", keyKind)
	// Only array is allowed as datastruct to be a key.  Slices or maps are forbidden by golang.
	case reflect.Array:
		return checkMapKey(keyType.Elem())
	}

	return nil
}

func checkDataStructMostInnerValue(valueType reflect.Type) error {
	valueKind := valueType.Kind()
	switch valueKind {
	case reflect.Invalid, reflect.Uintptr, reflect.Chan, reflect.Func, reflect.Interface, reflect.UnsafePointer:
		return fmt.Errorf("not allowed value type: %s", valueKind)
	case reflect.Ptr:
		return checkDataStructMostInnerValue(valueType.Elem())
	}

	return nil
}

func collectDataStructValues(name string, value reflect.Value) (map[string]reflect.Value, error) {
	tempMap := make(map[string]reflect.Value)
	tempMap[name] = value
	currentKind := value.Kind()
	currentType := value.Type()

	for currentKind == reflect.Map || currentKind == reflect.Slice || currentKind == reflect.Array {
		// if current data structure is a map, check the key kind
		if currentKind == reflect.Map {
			err := checkMapKey(currentType.Key())
			if err != nil {
				return nil, err
			}
		}

		// no items, stop whole process here
		if len(tempMap) == 0 {
			return nil, nil
		}

		nextTempMap := make(map[string]reflect.Value)
		var elemType reflect.Type

		for k, v := range tempMap {
			if currentKind == reflect.Map {
				keys := v.MapKeys()

				for _, keyValue := range keys {
					elemValue := v.MapIndex(keyValue)
					key := toString(keyValue)
					nextTempMap[fmt.Sprintf("%s[%s]", k, key)] = elemValue
				}
			} else {
				length := v.Len()

				for i := 0; i < length; i++ {
					elemValue := v.Index(i)
					nextTempMap[fmt.Sprintf("%s[%d]", k, i)] = elemValue
				}
			}

			elemType = v.Type().Elem()
		}

		currentKind = elemType.Kind()
		currentType = elemType
		tempMap = nextTempMap
	}

	// check the most inner value, must be an allowed type.
	err := checkDataStructMostInnerValue(currentType)
	if err != nil {
		return nil, err
	}

	return tempMap, nil
}

func toString(value reflect.Value) string {
	switch value.Kind() {
	case reflect.Bool:
		return fmt.Sprint(value.Bool())
	// case reflect.String:
	// 	value.String()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return fmt.Sprint(value.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return fmt.Sprint(value.Uint())
	case reflect.Float32, reflect.Float64:
		return fmt.Sprint(value.Float())
	case reflect.Complex64, reflect.Complex128:
		return fmt.Sprint(value.Complex())
	case reflect.Array:
		// this case is only used to build a map key composed of array
		length := value.Len()
		strList := make([]string, length)
		elemKind := value.Type().Elem().Kind()

		for i := 0; i < length; i++ {
			elemValue := value.Index(i)
			strList[i] = toString(elemValue)
		}
		return fmt.Sprintf("[]%s{%s}", elemKind.String(), strings.Join(strList, ", "))
	}

	// default case, it never panics, always return a string representation of the value.
	return value.String()
}

// Object object is a meta representation of any kind of object.
type Object struct {
	Fields    map[string]string
	SubFields map[string]*Object
}

// NewObject function creates a new Object instance
func NewObject(object interface{}) (*Object, error) {
	if object == nil {
		return &Object{}, nil
	}

	val := reflect.ValueOf(object) // Get the run-time data of the value
	val = reflect.Indirect(val)    // be sure it is not a pointer
	return newObject(val)
}

func newObject(value reflect.Value) (*Object, error) {
	valueKind := value.Kind()
	if valueKind != reflect.Struct {
		return nil, fmt.Errorf("Expected a struct, got a %s", valueKind)
	}

	obj := &Object{
		Fields:    make(map[string]string),
		SubFields: make(map[string]*Object),
	}

	typ := value.Type()

	for i := 0; i < typ.NumField(); i++ {
		fieldValue := value.Field(i) // returns a reflect.Value
		field := typ.Field(i)        // returns a reflect.StructField
		err := obj.addValue(field.Name, fieldValue)

		if err != nil {
			return nil, err
		}
	}

	return obj, nil
}

// GetFields method returns the map of fields
func (o Object) GetFields() map[string]string {
	return o.Fields
}

// GetSubObjects method returns the map of sub objects
func (o Object) GetSubObjects() map[string]*Object {
	return o.SubFields
}

// String method prints a string representation of an Object into an io stream
func (o Object) String(prefix string) string {
	var buffer bytes.Buffer

	for k, v := range o.Fields {
		buffer.WriteString(fmt.Sprintf("%s%s: %s\n", prefix, k, v))
	}

	for k, v := range o.SubFields {
		buffer.WriteString(fmt.Sprintf("%s%s:\n", prefix, k))
		buffer.WriteString(v.String(fmt.Sprintf("    %s", prefix)))
	}

	return buffer.String()
}

func (o *Object) addValue(name string, value reflect.Value) error {
	switch value.Kind() {
	case reflect.Ptr:
		if value.IsNil() {
			// do not store something that is nil
			return nil
		}

		value = reflect.Indirect(value)
		return o.addValue(name, value)
	case reflect.Struct:
		subObj, err := newObject(value)

		if err != nil {
			return fmt.Errorf("got an error while processing field %s: %w", name, err)
		}

		o.SubFields[name] = subObj
	case reflect.Map, reflect.Slice, reflect.Array:
		valueMap, err := collectDataStructValues(name, value)
		if err != nil {
			return fmt.Errorf("got an error while processing field %s: %w", name, err)
		}

		for k, v := range valueMap {
			err = o.addValue(k, v)

			if err != nil {
				// do not encapsulate err with fmt.Errorf, it has already be wrapped
				// in addValue recursive call.
				return err
			}
		}
	case reflect.Invalid, reflect.Uintptr, reflect.Chan, reflect.Func, reflect.UnsafePointer, reflect.Interface:
		// not supported type, or not pertinent to support, trigger an error.
		return fmt.Errorf("fields %s is of unsuported type: %s", name, value.Kind().String())
	default:
		o.Fields[name] = toString(value)
	}

	return nil
}

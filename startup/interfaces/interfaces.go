package interfaces

import (
	"fmt"

	"gitlab.com/go-netnode/startup"
	"gitlab.com/go-netnode/startup/l2/ethernet"
	"gitlab.com/go-netnode/startup/l3/ipv4"
	"gitlab.com/go-netnode/startup/l3/ipv6"
)

const (
	// ConfigModeNone constant represents an interface with no purpose
	ConfigModeNone = iota

	// ConfigModeEthernet constant represents an interface that works with
	// ethernet ethernet encapsulation
	ConfigModeEthernet

	// ConfigModeVlan constant represents an interface that works with
	// vlan encapsulation
	ConfigModeVlan

	// ConfigModeQinQ constant represents an interface that works with
	// qinq encapsulation
	ConfigModeQinQ

	// ConfigModeRouted constant represents an interface that works with
	// IPv4/IPv6 encapsulation
	ConfigModeRouted
)

// Config object represents an network interface configuration
type Config struct {
	Mode int
	MTU  uint16 `json:"mtu"`

	L2Ethernet *ethernet.Config `json:"ethernet"`
	L3IPv4     *ipv4.Config     `json:"ipv4"`
	L3IPv6     *ipv6.Config     `json:"ipv6"`
}

// NewConfig function creates a Config object
func NewConfig(mtu uint16, mode int) *Config {
	return &Config{
		Mode: mode,
		MTU:  mtu,
	}
}

// Validate method allows to check if the interface configuration is valid.
func (c Config) Validate(prefix string, issues *[]string) {
	if c.Mode < ConfigModeNone || c.Mode > ConfigModeRouted {
		*issues = append(*issues, fmt.Sprintf("%s.Mode: unknown value", prefix))
	}

	if c.MTU < 1 {
		*issues = append(*issues, fmt.Sprintf("%s.MTU must be bigger than 0", prefix))
	}

	if c.L2Ethernet != nil {
		c.L2Ethernet.Validate(fmt.Sprintf("%s.L2Ethernet", prefix), issues)
	}

	if c.L3IPv4 != nil {
		c.L3IPv4.Validate(fmt.Sprintf("%s.L3IPv4", prefix), issues)
	}

	if c.L3IPv6 != nil {
		c.L3IPv6.Validate(fmt.Sprintf("%s.L3IPv6", prefix), issues)
	}
}

// Clone method allows to clone the interface configuration
func (c Config) Clone() startup.Config {
	iface := NewConfig(c.MTU, c.Mode)

	if c.L2Ethernet != nil {
		value := c.L2Ethernet.Clone()
		iface.L2Ethernet, _ = value.(*ethernet.Config)
	}

	if c.L3IPv4 != nil {
		value := c.L3IPv4.Clone()
		iface.L3IPv4, _ = value.(*ipv4.Config)
	}

	if c.L3IPv6 != nil {
		value := c.L3IPv6.Clone()
		iface.L3IPv6, _ = value.(*ipv6.Config)
	}

	return iface
}

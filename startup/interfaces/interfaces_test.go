package interfaces_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/startup/diff"
	"gitlab.com/go-netnode/startup/interfaces"
	"gitlab.com/go-netnode/startup/l2/ethernet"
	"gitlab.com/go-netnode/startup/l3/ipv4"
	"gitlab.com/go-netnode/startup/l3/ipv6"
)

func TestNewConfig(t *testing.T) {
	cfg := interfaces.NewConfig(15, 30)
	assert.Equal(t, cfg.Mode, 30)
	assert.Equal(t, cfg.MTU, uint16(15))
}

func TestValidate(t *testing.T) {
	tests := map[string]struct {
		mode  int
		mtu   uint16
		issue string
	}{
		"TestValidateInvalidMode": {
			mode:  200,
			mtu:   30,
			issue: "Interface.Mode: unknown value",
		},
		"TestValidateInvalidMTU": {
			mode:  1,
			mtu:   0,
			issue: "Interface.MTU must be bigger than 0",
		},
		"TestValidateOk": {
			mode:  1,
			mtu:   30,
			issue: "",
		},
	}

	for testName, parameters := range tests {
		t.Run(testName, func(t *testing.T) {
			cfg := interfaces.NewConfig(parameters.mtu, parameters.mode)
			issues := make([]string, 0)
			cfg.Validate("Interface", &issues)

			if len(parameters.issue) == 0 {
				require.Empty(t, issues)
			} else {
				require.NotEmpty(t, issues)
				assert.Equal(t, parameters.issue, issues[0])
			}
		})
	}
}

func TestValidateSubObjects(t *testing.T) {
	cfg := interfaces.NewConfig(30, 1)
	cfg.L2Ethernet = ethernet.NewConfig([6]byte{0x01, 0x00, 0x00, 0x00, 0x00, 0x00})
	cfg.L3IPv4 = ipv4.NewConfig(true, true, true, true)
	cfg.L3IPv6 = ipv6.NewConfig(true, true, true)
	issues := make([]string, 0)
	cfg.Validate("Interface", &issues)
	require.Empty(t, issues)
}

func TestClone(t *testing.T) {
	cfg := interfaces.NewConfig(30, 1)
	cfg.L2Ethernet = ethernet.NewConfig([6]byte{0x01, 0x00, 0x00, 0x00, 0x00, 0x00})
	cfg.L3IPv4 = ipv4.NewConfig(true, true, true, true)
	cfg.L3IPv6 = ipv6.NewConfig(true, true, true)
	cpy := cfg.Clone()
	cfgDiff, err := diff.NewDiff(cfg, cpy)
	require.NoError(t, err)
	assert.True(t, cfgDiff.IsEmpty())
}

package ethernet

import (
	"fmt"

	"gitlab.com/go-netnode/startup"
)

// Config object represents an ethernet configuration for an interface
type Config struct {
	MAC [6]byte `json:"mac"` // TODO should be a simple string

	// no more MAC address is needed, the other are going to be computed at
	// runtime.  E.g.:
	//	IPv4 mutlicast
	//	IPv6 multicast
	//  IPv6 solicited node multicast
	//  ...
}

// NewConfig function creates an ethernet Config object
func NewConfig(mac [6]byte) *Config {
	return &Config{
		MAC: mac,
	}
}

// Validate method allows to check if the ethernet configuration is valid
func (c Config) Validate(prefix string, issues *[]string) {
	if c.MAC[0]&0x01 != 1 {
		*issues = append(*issues, fmt.Sprintf("%s.MAC is not a locally administered address", prefix))
	}
}

// Clone method allows to clone the ethernet configuration
func (c Config) Clone() startup.Config {
	return NewConfig(c.MAC)
}

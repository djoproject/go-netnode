package ethernet_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/startup/diff"
	"gitlab.com/go-netnode/startup/l2/ethernet"
)

func TestNewConfig(t *testing.T) {
	e := ethernet.NewConfig([6]byte{0x01, 0x11, 0x22, 0x33, 0x44, 0x55})
	assert.Equal(t, [6]byte{0x01, 0x11, 0x22, 0x33, 0x44, 0x55}, e.MAC)
}

func TestValidateOk(t *testing.T) {
	e := ethernet.NewConfig([6]byte{0x01, 0x11, 0x22, 0x33, 0x44, 0x55})
	issues := make([]string, 0)
	e.Validate("ethernet", &issues)
	assert.Empty(t, issues)
}

func TestInvalidMac(t *testing.T) {
	e := ethernet.NewConfig([6]byte{0x00, 0x11, 0x22, 0x33, 0x44, 0x55})
	issues := make([]string, 0)
	e.Validate("ethernet", &issues)
	require.NotEmpty(t, issues)
	assert.Len(t, issues, 1)
	assert.Equal(t, "ethernet.MAC is not a locally administered address", issues[0])
}

func TestClone(t *testing.T) {
	cfg := ethernet.NewConfig([6]byte{0x01, 0x11, 0x22, 0x33, 0x44, 0x55})
	cpy := cfg.Clone()

	cfgDiff, err := diff.NewDiff(cfg, cpy)
	require.NoError(t, err)
	assert.True(t, cfgDiff.IsEmpty())
}

package ipv6

import "fmt"

// AllHosts variable contains the adresse representation of the multicast address
// all hosts ff02::1
var AllHosts [16]byte

// AllRouters variable contains the adresse representation of the multicast address
// all routers ff02::2
var AllRouters [16]byte

func init() {
	AllHosts = CreateMulticastAddress(McastScopeLink, McasSrvAllNodes)
	AllRouters = CreateMulticastAddress(McastScopeLink, McasSrvAllRouters)
}

// CreateAddress function allow to create an IPv6 adress from a prefix and a suffix byte slices.
func CreateAddress(prefix, suffix []byte) ([16]byte, error) {
	var result [16]byte

	if len(prefix)+len(suffix) > 16 {
		return result, fmt.Errorf("CreateAddress: sum of prefix and suffix bytes gives more than 16: %d", len(prefix)+len(suffix))
	}

	for i := 0; i < len(prefix); i++ {
		result[i] = prefix[i]
	}

	for i := 0; i < len(suffix); i++ {
		result[16-len(suffix)+i] = suffix[i]
	}

	return result, nil
}

const (
	// McastScopeInterface constant defines multicast interface scope
	McastScopeInterface uint8 = 1
	// McastScopeLink constant defines multicast link scope
	McastScopeLink uint8 = 2
	// McastScopeRealm constant defines multicast realm scope
	McastScopeRealm uint8 = 3
	// McastScopeAdmin constant defines multicast admin scope
	McastScopeAdmin uint8 = 4
	// McastScopeSite constant defines multicast site scope
	McastScopeSite uint8 = 5
	// McastScopeOrg constant defines multicast organisation scope
	McastScopeOrg uint8 = 8
)

var (
	// McasSrvAllNodes variable defines All Nodes service
	McasSrvAllNodes = [4]byte{0x00, 0x00, 0x00, 0x01}
	// McasSrvAllRouters variable defines All Routers service
	McasSrvAllRouters = [4]byte{0x00, 0x00, 0x00, 0x02}
	// McasSrvOSPFSPFRouter variable defines OSPF SPF Routers service
	McasSrvOSPFSPFRouter = [4]byte{0x00, 0x00, 0x00, 0x05}
	// McasSrvOSPFDRRouter variable defines OSPF DR Routers service
	McasSrvOSPFDRRouter = [4]byte{0x00, 0x00, 0x00, 0x06}
	// McasSrvISISrouter variable defines ISIS Routers service
	McasSrvISISrouter = [4]byte{0x00, 0x00, 0x00, 0x08}
	// McasSrvRIPRouter variable defines RIP Routers service
	McasSrvRIPRouter = [4]byte{0x00, 0x00, 0x00, 0x09}
	// McasSrvEIRGPRouter variable defines EIGRP Routers service
	McasSrvEIRGPRouter = [4]byte{0x00, 0x00, 0x00, 0x0A}
	// McasSrvPIMRouter variable defines PIM Routers service
	McasSrvPIMRouter = [4]byte{0x00, 0x00, 0x00, 0x0D}
	// McasSrvVRRPRouter variable defines VRRP Routers service
	McasSrvVRRPRouter = [4]byte{0x00, 0x00, 0x00, 0x12}
	// McasSrvMLD variable defines MLD service
	McasSrvMLD = [4]byte{0x00, 0x00, 0x00, 0x16}
	// McasSrvDHCPAll variable defines DHCP ALL (server+relay) service
	McasSrvDHCPAll = [4]byte{0x00, 0x01, 0x00, 0x02}
	// McasSrvLLMNR variable defines LLMNR service
	McasSrvLLMNR = [4]byte{0x00, 0x01, 0x00, 0x03}
	// McasSrvDHCPServer variable defines DHCP Server service
	McasSrvDHCPServer = [4]byte{0x00, 0x01, 0x00, 0x03}
	// McasSrvSSDP variable defines SSDP service
	McasSrvSSDP = [4]byte{0x00, 0x00, 0x00, 0x0C}
	// McasSrvMDNS variable defines MDNS service
	McasSrvMDNS = [4]byte{0x00, 0x00, 0x00, 0xFB}
	// McasSrvNTP variable defines NTP service
	McasSrvNTP = [4]byte{0x00, 0x00, 0x01, 0x01}
	// McasSrvNIS variable defines NIS service
	McasSrvNIS = [4]byte{0x00, 0x00, 0x01, 0x08}
	// McasSrvPTPMessage variable defines PTP Message service
	McasSrvPTPMessage = [4]byte{0x00, 0x00, 0x01, 0x81}
	// McasSrvPTPPeerDelay variable defines PTP Peer Delay service
	McasSrvPTPPeerDelay = [4]byte{0x00, 0x00, 0x00, 0x6b}
	// McasSrvExperiements variable defines Experiments service
	McasSrvExperiements = [4]byte{0x00, 0x00, 0x01, 0x14}
)

// CreateMulticastAddress function creates a multicast address based on a scope and a service
func CreateMulticastAddress(scope uint8, service [4]byte) [16]byte {
	var result [16]byte

	result[0] = 0xFF
	result[1] = scope
	result[12] = service[0]
	result[13] = service[1]
	result[14] = service[2]
	result[15] = service[3]

	return result
}

// IsInRange function returns true if an address is inside a subnet
func IsInRange(rang [16]byte, mask uint8, address [16]byte) bool {
	for i := 0; i < 16; i++ {
		if mask >= 8 {
			if rang[i] != address[i] {
				return false
			}

			mask -= 8
			continue
		}

		submask := byte(^((1 << mask) - 1))
		return rang[i]&submask == address[i]&submask
	}

	return true
}

// IsInMulticastRange function returns true if the address is a multicast
// address
func IsInMulticastRange(address [16]byte) bool {
	// range: ff00::/8
	return address[0] == 0xFF
}

// IsInRestrictedRange function returns true if an address is into any
// restricted range
func IsInRestrictedRange(address [16]byte) bool {
	// allowed range:
	//	* 2000::/3
	//  * fe80::/10
	//  * ::1/128
	//  * fc00::/7
	//  * ff00::/8

	return !IsInMulticastRange(address) && !IsInLinkLocalRange(address) && !IsInPrivateRange(address) && !IsGlobalUnicast(address) && !IsLoopBack(address)
}

// IsLoopBack function returns true if the provided address is a loopback address
func IsLoopBack(address [16]byte) bool {
	for i := 0; i < 14; i++ {
		if address[i] != 0 {
			return false
		}
	}

	return address[15] == 1
}

// IsGlobalUnicast function returns true if the provided address is in the Global
// Unicast range.
func IsGlobalUnicast(address [16]byte) bool {
	// range: 2000::/3
	return address[0]&0xE0 == 0x20
}

// IsInLinkLocalRange function returns true if an address is in link local
// range
func IsInLinkLocalRange(address [16]byte) bool {
	// range: fe80::/10
	return address[0] == 0xFE && address[1]&0xC0 == 0x80
}

// IsInPrivateRange function returns true if an address is in private range
func IsInPrivateRange(address [16]byte) bool {
	// range: fc00::/7
	return address[0]&0xFE == 0xFC
}

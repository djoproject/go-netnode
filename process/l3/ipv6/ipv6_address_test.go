package ipv6_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process/l3/ipv6"
)

func TestCreateAddress(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{1, 2, 3}, []byte{4, 5, 6})
	require.NoError(t, err)
	assert.Equal(t, [16]byte{1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 5, 6}, addr)
}

func TestCreateAddressError(t *testing.T) {
	_, err := ipv6.CreateAddress([]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}, []byte{4, 5, 6})
	require.EqualError(t, err, "CreateAddress: sum of prefix and suffix bytes gives more than 16: 19")
}

func TestIsInRange(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{0x01})
	require.NoError(t, err)
	assert.True(t, ipv6.IsInRange([16]byte{0xfe, 0x80}, 10, addr))
}

func TestIsInRangeFull(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{})
	require.NoError(t, err)
	assert.True(t, ipv6.IsInRange([16]byte{0xfe, 0x80}, 128, addr))
}

func TestIsInRangeNot(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0x20}, []byte{0x01})
	require.NoError(t, err)
	assert.False(t, ipv6.IsInRange([16]byte{0xfe, 0x80}, 10, addr))
}

func TestIsInMulticastRange(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0xff, 0x02}, []byte{0x01})
	require.NoError(t, err)
	assert.True(t, ipv6.IsInMulticastRange(addr))
}

func TestIsInMulticastRangeNot(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{0x01})
	require.NoError(t, err)
	assert.False(t, ipv6.IsInMulticastRange(addr))
}

func TestIsInRestrictedRange(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0x44}, []byte{})
	require.NoError(t, err)
	assert.True(t, ipv6.IsInRestrictedRange(addr))
}

func TestIsInRestrictedRangeNot(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{})
	require.NoError(t, err)
	assert.False(t, ipv6.IsInRestrictedRange(addr))
}

func TestIsLoopBack(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{}, []byte{0x01})
	require.NoError(t, err)
	assert.True(t, ipv6.IsLoopBack(addr))
}

func TestIsLoopBackNot(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{0x01})
	require.NoError(t, err)
	assert.False(t, ipv6.IsLoopBack(addr))
}

func TestIsGlobalUnicast(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0x20}, []byte{0x01})
	require.NoError(t, err)
	assert.True(t, ipv6.IsGlobalUnicast(addr))
}

func TestIsGlobalUnicastNot(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{0x01})
	require.NoError(t, err)
	assert.False(t, ipv6.IsGlobalUnicast(addr))
}

func TestIsInLinkLocalRange(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{0x01})
	require.NoError(t, err)
	assert.True(t, ipv6.IsInLinkLocalRange(addr))
}

func TestIsInLinkLocalRangeNot(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0x20}, []byte{0x01})
	require.NoError(t, err)
	assert.False(t, ipv6.IsInLinkLocalRange(addr))
}

func TestIsInPrivateRange(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0xfc}, []byte{0x01})
	require.NoError(t, err)
	assert.True(t, ipv6.IsInPrivateRange(addr))
}

func TestIsInPrivateRangeNot(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{0x01})
	require.NoError(t, err)
	assert.False(t, ipv6.IsInPrivateRange(addr))
}

func TestCreateMulticastAddress(t *testing.T) {
	addr := ipv6.CreateMulticastAddress(ipv6.McastScopeLink, ipv6.McasSrvAllNodes)
	assert.Equal(t, [16]byte{0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}, addr)
}

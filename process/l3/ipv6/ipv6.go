package ipv6

import (
	"bytes"
	"encoding/binary"
	"io"
	"net"
)

// Header struct represents the IPv6 header
type Header struct {
	VersionTrafficClassFlowLabel uint32
	PayloadLength                uint16
	NextHeader                   uint8
	HopLimit                     uint8
	SourceByte                   [16]byte
	DestinationByte              [16]byte
}

// NewHeader function creates a new Header struct
func NewHeader(protocol uint8, source, destination [16]byte) *Header {
	return &Header{
		// This basicaly set
		//  * version: 6
		//  * traffic class: 0
		//  * flow label: 0
		VersionTrafficClassFlowLabel: 1610612736,

		PayloadLength:   0,
		NextHeader:      protocol,
		HopLimit:        64,
		SourceByte:      source,
		DestinationByte: destination,
	}
}

// Serialize allows to convert object to []byte
func (h *Header) Serialize(writer io.Writer) error {
	return binary.Write(writer, binary.BigEndian, h)
}

// Deserialize allows to convert []byte into a Header object
func (h *Header) Deserialize(data []byte) error {
	buf := bytes.NewReader(data)
	return binary.Read(buf, binary.BigEndian, h)
}

// Version method returns Header version
func (h Header) Version() uint8 {
	return uint8(h.VersionTrafficClassFlowLabel >> 28)
}

// TrafficClass method returns the traffic class value
func (h Header) TrafficClass() uint8 {
	return uint8(h.VersionTrafficClassFlowLabel >> 20)
}

// FlowLabel method returns the flow label value
func (h Header) FlowLabel() uint32 {
	return h.VersionTrafficClassFlowLabel & 0x000FFFFF
}

// Source method returns source address on a net.IP form
func (h Header) Source() *net.IP {
	p := make(net.IP, net.IPv6len)
	copy(p, h.SourceByte[:])
	return &p
}

// Destination method returns destination address on a net.IP form
func (h Header) Destination() *net.IP {
	p := make(net.IP, net.IPv6len)
	copy(p, h.DestinationByte[:])
	return &p
}

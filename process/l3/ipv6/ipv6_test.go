package ipv6_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process/l3/ipv6"
)

var serializedIPv6 = []byte{0x60, 0x0, 0x0, 0x0, 0x0, 0x0, 0x01, 0x30, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf, 0x10}

func TestNewHeader(t *testing.T) {
	src := [16]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}
	dst := [16]byte{17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32}
	ip := ipv6.NewHeader(42, src, dst)
	require.NotNil(t, ip)
	assert.Equal(t, uint8(6), ip.Version())
	assert.Equal(t, uint8(0), ip.TrafficClass())
	assert.Equal(t, uint32(0), ip.FlowLabel())
	assert.Equal(t, "102:304:506:708:90a:b0c:d0e:f10", ip.Source().String())
	assert.Equal(t, "1112:1314:1516:1718:191a:1b1c:1d1e:1f20", ip.Destination().String())
	assert.Equal(t, uint8(42), ip.NextHeader)
	assert.Equal(t, uint8(64), ip.HopLimit)
}

func TestSerialize(t *testing.T) {
	src := [16]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}
	dst := [16]byte{17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32}
	ip := ipv6.NewHeader(42, src, dst)
	require.NotNil(t, ip)

	var binBuf bytes.Buffer
	err := ip.Serialize(&binBuf)
	assert.NoError(t, err, "ip.Serialize")
	assert.Equal(t, []byte{
		0x60, 0x0, 0x0, 0x0, // version + traffic class + flow label
		0x0, 0x0, 0x2a, 0x40, // payload length + next header + hop limit
		0x1, 0x2, 0x3, 0x4, // src address
		0x5, 0x6, 0x7, 0x8,
		0x9, 0xa, 0xb, 0xc,
		0xd, 0xe, 0xf, 0x10,
		0x11, 0x12, 0x13, 0x14, // dst address
		0x15, 0x16, 0x17, 0x18,
		0x19, 0x1a, 0x1b, 0x1c,
		0x1d, 0x1e, 0x1f, 0x20},
		binBuf.Bytes())
}

func TestDeserialize(t *testing.T) {
	ip := ipv6.Header{}
	err := ip.Deserialize(serializedIPv6)
	assert.NoError(t, err, "ip.Deserialize")

	assert.Equal(t, uint8(6), ip.Version())
	assert.Equal(t, uint8(0), ip.TrafficClass())
	assert.Equal(t, uint32(0), ip.FlowLabel())
	assert.Equal(t, "102:304:506:708:90a:b0c:d0e:f10", ip.Destination().String())
	assert.Equal(t, "1112:1314:1516:1718:191a:1b1c:1d1e:1f20", ip.Source().String())
	assert.Equal(t, uint8(1), ip.NextHeader)
	assert.Equal(t, uint8(48), ip.HopLimit)
}

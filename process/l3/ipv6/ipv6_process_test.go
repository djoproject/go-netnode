package ipv6_test

import (
	"bytes"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l3/ipv4"
	"gitlab.com/go-netnode/process/l3/ipv6"
	"gitlab.com/go-netnode/process/processtest"
	"gitlab.com/go-netnode/utils/pool"
)

//// PROCESSES ///////////////////////////////////////////////////////////////

type emptyProcess struct{}

func (e emptyProcess) Process(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error) {
	return nil, nil
}

type errorProcess struct{}

func (e errorProcess) Process(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error) {
	return nil, errors.New("boom")
}

type dataProcess struct{}

func (e dataProcess) Process(system *process.System, _ []byte, _ process.Meta) (*pool.ReusableBuffer, error) {
	reBuff := system.Device.GetBufferPool().GetBuffer()
	buf := reBuff.GetBuffer()

	d := []byte{0x42, 0xff}
	_, err := buf.GetPrepend().Write(d)
	return reBuff, err
}

//// TESTS ///////////////////////////////////////////////////////////////////

func TestNewProcess(t *testing.T) {
	processObject := ipv6.NewProcess()
	require.NotNil(t, processObject)
	assert.Len(t, processObject.Addresses, 0)
	assert.Len(t, processObject.Protocols, 0)
	assert.Equal(t, 0, processObject.GetRegisteredAddressCount())
}

func TestRegisterAddress(t *testing.T) {
	processObject := ipv6.NewProcess()
	require.NotNil(t, processObject)
	addr, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{0x01})
	require.NoError(t, err)
	processObject.RegisterAddress(addr)

	assert.Equal(t, 1, processObject.GetRegisteredAddressCount())
	_, exist := processObject.Addresses[addr]
	assert.True(t, exist)
}

func TestProcessRegisterAddressProtocol(t *testing.T) {
	addr, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{0x01})
	require.NoError(t, err)

	i := ipv6.NewProcess()
	i.RegisterAddressProtocol(addr, ipv4.ProtocolIgmp, &emptyProcess{})
	_, exists := i.Protocols[ipv4.ProtocolIgmp]
	assert.False(t, exists)

	subPro, exists := i.Addresses[addr]
	require.True(t, exists)
	assert.NotNil(t, subPro)

	pro, exists := subPro[ipv4.ProtocolIgmp]
	assert.True(t, exists)
	assert.NotNil(t, pro)
}

func TestRegisterProtocol(t *testing.T) {
	processObject := ipv6.NewProcess()
	require.NotNil(t, processObject)
	subProcess := emptyProcess{}
	processObject.RegisterProtocol(42, subProcess)
	assert.Len(t, processObject.Protocols, 1)
	value, exist := processObject.Protocols[42]
	require.True(t, exist)
	assert.Equal(t, subProcess, value)
}

func TestTooSmallPacket(t *testing.T) {
	var b []byte
	processObject := ipv6.NewProcess()
	reply, err := processObject.Process(nil, b, process.Meta{})
	assert.EqualError(t, err, "Expecting at least a 40 bytes data, got 0")
	assert.Nil(t, reply)
}

var addrToRegister, _ = ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{0x02})

var datas = map[string]struct {
	addressToRegister  [][16]byte
	protocolToRegister map[uint8]process.Object
	bufferSize         int
	expectedError      string
	expectedData       []byte
}{
	"NoAddressTargeted": {
		addressToRegister:  nil,
		protocolToRegister: nil,
		bufferSize:         100,
		expectedError:      "",
		expectedData:       nil,
	},
	"NoProtocolTargeted": {
		addressToRegister:  [][16]byte{addrToRegister},
		protocolToRegister: nil,
		bufferSize:         100,
		expectedError:      "",
		expectedData:       nil,
	},
	"ProtocolOutputBufferTooSmall": {
		addressToRegister:  [][16]byte{addrToRegister},
		protocolToRegister: map[uint8]process.Object{ipv4.ProtocolTCP: &dataProcess{}},
		bufferSize:         4,
		expectedError:      "Process: Failed to serialize header: Buffer: not enough space",
		expectedData:       nil,
	},
	"ProtocolError": {
		addressToRegister:  [][16]byte{addrToRegister},
		protocolToRegister: map[uint8]process.Object{ipv4.ProtocolTCP: &errorProcess{}},
		bufferSize:         100,
		expectedError:      "Process: An error occured while processing protocol TCP: boom",
		expectedData:       nil,
	},
	"ProtocolNoAnswer": {
		addressToRegister:  [][16]byte{addrToRegister},
		protocolToRegister: map[uint8]process.Object{ipv4.ProtocolTCP: &emptyProcess{}},
		bufferSize:         100,
		expectedError:      "",
		expectedData:       nil,
	},
	"ProtocolSuccess": {
		addressToRegister:  [][16]byte{addrToRegister},
		protocolToRegister: map[uint8]process.Object{ipv4.ProtocolTCP: &dataProcess{}},
		bufferSize:         100,
		expectedError:      "",
		expectedData: []byte{
			0x60, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x06, 0x40,
			0xfe, 0x80, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x02,
			0xfe, 0x80, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x01,
			0x42, 0xff},
	},
}

func TestIPv6Process(t *testing.T) {
	for name, data := range datas {
		t.Run(name, func(t *testing.T) {
			// forge packet
			src, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{0x01})
			require.NoError(t, err, "ipv6.CreateAddress")

			dest, err := ipv6.CreateAddress([]byte{0xfe, 0x80}, []byte{0x02})
			require.NoError(t, err, "ipv6.CreateAddress")

			packet := ipv6.NewHeader(ipv4.ProtocolTCP, src, dest)
			var binBuf bytes.Buffer
			err = packet.Serialize(&binBuf)
			require.NoError(t, err, "Serialize")
			b := binBuf.Bytes()

			// process packet
			i := ipv6.NewProcess()

			for prot, handler := range data.protocolToRegister {
				i.RegisterProtocol(prot, handler)
			}

			for _, addr := range data.addressToRegister {
				i.RegisterAddress(addr)
			}
			require.Equal(t, len(data.addressToRegister), i.GetRegisteredAddressCount())

			pdata := processtest.ProduceProcessData(t, data.bufferSize)
			reply, err := i.Process(pdata, b, process.Meta{})

			if len(data.expectedError) == 0 {
				assert.NoError(t, err)
			} else {
				assert.EqualError(t, err, data.expectedError)
			}

			if data.expectedData == nil {
				require.Nil(t, reply)
			} else {
				require.NotNil(t, reply)
				assert.Equal(t, 42, reply.GetBuffer().Len())

				buffBytes := reply.GetBuffer().Bytes()
				assert.Equal(t, data.expectedData, buffBytes)
			}
		})
	}
}

package ipv6

// TODO (later) manage every IPv6 extension headers
//	* if any issue with a next header, answer icmpv6(4, 1)
//	* pop every known next header type before forwarding paylod to a protocol process

import (
	"fmt"
	"net"

	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l3/ipv4"
	"gitlab.com/go-netnode/utils/levellog"
	"gitlab.com/go-netnode/utils/pool"
)

// Process object allows to manage IPv6 packet
type Process struct {
	Addresses map[[16]byte]map[uint8]process.Object
	Protocols map[uint8]process.Object
}

// NewProcess function creates a new Process object.
func NewProcess() *Process {
	return &Process{
		Addresses: make(map[[16]byte]map[uint8]process.Object),
		Protocols: make(map[uint8]process.Object),
	}
}

// RegisterAddress method allows to register an IP address to manage
func (p *Process) RegisterAddress(address [16]byte) {
	p.Addresses[address] = make(map[uint8]process.Object)
}

// RegisterProtocol method allows to set a specific action for a protocol type
func (p *Process) RegisterProtocol(protocol uint8, pro process.Object) {
	p.Protocols[protocol] = pro
}

// RegisterAddressProtocol method allows to register a process to a specific protocol for a specific address
func (p *Process) RegisterAddressProtocol(address [16]byte, protocol uint8, pro process.Object) {
	subProcesses, exists := p.Addresses[address]

	if !exists {
		subProcesses = make(map[uint8]process.Object)
		p.Addresses[address] = subProcesses
	}

	subProcesses[protocol] = pro
}

// GetRegisteredAddressCount returns the count of registered addresse
func (p Process) GetRegisteredAddressCount() int {
	return len(p.Addresses)
}

// Process method processes an IPv6 packet
func (p Process) Process(system *process.System, data []byte, meta process.Meta) (*pool.ReusableBuffer, error) {
	if len(data)-meta.Start < 40 {
		return nil, fmt.Errorf("Expecting at least a 40 bytes data, got %d", len(data)-meta.Start)
	}

	destIndex := meta.Start + 24
	dest := data[destIndex : destIndex+16]
	subProcesses, exists := p.Addresses[*(*[16]byte)(dest)]

	if !exists {
		// this check allows to avoid to compute net.IP for nothing if debug is not enabled.
		if levellog.IsDebug() {
			p := net.IP(dest)
			levellog.Debugf("Process: discard packet, destination address %s is not set on this interface.\n", p)
		}
		return nil, nil
	}

	protocol := uint8(data[meta.Start+6])

	protocolProcess, exists := subProcesses[protocol]
	if !exists {
		protocolProcess, exists = p.Protocols[protocol]
	}

	if !exists {
		levellog.Debugf("Process: discard packet, no process registered to managed protocol %s\n", ipv4.ProtocolToString(protocol))
		return nil, nil
	}

	nextMeta := process.Meta{Start: meta.Start + 40, Previous: meta.Start}
	reBuff, err := protocolProcess.Process(system, data, nextMeta)
	if err != nil {
		return nil, fmt.Errorf("Process: An error occured while processing protocol %s: %w", ipv4.ProtocolToString(protocol), err)
	}

	if reBuff == nil {
		return nil, nil
	}

	reply := reBuff.GetBuffer()
	srcIndex := meta.Start + 8
	src := data[srcIndex : srcIndex+16]
	// TODO if multicast dest, should not use it as source
	//	be careful with icmpv6 that use dest address in checksum
	replyHeader := NewHeader(protocol, *(*[16]byte)(dest), *(*[16]byte)(src))

	err = replyHeader.Serialize(reply.GetPrepend())
	if err != nil {
		return nil, fmt.Errorf("Process: Failed to serialize header: %w", err)
	}

	return reBuff, nil
}

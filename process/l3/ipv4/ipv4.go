package ipv4

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"net"
)

// Header object represents an IPv4 packet header
type Header struct {
	VersionIHL      uint8
	DSCPECN         uint8
	TotalLength     uint16
	Identification  uint16
	FlagsOffset     uint16
	TTL             uint8
	Protocol        uint8
	Checksum        uint16
	SourceByte      [4]byte
	DestinationByte [4]byte
}

// NewHeader function generates a valid Header object
func NewHeader(protocol uint8, source, destination [4]byte) *Header {
	toRet := Header{
		VersionIHL:      0x45,
		DSCPECN:         0x0,
		Identification:  0x0, // TODO should it be randomly attributed when creating a new ip header ?
		FlagsOffset:     0x0,
		TTL:             0x40, // default ttl: 64
		Protocol:        protocol,
		Checksum:        0x0,
		SourceByte:      source,
		DestinationByte: destination,
	}

	return &toRet
}

// Serialize allows to convert object to []byte
func (i *Header) Serialize(writer io.Writer) error {
	return binary.Write(writer, binary.BigEndian, i)
}

// Deserialize allows to convert []byte into a Header object
func (i *Header) Deserialize(data []byte) error {
	if len(data) < 20 {
		return fmt.Errorf("Expecting at least a 20 bytes data, got %d", len(data))
	}

	buf := bytes.NewReader(data)
	return binary.Read(buf, binary.BigEndian, i)
}

// Version method returns Header version
func (i Header) Version() uint8 {
	return i.VersionIHL >> 4
}

// IHL method returns Header length
func (i Header) IHL() uint8 {
	return i.VersionIHL & 0x0f
}

// DSCP method returns DSCP field
func (i Header) DSCP() uint8 {
	return i.DSCPECN >> 2
}

// ECN method returns ECN field
func (i Header) ECN() uint8 {
	return i.DSCPECN & 0x03
}

// Flags method returns header flags
func (i Header) Flags() uint8 {
	return uint8(i.FlagsOffset >> 13)
}

// IsDontFragmentFlag method return true if don't fragment flag is set
func (i Header) IsDontFragmentFlag() bool {
	return i.FlagsOffset&0x4000 != 0
}

// SetDontFragmentFlag method allow to set or unset don't fragment flag
func (i *Header) SetDontFragmentFlag(value bool) {
	if value {
		i.FlagsOffset |= 0x4000
	} else {
		i.FlagsOffset &= 0xBFFF
	}
}

// IsMoreFragmentFlag method return true if more fragment flag is set
func (i Header) IsMoreFragmentFlag() bool {
	return i.FlagsOffset&0x2000 != 0
}

// SetMoreFragmentFlag method allow to set or unset don't  more fragment flag
func (i *Header) SetMoreFragmentFlag(value bool) {
	if value {
		i.FlagsOffset |= 0x2000
	} else {
		i.FlagsOffset &= 0xDFFF
	}
}

// FragmentationOffset method returns fragmentation offset
func (i Header) FragmentationOffset() uint16 {
	return i.FlagsOffset & 0x3fff
}

// Source method returns source address on a net.IP form
func (i Header) Source() *net.IP {
	addr := net.IPv4(i.SourceByte[0], i.SourceByte[1], i.SourceByte[2], i.SourceByte[3])
	return &addr
}

// Destination method returns destination address on a net.IP form
func (i Header) Destination() *net.IP {
	addr := net.IPv4(i.DestinationByte[0], i.DestinationByte[1], i.DestinationByte[2], i.DestinationByte[3])
	return &addr
}

package ipv4_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-netnode/process/l3/ipv4"
)

func TestComputeChecksum(t *testing.T) {
	checker := ipv4.Checksum{}
	sum := checker.Compute()
	assert.Equal(t, uint16(0xFFFF), sum, fmt.Sprintf("Expected 0xFFFF, got %04x", sum))
}

func TestChecksumCompute(t *testing.T) {
	checker := ipv4.Checksum{}

	// skip existing checksum
	tmp := make([]byte, len(serializedIPv4))
	copy(tmp, serializedIPv4)
	tmp[10] = 0
	tmp[11] = 0

	checker.AddData(tmp)
	sum := checker.Compute()
	assert.Equal(t, uint16(0xC4F0), sum)
}

func TestChecksumVerify(t *testing.T) {
	checker := ipv4.Checksum{}
	checker.AddData(serializedIPv4)
	assert.True(t, checker.Verify())
}

func TestChecksumReset(t *testing.T) {
	checker := ipv4.Checksum{}
	checker.AddData(serializedIPv4)
	checker.Reset()
	sum := checker.Compute()
	assert.Equal(t, uint16(0xFFFF), sum, fmt.Sprintf("Expected 0xFFFF, got %04x", sum))
}

var serializedIPv4V2 = []byte{0x45, 0x00, 0x00, 0x62, 0xbf, 0xc0, 0x40, 0x00, 0x40, 0x06, 0x00, 0x00, 0xc0, 0xa8, 0x01, 0x69, 0x23, 0xe3, 0xcf}

func TestChecksumOddLength(t *testing.T) {
	checker := ipv4.Checksum{}
	checker.AddData(serializedIPv4V2)
	sum := checker.Compute()
	assert.Equal(t, uint16(0xc5e0), sum)
}

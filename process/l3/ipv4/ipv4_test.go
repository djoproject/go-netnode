package ipv4_test

import (
	"bytes"
	"testing"

	"gitlab.com/go-netnode/process/l3/ipv4"

	"github.com/stretchr/testify/assert"
)

var serializedIPv4 = []byte{0x45, 0x00, 0x00, 0x62, 0xbf, 0xc0, 0x40, 0x00, 0x40, 0x06, 0xc4, 0xf0, 0xc0, 0xa8, 0x01, 0x69, 0x23, 0xe3, 0xcf, 0xf0}

func TestIPv4Deserialize(t *testing.T) {
	ip := ipv4.Header{}
	err := ip.Deserialize(serializedIPv4)
	assert.NoError(t, err, "ip.Deserialize")
	assert.Equal(t, uint8(4), ip.Version())
	assert.Equal(t, uint8(5), ip.IHL())
	assert.Equal(t, uint8(0), ip.DSCP())
	assert.Equal(t, uint8(0), ip.ECN())
	assert.Equal(t, uint16(98), ip.TotalLength)
	assert.Equal(t, uint16(0xbfc0), ip.Identification)
	assert.Equal(t, uint8(0x2), ip.Flags())
	assert.True(t, ip.IsDontFragmentFlag())
	assert.False(t, ip.IsMoreFragmentFlag())
	assert.Equal(t, uint16(0), ip.FragmentationOffset())
	assert.Equal(t, uint8(64), ip.TTL)
	assert.Equal(t, uint8(6), ip.Protocol)
	assert.Equal(t, uint16(0xC4F0), ip.Checksum)
	assert.Equal(t, [4]byte{192, 168, 1, 105}, ip.SourceByte)
	assert.Equal(t, [4]byte{35, 227, 207, 240}, ip.DestinationByte)
	assert.Equal(t, "192.168.1.105", ip.Source().String())
	assert.Equal(t, "35.227.207.240", ip.Destination().String())
}

func TestIPv4Serialize(t *testing.T) {
	ip := ipv4.NewHeader(ipv4.ProtocolUDP, [4]byte{1, 2, 3, 4}, [4]byte{1, 2, 3, 5})
	var binBuf bytes.Buffer
	err := ip.Serialize(&binBuf)
	assert.NoError(t, err, "ip.Serialize")
	assert.Equal(t, []byte{
		0x45, 0x00, 0x00, 0x00, // version=4, header length=5, dscp=0, ecn=0, total length=0 (not set)
		0x00, 0x00, 0x00, 0x00, // identification (not set), flags (none), fragment offset (0)
		0x40, 0x11, 0x00, 0x00, // ttl=255, protocol=11, checksum=0x0000 (not computed)
		0x01, 0x02, 0x03, 0x04, // src ip 1.2.3.4
		0x01, 0x02, 0x03, 0x05}, // dest ip 1.2.3.5
		binBuf.Bytes())
}

func TestIPv4DeserializeNotEnoughtData(t *testing.T) {
	ip := ipv4.Header{}
	err := ip.Deserialize([]byte{})
	assert.EqualError(t, err, "Expecting at least a 20 bytes data, got 0")
}

func TestIPv4SetDontFragment(t *testing.T) {
	ip := ipv4.Header{}
	assert.False(t, ip.IsDontFragmentFlag())
	ip.SetDontFragmentFlag(true)
	assert.True(t, ip.IsDontFragmentFlag())
	ip.SetDontFragmentFlag(false)
	assert.False(t, ip.IsDontFragmentFlag())
}

func TestIPv4SetMoreFragmentFlag(t *testing.T) {
	ip := ipv4.Header{}
	assert.False(t, ip.IsMoreFragmentFlag())
	ip.SetMoreFragmentFlag(true)
	assert.True(t, ip.IsMoreFragmentFlag())
	ip.SetMoreFragmentFlag(false)
	assert.False(t, ip.IsMoreFragmentFlag())
}

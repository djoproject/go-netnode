package ipv4

const (
	// ProtocolHopopt is the protocol number for Hopopt
	ProtocolHopopt uint8 = 0x00
	// ProtocolIcmp is the protocol number for icmp
	ProtocolIcmp uint8 = 0x01
	// ProtocolIgmp is the protocol number for igmp
	ProtocolIgmp uint8 = 0x02
	// ProtocolGgp is the protocol number for gpg
	ProtocolGgp uint8 = 0x03
	// ProtocolIPInIP is the protocol number for IPinIP
	ProtocolIPInIP uint8 = 0x04
	// ProtocolSt is the protocol number for st
	ProtocolSt uint8 = 0x05
	// ProtocolTCP is the protocol number for TCP
	ProtocolTCP uint8 = 0x06
	// ProtocolCbt is the protocol number for cbt
	ProtocolCbt uint8 = 0x07
	// ProtocolEgp is the protocol number for egp
	ProtocolEgp uint8 = 0x08
	// ProtocolIgp is the protocol number for igp
	ProtocolIgp uint8 = 0x09
	// ProtocolBbnRccMon is the protocol number for bbnrccmon
	ProtocolBbnRccMon uint8 = 0x0A
	// ProtocolNvpIi is the protocol number for nvpii
	ProtocolNvpIi uint8 = 0x0B
	// ProtocolPup is the protocol number for pup
	ProtocolPup uint8 = 0x0C
	// ProtocolArgus is the protocol number for argus
	ProtocolArgus uint8 = 0x0D
	// ProtocolEmcon is the protocol number for emcon
	ProtocolEmcon uint8 = 0x0E
	// ProtocolXnet is the protocol number for xnet
	ProtocolXnet uint8 = 0x0F
	// ProtocolChaos is the protocol number for chaos
	ProtocolChaos uint8 = 0x10
	// ProtocolUDP is the protocol number for udp
	ProtocolUDP uint8 = 0x11
	// ProtocolMux is the protocol number for mux
	ProtocolMux uint8 = 0x12
	// ProtocolDcnMeas is the protocol number for dcnmeas
	ProtocolDcnMeas uint8 = 0x13
	// ProtocolHmp is the protocol number for hmp
	ProtocolHmp uint8 = 0x14
	// ProtocolPrm is the protocol number for prm
	ProtocolPrm uint8 = 0x15
	// ProtocolXnsIdp is the protocol number for xnsidp
	ProtocolXnsIdp uint8 = 0x16
	// ProtocolTrunk1 is the protocol number for trunk1
	ProtocolTrunk1 uint8 = 0x17
	// ProtocolTrunk2 is the protocol number for trunk2
	ProtocolTrunk2 uint8 = 0x18
	// ProtocolLeaf1 is the protocol number for leaf1
	ProtocolLeaf1 uint8 = 0x19
	// ProtocolLeaf2 is the protocol number for leaf2
	ProtocolLeaf2 uint8 = 0x1A
	// ProtocolRdp is the protocol number for rdp
	ProtocolRdp uint8 = 0x1B
	// ProtocolIrtp is the protocol number for irtp
	ProtocolIrtp uint8 = 0x1C
	// ProtocolIsoTp4 is the protocol number for isotp4
	ProtocolIsoTp4 uint8 = 0x1D
	// ProtocolNetblt is the protocol number for netblt
	ProtocolNetblt uint8 = 0x1E
	// ProtocolMfeNsp is the protocol number for mfensp
	ProtocolMfeNsp uint8 = 0x1F
	// ProtocolMeritInp is the protocol number for meritinp
	ProtocolMeritInp uint8 = 0x20
	// ProtocolDccp is the protocol number for dccp
	ProtocolDccp uint8 = 0x21
	// ProtocolThreePc is the protocol number for threepc
	ProtocolThreePc uint8 = 0x22
	// ProtocolIdpr is the protocol number for idpr
	ProtocolIdpr uint8 = 0x23
	// ProtocolXtp is the protocol number for xtp
	ProtocolXtp uint8 = 0x24
	// ProtocolDdp is the protocol number for ddp
	ProtocolDdp uint8 = 0x25
	// ProtocolIdprCmtp is the protocol number for idprcmtp
	ProtocolIdprCmtp uint8 = 0x26
	// ProtocolTpPlusPlus is the protocol number for tpplusplus
	ProtocolTpPlusPlus uint8 = 0x27
	// ProtocolIl is the protocol number for il
	ProtocolIl uint8 = 0x28
	// ProtocolIpv6 is the protocol number for ipv6
	ProtocolIpv6 uint8 = 0x29
	// ProtocolSdrp is the protocol number for sdrp
	ProtocolSdrp uint8 = 0x2A
	// ProtocolIpv6Route is the protocol number for ipv6route
	ProtocolIpv6Route uint8 = 0x2B
	// ProtocolIpv6Frag is the protocol number for ipv6frag
	ProtocolIpv6Frag uint8 = 0x2C
	// ProtocolIdrp is the protocol number for idrp
	ProtocolIdrp uint8 = 0x2D
	// ProtocolRsvp is the protocol number for rsvp
	ProtocolRsvp uint8 = 0x2E
	// ProtocolGre is the protocol number for gre
	ProtocolGre uint8 = 0x2F
	// ProtocolDsr is the protocol number for dsr
	ProtocolDsr uint8 = 0x30
	// ProtocolBna is the protocol number for bna
	ProtocolBna uint8 = 0x31
	// ProtocolEsp is the protocol number for esp
	ProtocolEsp uint8 = 0x32
	// ProtocolAh is the protocol number for ah
	ProtocolAh uint8 = 0x33
	// ProtocolINlsp is the protocol number for inlsp
	ProtocolINlsp uint8 = 0x34
	// ProtocolSwIpe is the protocol number for swipe
	ProtocolSwIpe uint8 = 0x35
	// ProtocolNarp is the protocol number for narp
	ProtocolNarp uint8 = 0x36
	// ProtocolMobile is the protocol number for mobile
	ProtocolMobile uint8 = 0x37
	// ProtocolTlsp is the protocol number for tlsp
	ProtocolTlsp uint8 = 0x38
	// ProtocolSkip is the protocol number for skip
	ProtocolSkip uint8 = 0x39
	// ProtocolIpv6Icmp is the protocol number for ipv6icmp
	ProtocolIpv6Icmp uint8 = 0x3A
	// ProtocolIpv6NoNxt is the protocol number for ipv6nonxt
	ProtocolIpv6NoNxt uint8 = 0x3B
	// ProtocolIpv6Opts is the protocol number for ipv6opts
	ProtocolIpv6Opts uint8 = 0x3C
	// ProtocolHost is the protocol number for host
	ProtocolHost uint8 = 0x3D
	// ProtocolCftp is the protocol number for cftp
	ProtocolCftp uint8 = 0x3E
	// ProtocolLocal is the protocol number for local
	ProtocolLocal uint8 = 0x3F
	// ProtocolSatExpak is the protocol number for satexpak
	ProtocolSatExpak uint8 = 0x40
	// ProtocolKryptolan is the protocol number for kryptolan
	ProtocolKryptolan uint8 = 0x41
	// ProtocolRvd is the protocol number for rvd
	ProtocolRvd uint8 = 0x42
	// ProtocolIppc is the protocol number for ippc
	ProtocolIppc uint8 = 0x43
	// ProtocolDistributedFs is the protocol number for distributefs
	ProtocolDistributedFs uint8 = 0x44
	// ProtocolSatMon is the protocol number for satmon
	ProtocolSatMon uint8 = 0x45
	// ProtocolVisa is the protocol number for visa
	ProtocolVisa uint8 = 0x46
	// ProtocolIpcu is the protocol number for ipcu
	ProtocolIpcu uint8 = 0x47
	// ProtocolCpnx is the protocol number for cpnx
	ProtocolCpnx uint8 = 0x48
	// ProtocolCphb is the protocol number for cphb
	ProtocolCphb uint8 = 0x49
	// ProtocolWsn is the protocol number for wsn
	ProtocolWsn uint8 = 0x4A
	// ProtocolPvp is the protocol number for pvp
	ProtocolPvp uint8 = 0x4B
	// ProtocolBrSatMon is the protocol number for brsatmon
	ProtocolBrSatMon uint8 = 0x4C
	// ProtocolSunNd is the protocol number for sunnd
	ProtocolSunNd uint8 = 0x4D
	// ProtocolWbMon is the protocol number for wbmon
	ProtocolWbMon uint8 = 0x4E
	// ProtocolWbExpak is the protocol number for wbexpak
	ProtocolWbExpak uint8 = 0x4F
	// ProtocolIsoIP is the protocol number for isoip
	ProtocolIsoIP uint8 = 0x50
	// ProtocolVmtp is the protocol number for vmtp
	ProtocolVmtp uint8 = 0x51
	// ProtocolSecureVmtp is the protocol number for securevmtp
	ProtocolSecureVmtp uint8 = 0x52
	// ProtocolVines is the protocol number for vines
	ProtocolVines uint8 = 0x53
	// ProtocolTtp is the protocol number for ttp
	ProtocolTtp uint8 = 0x54
	// ProtocolIptm is the protocol number for iptm
	ProtocolIptm uint8 = 0x54
	// ProtocolNsfnetIgp is the protocol number for igp
	ProtocolNsfnetIgp uint8 = 0x55
	// ProtocolDgp is the protocol number for dgp
	ProtocolDgp uint8 = 0x56
	// ProtocolTcf is the protocol number for tcf
	ProtocolTcf uint8 = 0x57
	// ProtocolEigrp is the protocol number for eigrp
	ProtocolEigrp uint8 = 0x58
	// ProtocolOspf is the protocol number for ospf
	ProtocolOspf uint8 = 0x59
	// ProtocolSpriteRPC is the protocol number for spriterpc
	ProtocolSpriteRPC uint8 = 0x5A
	// ProtocolLarp is the protocol number for larp
	ProtocolLarp uint8 = 0x5B
	// ProtocolMtp is the protocol number for mtp
	ProtocolMtp uint8 = 0x5C
	// ProtocolAx25 is the protocol number for ax25
	ProtocolAx25 uint8 = 0x5D
	// ProtocolOs is the protocol number for os
	ProtocolOs uint8 = 0x5E
	// ProtocolMicp is the protocol number for micp
	ProtocolMicp uint8 = 0x5F
	// ProtocolSccSp is the protocol number for sccsp
	ProtocolSccSp uint8 = 0x60
	// ProtocolEtherip is the protocol number for etherip
	ProtocolEtherip uint8 = 0x61
	// ProtocolEncap is the protocol number for encap
	ProtocolEncap uint8 = 0x62
	// ProtocolPrivEnc is the protocol number for privenc
	ProtocolPrivEnc uint8 = 0x63
	// ProtocolGmtp is the protocol number for gmtp
	ProtocolGmtp uint8 = 0x64
	// ProtocolIfmp is the protocol number for ifmp
	ProtocolIfmp uint8 = 0x65
	// ProtocolPnni is the protocol number for pnni
	ProtocolPnni uint8 = 0x66
	// ProtocolPim is the protocol number for pim
	ProtocolPim uint8 = 0x67
	// ProtocolAris is the protocol number for aris
	ProtocolAris uint8 = 0x68
	// ProtocolScps is the protocol number for scps
	ProtocolScps uint8 = 0x69
	// ProtocolQnx is the protocol number for qnx
	ProtocolQnx uint8 = 0x6A
	// ProtocolAN is the protocol number for an
	ProtocolAN uint8 = 0x6B
	// ProtocolIpcomp is the protocol number for ipcomp
	ProtocolIpcomp uint8 = 0x6C
	// ProtocolSnp is the protocol number for snp
	ProtocolSnp uint8 = 0x6D
	// ProtocolCompaqPeer is the protocol number for compaqpeer
	ProtocolCompaqPeer uint8 = 0x6E
	// ProtocolIpxInIP is the protocol number for ipxninip
	ProtocolIpxInIP uint8 = 0x6F
	// ProtocolVrrp is the protocol number for vrrp
	ProtocolVrrp uint8 = 0x70
	// ProtocolPgm is the protocol number for pgm
	ProtocolPgm uint8 = 0x71
	// ProtocolZeroHop is the protocol number for zerohop
	ProtocolZeroHop uint8 = 0x72
	// ProtocolL2Tp is the protocol number for l2tp
	ProtocolL2Tp uint8 = 0x73
	// ProtocolDdx is the protocol number for ddx
	ProtocolDdx uint8 = 0x74
	// ProtocolIatp is the protocol number for iatp
	ProtocolIatp uint8 = 0x75
	// ProtocolStp is the protocol number for stp
	ProtocolStp uint8 = 0x76
	// ProtocolSrp is the protocol number for srp
	ProtocolSrp uint8 = 0x77
	// ProtocolUti is the protocol number for uti
	ProtocolUti uint8 = 0x78
	// ProtocolSmp is the protocol number for smp
	ProtocolSmp uint8 = 0x79
	// ProtocolSm is the protocol number for sm
	ProtocolSm uint8 = 0x7A
	// ProtocolPtp is the protocol number for ptp
	ProtocolPtp uint8 = 0x7B
	// ProtocolIsIsOverIpv4 is the protocol number for isisoveripv4
	ProtocolIsIsOverIpv4 uint8 = 0x7C
	// ProtocolFire is the protocol number for fire
	ProtocolFire uint8 = 0x7D
	// ProtocolCrtp is the protocol number for crtp
	ProtocolCrtp uint8 = 0x7E
	// ProtocolCrudp is the protocol number for crudp
	ProtocolCrudp uint8 = 0x7F
	// ProtocolSscopmce is the protocol number for sscopmce
	ProtocolSscopmce uint8 = 0x80
	// ProtocolIplt is the protocol number for iplt
	ProtocolIplt uint8 = 0x81
	// ProtocolSps is the protocol number for sps
	ProtocolSps uint8 = 0x82
	// ProtocolPipe is the protocol number for pipe
	ProtocolPipe uint8 = 0x83
	// ProtocolSctp is the protocol number for sctp
	ProtocolSctp uint8 = 0x84
	// ProtocolFc is the protocol number for fc
	ProtocolFc uint8 = 0x85
	// ProtocolRsvpE2EIgnore is the protocol number for rsvpe2eignore
	ProtocolRsvpE2EIgnore uint8 = 0x86
	// ProtocolMobilityHeader is the protocol number for mobilityheader
	ProtocolMobilityHeader uint8 = 0x87
	// ProtocolUdplite is the protocol number for udplite
	ProtocolUdplite uint8 = 0x88
	// ProtocolMplsInIP is the protocol number for mplsinip
	ProtocolMplsInIP uint8 = 0x89
	// ProtocolManet is the protocol number for manet
	ProtocolManet uint8 = 0x8A
	// ProtocolHip is the protocol number for hip
	ProtocolHip uint8 = 0x8B
	// ProtocolShim6 is the protocol number for shim6
	ProtocolShim6 uint8 = 0x8C
	// ProtocolWesp is the protocol number for wesp
	ProtocolWesp uint8 = 0x8D
	// ProtocolRohc is the protocol number for rohc
	ProtocolRohc uint8 = 0x8E
	// ProtocolEthernet is the protocol number for ethernet
	ProtocolEthernet uint8 = 0x8F
	// ProtocolReserved is reserved value
	ProtocolReserved uint8 = 0xFF
)

// AllProtocol variable contains every existing protocols
var AllProtocol = []uint8{ProtocolHopopt, ProtocolIcmp, ProtocolIgmp, ProtocolGgp, ProtocolIPInIP, ProtocolSt, ProtocolTCP, ProtocolCbt, ProtocolEgp, ProtocolIgp, ProtocolBbnRccMon, ProtocolNvpIi, ProtocolPup, ProtocolArgus, ProtocolEmcon, ProtocolXnet, ProtocolChaos, ProtocolUDP, ProtocolMux, ProtocolDcnMeas, ProtocolHmp, ProtocolPrm, ProtocolXnsIdp, ProtocolTrunk1, ProtocolTrunk2, ProtocolLeaf1, ProtocolLeaf2, ProtocolRdp, ProtocolIrtp, ProtocolIsoTp4, ProtocolNetblt, ProtocolMfeNsp, ProtocolMeritInp, ProtocolDccp, ProtocolThreePc, ProtocolIdpr, ProtocolXtp, ProtocolDdp, ProtocolIdprCmtp, ProtocolTpPlusPlus, ProtocolIl, ProtocolIpv6, ProtocolSdrp, ProtocolIpv6Route, ProtocolIpv6Frag, ProtocolIdrp, ProtocolRsvp, ProtocolGre, ProtocolDsr, ProtocolBna, ProtocolEsp, ProtocolAh, ProtocolINlsp, ProtocolSwIpe, ProtocolNarp, ProtocolMobile, ProtocolTlsp, ProtocolSkip, ProtocolIpv6Icmp, ProtocolIpv6NoNxt, ProtocolIpv6Opts, ProtocolHost, ProtocolCftp, ProtocolLocal, ProtocolSatExpak, ProtocolKryptolan, ProtocolRvd, ProtocolIppc, ProtocolDistributedFs, ProtocolSatMon, ProtocolVisa, ProtocolIpcu, ProtocolCpnx, ProtocolCphb, ProtocolWsn, ProtocolPvp, ProtocolBrSatMon, ProtocolSunNd, ProtocolWbMon, ProtocolWbExpak, ProtocolIsoIP, ProtocolVmtp, ProtocolSecureVmtp, ProtocolVines, ProtocolTtp, ProtocolIptm, ProtocolNsfnetIgp, ProtocolDgp, ProtocolTcf, ProtocolEigrp, ProtocolOspf, ProtocolSpriteRPC, ProtocolLarp, ProtocolMtp, ProtocolAx25, ProtocolOs, ProtocolMicp, ProtocolSccSp, ProtocolEtherip, ProtocolEncap, ProtocolPrivEnc, ProtocolGmtp, ProtocolIfmp, ProtocolPnni, ProtocolPim, ProtocolAris, ProtocolScps, ProtocolQnx, ProtocolAN, ProtocolIpcomp, ProtocolSnp, ProtocolCompaqPeer, ProtocolIpxInIP, ProtocolVrrp, ProtocolPgm, ProtocolZeroHop, ProtocolL2Tp, ProtocolDdx, ProtocolIatp, ProtocolStp, ProtocolSrp, ProtocolUti, ProtocolSmp, ProtocolSm, ProtocolPtp, ProtocolIsIsOverIpv4, ProtocolFire, ProtocolCrtp, ProtocolCrudp, ProtocolSscopmce, ProtocolIplt, ProtocolSps, ProtocolPipe, ProtocolSctp, ProtocolFc, ProtocolRsvpE2EIgnore, ProtocolMobilityHeader, ProtocolUdplite, ProtocolMplsInIP, ProtocolManet, ProtocolHip, ProtocolShim6, ProtocolWesp, ProtocolRohc, ProtocolEthernet, ProtocolReserved}

// ProtocolToString convert a protocol value to a string
func ProtocolToString(protocol uint8) string {
	switch protocol {
	case 0x00:
		return "HOPOPT"
	case 0x01:
		return "ICMP"
	case 0x02:
		return "IGMP"
	case 0x03:
		return "GGP"
	case 0x04:
		return "IP_in_IP"
	case 0x05:
		return "ST"
	case 0x06:
		return "TCP"
	case 0x07:
		return "CBT"
	case 0x08:
		return "EGP"
	case 0x09:
		return "IGP"
	case 0x0A:
		return "BBN_RCC_MON"
	case 0x0B:
		return "NVP_II"
	case 0x0C:
		return "PUP"
	case 0x0D:
		return "ARGUS"
	case 0x0E:
		return "EMCON"
	case 0x0F:
		return "XNET"
	case 0x10:
		return "CHAOS"
	case 0x11:
		return "UDP"
	case 0x12:
		return "MUX"
	case 0x13:
		return "DCN_MEAS"
	case 0x14:
		return "HMP"
	case 0x15:
		return "PRM"
	case 0x16:
		return "XNS_IDP"
	case 0x17:
		return "TRUNK_1"
	case 0x18:
		return "TRUNK_2"
	case 0x19:
		return "LEAF_1"
	case 0x1A:
		return "LEAF_2"
	case 0x1B:
		return "RDP"
	case 0x1C:
		return "IRTP"
	case 0x1D:
		return "ISO_TP4"
	case 0x1E:
		return "NETBLT"
	case 0x1F:
		return "MFE_NSP"
	case 0x20:
		return "MERIT_INP"
	case 0x21:
		return "DCCP"
	case 0x22:
		return "THREE_PC"
	case 0x23:
		return "IDPR"
	case 0x24:
		return "XTP"
	case 0x25:
		return "DDP"
	case 0x26:
		return "IDPR_CMTP"
	case 0x27:
		return "TP_PLUS_PLUS"
	case 0x28:
		return "IL"
	case 0x29:
		return "IPv6"
	case 0x2A:
		return "SDRP"
	case 0x2B:
		return "IPv6_Route"
	case 0x2C:
		return "IPv6_Frag"
	case 0x2D:
		return "IDRP"
	case 0x2E:
		return "RSVP"
	case 0x2F:
		return "GRE"
	case 0x30:
		return "DSR"
	case 0x31:
		return "BNA"
	case 0x32:
		return "ESP"
	case 0x33:
		return "AH"
	case 0x34:
		return "I_NLSP"
	case 0x35:
		return "SwIPe"
	case 0x36:
		return "NARP"
	case 0x37:
		return "MOBILE"
	case 0x38:
		return "TLSP"
	case 0x39:
		return "SKIP"
	case 0x3A:
		return "IPv6_ICMP"
	case 0x3B:
		return "IPv6_NoNxt"
	case 0x3C:
		return "IPv6_Opts"
	case 0x3D:
		return "HOST"
	case 0x3E:
		return "CFTP"
	case 0x3F:
		return "LOCAL"
	case 0x40:
		return "SAT_EXPAK"
	case 0x41:
		return "KRYPTOLAN"
	case 0x42:
		return "RVD"
	case 0x43:
		return "IPPC"
	case 0x44:
		return "DISTRIBUTED_FS"
	case 0x45:
		return "SAT_MON"
	case 0x46:
		return "VISA"
	case 0x47:
		return "IPCU"
	case 0x48:
		return "CPNX"
	case 0x49:
		return "CPHB"
	case 0x4A:
		return "WSN"
	case 0x4B:
		return "PVP"
	case 0x4C:
		return "BR_SAT_MON"
	case 0x4D:
		return "SUN_ND"
	case 0x4E:
		return "WB_MON"
	case 0x4F:
		return "WB_EXPAK"
	case 0x50:
		return "ISO_IP"
	case 0x51:
		return "VMTP"
	case 0x52:
		return "SECURE_VMTP"
	case 0x53:
		return "VINES"
	case 0x54:
		return "TTP or IPTM"
	case 0x55:
		return "NSFNET_IGP"
	case 0x56:
		return "DGP"
	case 0x57:
		return "TCF"
	case 0x58:
		return "EIGRP"
	case 0x59:
		return "OSPF"
	case 0x5A:
		return "Sprite_RPC"
	case 0x5B:
		return "LARP"
	case 0x5C:
		return "MTP"
	case 0x5D:
		return "AX_25"
	case 0x5E:
		return "OS"
	case 0x5F:
		return "MICP"
	case 0x60:
		return "SCC_SP"
	case 0x61:
		return "ETHERIP"
	case 0x62:
		return "ENCAP"
	case 0x63:
		return "PRIV_ENC"
	case 0x64:
		return "GMTP"
	case 0x65:
		return "IFMP"
	case 0x66:
		return "PNNI"
	case 0x67:
		return "PIM"
	case 0x68:
		return "ARIS"
	case 0x69:
		return "SCPS"
	case 0x6A:
		return "QNX"
	case 0x6B:
		return "A_N"
	case 0x6C:
		return "IPComp"
	case 0x6D:
		return "SNP"
	case 0x6E:
		return "Compaq_Peer"
	case 0x6F:
		return "IPX_in_IP"
	case 0x70:
		return "VRRP"
	case 0x71:
		return "PGM"
	case 0x72:
		return "ZERO_HOP"
	case 0x73:
		return "L2TP"
	case 0x74:
		return "DDX"
	case 0x75:
		return "IATP"
	case 0x76:
		return "STP"
	case 0x77:
		return "SRP"
	case 0x78:
		return "UTI"
	case 0x79:
		return "SMP"
	case 0x7A:
		return "SM"
	case 0x7B:
		return "PTP"
	case 0x7C:
		return "IS_IS_over_IPv4"
	case 0x7D:
		return "FIRE"
	case 0x7E:
		return "CRTP"
	case 0x7F:
		return "CRUDP"
	case 0x80:
		return "SSCOPMCE"
	case 0x81:
		return "IPLT"
	case 0x82:
		return "SPS"
	case 0x83:
		return "PIPE"
	case 0x84:
		return "SCTP"
	case 0x85:
		return "FC"
	case 0x86:
		return "RSVP_E2E_IGNORE"
	case 0x87:
		return "Mobility_Header"
	case 0x88:
		return "UDPLite"
	case 0x89:
		return "MPLS_in_IP"
	case 0x8A:
		return "manet"
	case 0x8B:
		return "HIP"
	case 0x8C:
		return "Shim6"
	case 0x8D:
		return "WESP"
	case 0x8E:
		return "ROHC"
	case 0x8F:
		return "Ethernet"
	case 0xFD:
		return "TESTING_FD"
	case 0xFE:
		return "TESTING_FE"
	case 0xFF:
		return "Reserved"
	}

	return "Unassigned"
}

package ipv4

import (
	"fmt"

	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/utils/levellog"
	"gitlab.com/go-netnode/utils/pool"
)

// TODO manage fragmentation, but later
//	only need to fragment if output interface has a lowest mtu
//	never need to reassemble.  why ?
//
//	it happens when the upper layer want to answer more byte than the MTU.
//	Currently, it is going to trigger a buffer full error.
//
//	IDEA 1: as soon as a layer detect that the buffer is going to be full, it
//	has to ask for a next buffer to write its next data part, lower layer able
//	to split it would have to distribute it.  Right now, only ipv4 has to do that
//	A layer like ethernet would trigger an error if too much data has to be sent
//	in a single frame.
//
//	IDEA 2: include that feature into buffer mechanism, chain to a second buffer
//	if the first one is full, allow to chain several buffer before trigger an error
//	the lower layer has to divide data into separate frame.  Right now only IPv4
//	is allowed to do that
//	could start with a pool of buffer, and add more at runtime if needed, then put
//	them back into the pool of available buffer
//
//  IDEA2b: same as previous but use slice feature to expand current buffer in place
//	of chaining it.
//	drawback: data are going to be moved in memory at each writting
//
//	IDEA3: create a fragment task that is going to split then send the packet to fragment.

// Process object allows to manage IPv4 packet
type Process struct {
	Addresses map[[4]byte]map[uint8]process.Object
	Protocols map[uint8]process.Object
}

// NewProcess function creates a new Process object.
func NewProcess() *Process {
	return &Process{
		Addresses: make(map[[4]byte]map[uint8]process.Object),
		Protocols: make(map[uint8]process.Object),
	}
}

// RegisterAddress method allows to register an IP address to manage
func (p *Process) RegisterAddress(address [4]byte) {
	p.Addresses[address] = make(map[uint8]process.Object)
}

// RegisterProtocol method allows to set a specific action for a protocol type
func (p *Process) RegisterProtocol(protocol uint8, pro process.Object) {
	p.Protocols[protocol] = pro
}

// RegisterAddressProtocol method allows to register a process to a specific protocol for a specific address
func (p *Process) RegisterAddressProtocol(address [4]byte, protocol uint8, pro process.Object) {
	subProcesses, exists := p.Addresses[address]

	if !exists {
		subProcesses = make(map[uint8]process.Object)
		p.Addresses[address] = subProcesses
	}

	subProcesses[protocol] = pro
}

// GetRegisteredAddressCount returns the count of registered addresses
func (p Process) GetRegisteredAddressCount() int {
	return len(p.Addresses)
}

// Process method processes an IPv4 packet
func (p Process) Process(system *process.System, data []byte, meta process.Meta) (*pool.ReusableBuffer, error) {
	header := Header{}
	err := header.Deserialize(data[meta.Start:])

	if err != nil {
		return nil, fmt.Errorf("Process: Fail to deserialize IPv4 header: %w", err)
	}

	headerLength := int(header.IHL() * 4)

	if headerLength > len(data)-meta.Start {
		levellog.Debugln("Process: discard packet, header length is bigger than available bytes.")
		return nil, nil
	}

	checker := Checksum{}
	checker.AddData(data[meta.Start : meta.Start+headerLength])

	if !checker.Verify() {
		levellog.Debugln("Process: discard packet, invalid checkum")
		return nil, nil
	}

	subProcesses, exists := p.Addresses[header.DestinationByte]

	if !exists {
		levellog.Debugf("Process: discard packet, destination address %s is not set on this interface.\n", header.Destination())
		return nil, nil
	}

	protocolProcess, exists := subProcesses[header.Protocol]
	if !exists {
		protocolProcess, exists = p.Protocols[header.Protocol]
	}

	if !exists {
		levellog.Debugf("Process: discard packet, no process registered to managed protocol %s\n", ProtocolToString(header.Protocol))
		return nil, nil
	}

	nextMeta := process.Meta{Start: meta.Start + headerLength, Previous: meta.Start}
	reBuff, err := protocolProcess.Process(system, data, nextMeta)
	if err != nil {
		return nil, fmt.Errorf("Process: An error occured while processing protocol %d: %w", header.Protocol, err)
	}

	if reBuff == nil {
		return nil, nil
	}

	reply := reBuff.GetBuffer()

	// TODO if multicast dest, should probably not use it as source

	replyHeader := NewHeader(header.Protocol, header.DestinationByte, header.SourceByte)
	replyHeader.TotalLength = 20 + uint16(reply.Len())

	err = replyHeader.Serialize(reply.GetPrepend())
	if err != nil {
		return nil, fmt.Errorf("Process: Failed to serialize header: %w", err)
	}

	// Compute checksum on output buffer
	buffBytes := reply.Bytes()
	checker.Reset()
	checker.AddData(buffBytes[:20])
	sum := checker.Compute()
	buffBytes[10] = byte(sum >> 8)
	buffBytes[11] = byte(sum & 0x00FF)

	return reBuff, nil
}

package ipv4_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-netnode/process/l3/ipv4"
)

func TestIpv4ProtocolGetString(t *testing.T) {
	for i := 0; i < 256; i++ {
		assert.NotEmpty(t, ipv4.ProtocolToString(uint8(i)))
	}
}

func TestExistingProtocols(t *testing.T) {
	for _, protocol := range ipv4.AllProtocol {
		protocolString := ipv4.ProtocolToString(protocol)
		assert.NotEmpty(t, protocolString)
		assert.NotEqual(t, "Unassigned", protocolString)
	}
}

package ipv4_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-netnode/process/l3/ipv4"
)

func TestGetBroadcastLimited(t *testing.T) {
	brd := ipv4.GetBroadcast([4]byte{0, 0, 0, 0}, 0)
	assert.Equal(t, [4]byte{255, 255, 255, 255}, brd)
}

func TestGetBroadcastUnicast(t *testing.T) {
	brd := ipv4.GetBroadcast([4]byte{1, 1, 1, 1}, 32)
	assert.Equal(t, [4]byte{1, 1, 1, 1}, brd)
}

func TestGetBroadcast16range(t *testing.T) {
	brd := ipv4.GetBroadcast([4]byte{1, 1, 1, 1}, 16)
	assert.Equal(t, [4]byte{1, 1, 255, 255}, brd)
}

func TestGetBroadcast12range(t *testing.T) {
	brd := ipv4.GetBroadcast([4]byte{1, 1, 1, 1}, 12)
	assert.Equal(t, [4]byte{1, 15, 255, 255}, brd)
}

func TestIsInRangeNot(t *testing.T) {
	assert.False(t, ipv4.IsInRange([4]byte{1, 1, 0, 0}, 18, [4]byte{2, 2, 2, 2}))
}

func TestIsInRange(t *testing.T) {
	assert.True(t, ipv4.IsInRange([4]byte{1, 1, 0, 0}, 18, [4]byte{1, 1, 1, 2}))
}

func TestIsInRange2(t *testing.T) {
	assert.True(t, ipv4.IsInRange([4]byte{192, 168, 0, 0}, 16, [4]byte{192, 168, 1, 1}))
}

func TestIsInRangeFull(t *testing.T) {
	assert.True(t, ipv4.IsInRange([4]byte{1, 1, 1, 1}, 32, [4]byte{1, 1, 1, 1}))
}

func TestIsInMulticastRange(t *testing.T) {
	assert.True(t, ipv4.IsInMulticastRange([4]byte{224, 0, 0, 1}))
}

func TestIsInMulticastRangeNot(t *testing.T) {
	assert.False(t, ipv4.IsInMulticastRange([4]byte{192, 168, 1, 1}))
}

func TestIsInRestrictedRange(t *testing.T) {
	assert.True(t, ipv4.IsInRestrictedRange([4]byte{0, 0, 0, 1}))
}

func TestIsInRestrictedRangeNot(t *testing.T) {
	assert.False(t, ipv4.IsInRestrictedRange([4]byte{192, 168, 1, 1}))
}

func TestIsInLinkLocalRange(t *testing.T) {
	assert.True(t, ipv4.IsInLinkLocalRange([4]byte{169, 254, 1, 1}))
}

func TestIsInLinkLocalRangeNot(t *testing.T) {
	assert.False(t, ipv4.IsInLinkLocalRange([4]byte{192, 168, 1, 1}))
}

func TestIsInPrivateRange(t *testing.T) {
	assert.True(t, ipv4.IsInPrivateRange([4]byte{192, 168, 1, 1}))
}

func TestIsInPrivateRangeNot(t *testing.T) {
	assert.False(t, ipv4.IsInPrivateRange([4]byte{155, 168, 1, 1}))
}

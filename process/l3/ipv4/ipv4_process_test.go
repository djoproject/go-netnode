package ipv4_test

import (
	"bytes"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l3/ipv4"
	"gitlab.com/go-netnode/process/processtest"
	"gitlab.com/go-netnode/utils/pool"
)

//// PROCESSES ///////////////////////////////////////////////////////////////

type emptyProcess struct{}

func (e emptyProcess) Process(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error) {
	return nil, nil
}

type errorProcess struct{}

func (e errorProcess) Process(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error) {
	return nil, errors.New("boom")
}

type dataProcess struct{}

func (e dataProcess) Process(system *process.System, _ []byte, _ process.Meta) (*pool.ReusableBuffer, error) {
	reBuff := system.Device.GetBufferPool().GetBuffer()
	buf := reBuff.GetBuffer()

	d := []byte{0x42, 0xff}
	_, err := buf.GetPrepend().Write(d)
	return reBuff, err
}

//// TESTS ///////////////////////////////////////////////////////////////////

func TestProcessRegisterAddress(t *testing.T) {
	i := ipv4.NewProcess()
	i.RegisterAddress([4]byte{1, 2, 3, 4})
	require.Equal(t, 1, i.GetRegisteredAddressCount())
	_, exists := i.Addresses[[4]byte{1, 2, 3, 4}]
	assert.True(t, exists)
}

func TestProcessRegisterProtocol(t *testing.T) {
	i := ipv4.NewProcess()
	i.RegisterProtocol(ipv4.ProtocolIgmp, &emptyProcess{})
	_, exists := i.Protocols[ipv4.ProtocolIgmp]
	assert.True(t, exists)
}

func TestProcessRegisterAddressProtocol(t *testing.T) {
	i := ipv4.NewProcess()
	i.RegisterAddressProtocol([4]byte{1, 2, 3, 4}, ipv4.ProtocolIgmp, &emptyProcess{})
	_, exists := i.Protocols[ipv4.ProtocolIgmp]
	assert.False(t, exists)

	subPro, exists := i.Addresses[[4]byte{1, 2, 3, 4}]
	require.True(t, exists)
	assert.NotNil(t, subPro)

	pro, exists := subPro[ipv4.ProtocolIgmp]
	assert.True(t, exists)
	assert.NotNil(t, pro)
}

func TestProcessNotEnoughBytes(t *testing.T) {
	i := ipv4.NewProcess()
	b := make([]byte, 19)
	reply, err := i.Process(nil, b, process.Meta{})
	assert.EqualError(t, err, "Process: Fail to deserialize IPv4 header: Expecting at least a 20 bytes data, got 19")
	assert.Nil(t, reply)
}

func TestProcessHeaderLengthTooLong(t *testing.T) {
	i := ipv4.NewProcess()
	b := make([]byte, 20)
	b[0] = 0x46
	reply, err := i.Process(nil, b, process.Meta{})
	assert.NoError(t, err)
	assert.Nil(t, reply)
}

func TestProcessInvalidChecksum(t *testing.T) {
	i := ipv4.NewProcess()
	b := make([]byte, 20)
	b[0] = 0x45
	reply, err := i.Process(nil, b, process.Meta{})
	assert.NoError(t, err)
	assert.Nil(t, reply)
}

var datas = map[string]struct {
	addressToRegister  [][4]byte
	protocolToRegister map[uint8]process.Object
	bufferSize         int
	expectedError      string
	expectedData       []byte
}{
	"NoAddressTargeted": {
		addressToRegister:  nil,
		protocolToRegister: nil,
		bufferSize:         100,
		expectedError:      "",
		expectedData:       nil,
	},
	"NoProtocolTargeted": {
		addressToRegister:  [][4]byte{[4]byte{1, 1, 1, 2}},
		protocolToRegister: nil,
		bufferSize:         100,
		expectedError:      "",
		expectedData:       nil,
	},
	"ProtocolOutputBufferTooSmall": {
		addressToRegister:  [][4]byte{[4]byte{1, 1, 1, 2}},
		protocolToRegister: map[uint8]process.Object{ipv4.ProtocolTCP: &dataProcess{}},
		bufferSize:         4,
		expectedError:      "Process: Failed to serialize header: Buffer: not enough space",
		expectedData:       nil,
	},
	"ProtocolError": {
		addressToRegister:  [][4]byte{[4]byte{1, 1, 1, 2}},
		protocolToRegister: map[uint8]process.Object{ipv4.ProtocolTCP: &errorProcess{}},
		bufferSize:         100,
		expectedError:      "Process: An error occured while processing protocol 6: boom",
		expectedData:       nil,
	},
	"ProtocolNoAnswer": {
		addressToRegister:  [][4]byte{[4]byte{1, 1, 1, 2}},
		protocolToRegister: map[uint8]process.Object{ipv4.ProtocolTCP: &emptyProcess{}},
		bufferSize:         100,
		expectedError:      "",
		expectedData:       nil,
	},
	"ProtocolSuccess": {
		addressToRegister:  [][4]byte{[4]byte{1, 1, 1, 2}},
		protocolToRegister: map[uint8]process.Object{ipv4.ProtocolTCP: &dataProcess{}},
		bufferSize:         100,
		expectedError:      "",
		expectedData: []byte{
			0x45, 0x00, 0x00, 0x16, // version_ihl(1) + TOS(1) + total_length(2)
			0x00, 0x00, 0x00, 0x00, // id (2), flags+frag_offset (2)
			0x40, 0x06, 0x76, 0xde, // TTL(1), protocol(1), checksum(2)
			0x01, 0x01, 0x01, 0x02, // dest
			0x01, 0x01, 0x01, 0x01, // src
			0x42, 0xff}, // payload
	},
}

func TestIPv4Process(t *testing.T) {
	for name, data := range datas {
		t.Run(name, func(t *testing.T) {
			// forge packet
			packet := ipv4.NewHeader(ipv4.ProtocolTCP, [4]byte{1, 1, 1, 1}, [4]byte{1, 1, 1, 2})
			var binBuf bytes.Buffer
			err := packet.Serialize(&binBuf)
			require.NoError(t, err, "Serialize")
			b := binBuf.Bytes()

			// compute checksum
			checker := ipv4.Checksum{}
			checker.AddData(b)
			sum := checker.Compute()
			b[10] = byte(sum >> 8)
			b[11] = byte(sum & 0x00FF)

			// process packet
			i := ipv4.NewProcess()

			for prot, handler := range data.protocolToRegister {
				i.RegisterProtocol(prot, handler)
			}

			for _, addr := range data.addressToRegister {
				i.RegisterAddress(addr)
			}
			require.Equal(t, len(data.addressToRegister), i.GetRegisteredAddressCount())

			pdata := processtest.ProduceProcessData(t, data.bufferSize)
			reply, err := i.Process(pdata, b, process.Meta{})

			if len(data.expectedError) == 0 {
				assert.NoError(t, err)
			} else {
				assert.EqualError(t, err, data.expectedError)
			}

			if data.expectedData == nil {
				require.Nil(t, reply)
			} else {
				require.NotNil(t, reply)
				assert.Equal(t, 22, reply.GetBuffer().Len())

				// check data in buffer
				buffBytes := reply.GetBuffer().Bytes()
				checker.Reset()
				checker.AddData(buffBytes[:20])
				assert.True(t, checker.Verify())
				assert.Equal(t, data.expectedData, buffBytes)
			}
		})
	}
}

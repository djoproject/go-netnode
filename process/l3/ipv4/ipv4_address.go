package ipv4

// LimitedBroadcast address represents the limited broadcast address
var LimitedBroadcast = [4]byte{255, 255, 255, 255}

// AllHosts address represents the every hosts multicast address
var AllHosts = [4]byte{224, 0, 0, 1}

// AllRouters address represents the every routers multicast address
var AllRouters = [4]byte{224, 0, 0, 2}

// GetBroadcast function returns the broadcast address for a subnet
func GetBroadcast(rang [4]byte, mask uint8) [4]byte {
	broadcast := LimitedBroadcast
	for i := 0; i < 4; i++ {
		if mask >= 8 {
			broadcast[i] = rang[i]
			mask -= 8
			continue
		}

		broadcast[i] = rang[i] | ((1 << (8 - mask)) - 1)
		break
	}

	return broadcast
}

// IsInRange function returns true if an address is inside a subnet
func IsInRange(rang [4]byte, mask uint8, address [4]byte) bool {
	for i := 0; i < 4; i++ {
		if mask >= 8 {
			if rang[i] != address[i] {
				return false
			}

			mask -= 8
			continue
		}

		if mask == 0 {
			break
		}

		submask := byte(^((1 << mask) - 1))
		return rang[i]&submask == address[i]&submask
	}

	return true
}

// IsInMulticastRange function returns true if the address is a multicast
// address
func IsInMulticastRange(address [4]byte) bool {
	// Multicast range: 224.0.0.0/4
	return address[0]&0xF0 == 224
}

// IsInRestrictedRange function returns true if an address is into any
// restricted range
func IsInRestrictedRange(address [4]byte) bool {
	// Restricted ranges:
	//	*   0.0.0.0/8
	//  * 127.0.0.0/8
	//  * 240.0.0.0/8
	//  * ...
	//  * 255.0.0.0/8
	return address[0] == 0 || address[0] == 127 || address[0]&0xF0 == 240
}

// IsInLinkLocalRange function returns true if an address is in link local
// range
func IsInLinkLocalRange(address [4]byte) bool {
	// link local range
	// 169.254.0.0/16
	return address[0] == 169 && address[1] == 254
}

// IsInPrivateRange function returns true if an address is in private range
func IsInPrivateRange(address [4]byte) bool {
	// 10.0.0.0/8
	// 172.16.0.0/12
	// 192.168.0.0/16
	return IsInRange([4]byte{10, 0, 0, 0}, 8, address) ||
		IsInRange([4]byte{172, 16, 0, 0}, 12, address) ||
		IsInRange([4]byte{192, 168, 0, 0}, 16, address)
}

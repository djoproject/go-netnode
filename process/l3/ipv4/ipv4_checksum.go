package ipv4

// Checksum object allows to compute an IPv4 or ICMP checksum
type Checksum struct {
	sum uint32
}

// AddData method allows to add data to be taken into account while computing checksum
func (i *Checksum) AddData(data []byte) {
	for index := 0; index < len(data)-1; index += 2 {
		word := uint32(data[index])<<8 + uint32(data[index+1])
		i.sum += word
	}

	// manage case when data has an odd length
	if len(data)%2 == 1 {
		word := uint32(data[len(data)-1]) << 8
		i.sum += word
	}
}

func (i Checksum) computeSum() uint16 {
	sum := i.sum
	for sum > 0xFFFF {
		carry := sum >> 16
		sum = sum & 0xFFFF
		sum += carry
	}

	return uint16(sum)
}

// Compute method returns the checksum result
func (i Checksum) Compute() uint16 {
	return 0xFFFF ^ i.computeSum()
}

// Verify method check if the checksum is valid
func (i Checksum) Verify() bool {
	return i.computeSum() == 0xFFFF
}

// Reset method allow to reuse this object for the next checksum
func (i *Checksum) Reset() {
	i.sum = 0
}

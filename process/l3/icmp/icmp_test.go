package icmp_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-netnode/process/l3/icmp"
)

var serializedICMP = []byte{0x43, 0x02, 0x00, 0x00}

func TestICMPDeserialize(t *testing.T) {
	i := icmp.Header{}
	err := i.Deserialize(serializedICMP)
	assert.NoError(t, err, "icmp.Deserialize")
	assert.Equal(t, byte(0x43), i.Type)
	assert.Equal(t, byte(0x02), i.Code)
	assert.Equal(t, uint16(0), i.Checksum)
}

func TestICMPSerialize(t *testing.T) {
	i := icmp.NewHeader(icmp.TypeDestinationUnreachable, icmp.CodeDestinationUnreachableHostAdministrativelyProhibited)
	var binBuf bytes.Buffer
	err := i.Serialize(&binBuf)
	assert.NoError(t, err, "icmp.Serialize")
	assert.Equal(t, []byte{0x03, 0x0A, 0x00, 0x00}, binBuf.Bytes())
}

package icmp_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l3/icmp"
	"gitlab.com/go-netnode/process/l3/ipv4"
	"gitlab.com/go-netnode/process/processtest"
)

func TestICMPProcessNotEnoughBytes(t *testing.T) {
	i := icmp.NewProcess()
	d := make([]byte, 3)
	reply, err := i.Process(nil, d, process.Meta{})
	assert.EqualError(t, err, "icmp.Process: Not enough data to be an ICMP datagram")
	assert.Nil(t, reply)
}

func TestICMPProcessEnableUnknownType(t *testing.T) {
	i := icmp.NewProcess()
	err := i.EnableType(255)
	assert.EqualError(t, err, "unsurported ICMP type: 255")
}

func TestICMPProcessInvalidChecksum(t *testing.T) {
	i := icmp.NewProcess()
	err := i.EnableType(icmp.TypeEchoRequest)
	require.NoError(t, err, "EnableType")
	ic := icmp.NewHeader(
		icmp.TypeDestinationUnreachable,
		icmp.CodeDestinationUnreachableSourceRouteFailed,
	)

	var binBuf bytes.Buffer
	err = ic.Serialize(&binBuf)
	require.NoError(t, err, "Serialize")
	b := binBuf.Bytes()

	reply, err := i.Process(nil, b, process.Meta{})
	assert.NoError(t, err)
	assert.Nil(t, reply)
}

var datas = map[string]struct {
	typeToEnable  []uint8
	typeToSend    uint8
	data          []byte
	bufferSize    int
	expectedError string
	expectedData  []byte
}{
	"BufferTooSmallToWriteICMPHeader": {
		typeToEnable:  []byte{icmp.TypeEchoRequest},
		typeToSend:    icmp.TypeEchoRequest,
		data:          []byte{0x00, 0x01, 0x02},
		bufferSize:    4,
		expectedError: "icmp.Process: error while serializing header to reply: Buffer: not enough space",
		expectedData:  nil,
	},
	"TooSmallToWriteData": {
		typeToEnable:  []byte{icmp.TypeEchoRequest},
		typeToSend:    icmp.TypeEchoRequest,
		data:          []byte{0x00, 0x01, 0x02},
		bufferSize:    2,
		expectedError: "icmp.Process: error while writting data to reply: Buffer: not enough space",
		expectedData:  nil,
	},
	"ProcessAnswer": {
		typeToEnable:  []byte{icmp.TypeEchoRequest},
		typeToSend:    icmp.TypeEchoRequest,
		data:          nil,
		bufferSize:    100,
		expectedError: "",
		expectedData:  []byte{icmp.TypeEchoReply},
	},
	"EchoRequestNotEnabled": {
		typeToEnable:  []byte{icmp.TypeEchoReply},
		typeToSend:    icmp.TypeEchoRequest,
		data:          nil,
		bufferSize:    100,
		expectedError: "",
		expectedData:  nil,
	},
	"NothingEnabled": {
		typeToEnable:  nil,
		typeToSend:    icmp.TypeEchoRequest,
		data:          nil,
		bufferSize:    100,
		expectedError: "",
		expectedData:  nil,
	},
	"EchoReply": {
		typeToEnable:  []byte{icmp.TypeEchoReply},
		typeToSend:    icmp.TypeEchoReply,
		data:          nil,
		bufferSize:    100,
		expectedError: "",
		expectedData:  nil,
	},
}

func TestICMPProcess(t *testing.T) {
	for name, data := range datas {
		t.Run(name, func(t *testing.T) {
			i := icmp.NewProcess()
			var err error
			for _, typ := range data.typeToEnable {
				err = i.EnableType(typ)
				require.NoError(t, err, "EnableType")
			}

			ic := icmp.NewHeader(data.typeToSend, 0)

			var binBuf bytes.Buffer
			err = ic.Serialize(&binBuf)
			require.NoError(t, err, "Serialize")

			if data.data != nil {
				binBuf.Write(data.data)
			}
			b := binBuf.Bytes()

			checker := ipv4.Checksum{}
			checker.AddData(b)
			sum := checker.Compute()
			b[2] = byte(sum >> 8)
			b[3] = byte(sum & 0x00FF)

			pdata := processtest.ProduceProcessData(t, data.bufferSize)
			reply, err := i.Process(pdata, b, process.Meta{})

			if len(data.expectedError) > 0 {
				assert.EqualError(t, err, data.expectedError)
			} else {
				assert.Nil(t, err)
			}

			if data.expectedData != nil {
				bytes := reply.GetBuffer().Bytes()
				checker.Reset()
				checker.AddData(bytes)
				assert.True(t, checker.Verify())
				assert.Equal(t, data.expectedData[0], bytes[0])
			} else {
				assert.Nil(t, reply)
			}
		})
	}
}

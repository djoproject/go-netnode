package icmp

import "fmt"

const (
	// TypeEchoReply is the ICMP type for echo reply
	TypeEchoReply uint8 = 0

	// TypeDestinationUnreachable is the ICMP type for a destination unreachable
	TypeDestinationUnreachable uint8 = 3
	// CodeDestinationUnreachableDestinationNetworkUnreachable is the ICMP code for network unreachable
	CodeDestinationUnreachableDestinationNetworkUnreachable uint8 = 0
	// CodeDestinationUnreachableDestinationHostUnreachable is the ICMP code for host unreachable
	CodeDestinationUnreachableDestinationHostUnreachable uint8 = 1
	// CodeDestinationUnreachableDestinationProtocolUnreachable is the ICMP code for protocol unreachable
	CodeDestinationUnreachableDestinationProtocolUnreachable uint8 = 2
	// CodeDestinationUnreachableDestinationPortUnreachable is the ICMP code for port unreachable
	CodeDestinationUnreachableDestinationPortUnreachable uint8 = 3
	// CodeDestinationUnreachableFragmentationRequired is the ICMP code for fragmentation required
	CodeDestinationUnreachableFragmentationRequired uint8 = 4
	// CodeDestinationUnreachableSourceRouteFailed is the ICMP code for source route failed
	CodeDestinationUnreachableSourceRouteFailed uint8 = 5
	// CodeDestinationUnreachableDestinationNetworkUnknown is the ICMP code for destination network
	CodeDestinationUnreachableDestinationNetworkUnknown uint8 = 6
	// CodeDestinationUnreachableDestinationHostUnknown is the ICMP code for destination host unknown
	CodeDestinationUnreachableDestinationHostUnknown uint8 = 7
	// CodeDestinationUnreachableSourceHostIsolated is the ICMP code for source host isolated
	CodeDestinationUnreachableSourceHostIsolated uint8 = 8
	// CodeDestinationUnreachableNetworkAdministrativelyProhibited is the ICMP code for network administratively prohibited
	CodeDestinationUnreachableNetworkAdministrativelyProhibited uint8 = 9
	// CodeDestinationUnreachableHostAdministrativelyProhibited is the ICMP code for host administratively prohibited
	CodeDestinationUnreachableHostAdministrativelyProhibited uint8 = 10
	// CodeDestinationUnreachableNetworkUnreachableTos is the ICMP code for network unreachable tos
	CodeDestinationUnreachableNetworkUnreachableTos uint8 = 11
	// CodeDestinationUnreachableHostUnreachableTos is the ICMP code for host unreachable tos
	CodeDestinationUnreachableHostUnreachableTos uint8 = 12
	// CodeDestinationUnreachableCommunicationAdministrativelyProhibited is the ICMP code for communication administratively prohibited
	CodeDestinationUnreachableCommunicationAdministrativelyProhibited uint8 = 13
	// CodeDestinationUnreachableHostPrecedenceViolation is the ICMP code for host precedence violation
	CodeDestinationUnreachableHostPrecedenceViolation uint8 = 14
	// CodeDestinationUnreachablePrecedenceCutoffInEffect is the ICMP code for precendence cut off in effect
	CodeDestinationUnreachablePrecedenceCutoffInEffect uint8 = 15

	// TypeSourceQuench is the ICMP type for a source quench
	TypeSourceQuench uint8 = 4

	// TypeRedirectMessage is the ICMP type for a redirect message
	TypeRedirectMessage uint8 = 5
	// CodeRedirectMessageNetwork is the ICMP code for message network
	CodeRedirectMessageNetwork uint8 = 0
	// CodeRedirectMessageHost is the ICMP code for message host
	CodeRedirectMessageHost uint8 = 1
	// CodeRedirectMessageNetworkTos is the ICMP code for message network tos
	CodeRedirectMessageNetworkTos uint8 = 2
	// CodeRedirectMessageHostTos is the ICMP code for message host tos
	CodeRedirectMessageHostTos uint8 = 3

	// TypeAlternateHostAddress is the ICMP type for an alternative host address
	TypeAlternateHostAddress uint8 = 6
	// TypeEchoRequest is the ICMP type for an echo request
	TypeEchoRequest uint8 = 8
	// TypeRouterAdvertisement is the ICMP type for a router advertisement
	TypeRouterAdvertisement uint8 = 9
	// TypeRouterSolicitation is the ICMP type for a router solicitation
	TypeRouterSolicitation uint8 = 10

	// TypeTimeExceeded is the ICMP type for a time exceeded
	TypeTimeExceeded uint8 = 11
	// CodeTimeExceededTTL is the ICMP code for exceeded TTL
	CodeTimeExceededTTL uint8 = 0
	// CodeTimeExceededFragment is the ICMP code for exceeded fragment
	CodeTimeExceededFragment uint8 = 1

	// TypeParameterProblem is the ICMP type for a parameter problem
	TypeParameterProblem uint8 = 12
	// CodeParameterProblemPointer is the ICMP code for pointer problem
	CodeParameterProblemPointer uint8 = 0
	// CodeParameterProblemMissing is the ICMP code for something missing
	CodeParameterProblemMissing uint8 = 1
	// CodeParameterProblemBadLength is the ICMP code for bad length
	CodeParameterProblemBadLength uint8 = 2

	// TypeTimestamp is the ICMP type for a timestamp request
	TypeTimestamp uint8 = 13
	// TypeTimestampReply is the ICMP type for a timestamp reply
	TypeTimestampReply uint8 = 14
	// TypeInformationRequest is the ICMP type for an information request
	TypeInformationRequest uint8 = 15
	// TypeInformationReply is the ICMP type for an information reply
	TypeInformationReply uint8 = 16
	// TypeAddressMaskRequest is the ICMP type for an address mask request
	TypeAddressMaskRequest uint8 = 17
	// TypeAddressMaskReply is the ICMP type for an address mask reply
	TypeAddressMaskReply uint8 = 18
	// TypeTraceroute is the ICMP type for a traceroute
	TypeTraceroute uint8 = 30
	// TypeDatagramConversionError is the ICMP type for for a datagrame conversion error
	TypeDatagramConversionError uint8 = 31
	// TypeMobileHostRedirect is the ICMP type for a movile host redirect
	TypeMobileHostRedirect uint8 = 32
	// TypeWhereAreYou is the ICMP type for a where are you request
	TypeWhereAreYou uint8 = 33
	// TypeHereIAm is the ICMP type for here I am reply
	TypeHereIAm uint8 = 34
	// TypeMobileRegistrationRequest is the ICMP type for a mobile registration request
	TypeMobileRegistrationRequest uint8 = 35
	// TypeMobileRegistrationReply is the ICMP type for a mobile registration reply
	TypeMobileRegistrationReply uint8 = 36
	// TypeDomainNameRequest is the ICMP type for a domain name request
	TypeDomainNameRequest uint8 = 37
	// TypeDomainNameReply is the ICMP type for a domain name reply
	TypeDomainNameReply uint8 = 38
	// TypeSkip is the ICMP type for a Skip
	TypeSkip uint8 = 39
	// TypePhoturis is the ICMP type for a Photuris
	TypePhoturis uint8 = 40
	// TypeIcmpForExperimentalMobility is the ICMP type for experimental mobility
	TypeIcmpForExperimentalMobility uint8 = 41
	// TypeExtendedEchoRequest is the ICMP type for extended echo request
	TypeExtendedEchoRequest uint8 = 42

	// TypeExtendedEchoReply is the ICMP type for an extended echo reply
	TypeExtendedEchoReply uint8 = 43
	// CodeExtendedEchoReplyNoError is the ICMP code for no error
	CodeExtendedEchoReplyNoError uint8 = 0
	// CodeExtendedEchoReplyMalformedQuery is the ICMP code for malformed query
	CodeExtendedEchoReplyMalformedQuery uint8 = 1
	// CodeExtendedEchoReplyNoSuchInterface is the ICMP code for no such interface
	CodeExtendedEchoReplyNoSuchInterface uint8 = 2
	// CodeExtendedEchoReplyNoSuchTableEntry is the ICMP code for no such table entry
	CodeExtendedEchoReplyNoSuchTableEntry uint8 = 3
	// CodeExtendedEchoReplyMultiplyInterfacesSatisfyQuery is the ICMP code for
	CodeExtendedEchoReplyMultiplyInterfacesSatisfyQuery uint8 = 4

	// TypeRfc3692Exp1 is the ICMP type for the Rfc3692Exp1
	TypeRfc3692Exp1 uint8 = 253
	// TypeRfc3692Exp2 is the ICMP type for Rfc3692Exp2
	TypeRfc3692Exp2 uint8 = 254
)

// TypeList variable contains the list of every available type
var TypeList = []uint8{TypeEchoReply, TypeDestinationUnreachable, TypeSourceQuench, TypeRedirectMessage, TypeAlternateHostAddress, TypeEchoRequest, TypeRouterAdvertisement, TypeRouterSolicitation, TypeTimeExceeded, TypeParameterProblem, TypeTimestamp, TypeTimestampReply, TypeInformationRequest, TypeInformationReply, TypeAddressMaskRequest, TypeAddressMaskReply, TypeTraceroute, TypeDatagramConversionError, TypeMobileHostRedirect, TypeWhereAreYou, TypeHereIAm, TypeMobileRegistrationRequest, TypeMobileRegistrationReply, TypeDomainNameRequest, TypeDomainNameReply, TypeSkip, TypePhoturis, TypeIcmpForExperimentalMobility, TypeExtendedEchoRequest, TypeExtendedEchoReply, TypeRfc3692Exp1, TypeRfc3692Exp2}

// TypeWithCodeList variable contains the list of Type that have relevant codes
var TypeWithCodeList = []uint8{TypeDestinationUnreachable, TypeRedirectMessage, TypeTimeExceeded, TypeParameterProblem, TypeExtendedEchoReply}

// GetCodeForType function returns the revelant codes for a type
func GetCodeForType(typ uint8) []uint8 {
	switch typ {
	case TypeDestinationUnreachable:
		return []uint8{CodeDestinationUnreachableDestinationNetworkUnreachable, CodeDestinationUnreachableDestinationHostUnreachable, CodeDestinationUnreachableDestinationProtocolUnreachable, CodeDestinationUnreachableDestinationPortUnreachable, CodeDestinationUnreachableFragmentationRequired, CodeDestinationUnreachableSourceRouteFailed, CodeDestinationUnreachableDestinationNetworkUnknown, CodeDestinationUnreachableDestinationHostUnknown, CodeDestinationUnreachableSourceHostIsolated, CodeDestinationUnreachableNetworkAdministrativelyProhibited, CodeDestinationUnreachableHostAdministrativelyProhibited, CodeDestinationUnreachableNetworkUnreachableTos, CodeDestinationUnreachableHostUnreachableTos, CodeDestinationUnreachableCommunicationAdministrativelyProhibited, CodeDestinationUnreachableHostPrecedenceViolation, CodeDestinationUnreachablePrecedenceCutoffInEffect}
	case TypeRedirectMessage:
		return []uint8{CodeRedirectMessageNetwork, CodeRedirectMessageHost, CodeRedirectMessageNetworkTos, CodeRedirectMessageHostTos}
	case TypeTimeExceeded:
		return []uint8{CodeTimeExceededTTL, CodeTimeExceededFragment}
	case TypeParameterProblem:
		return []uint8{CodeParameterProblemPointer, CodeParameterProblemMissing, CodeParameterProblemBadLength}
	case TypeExtendedEchoReply:
		return []uint8{CodeExtendedEchoReplyNoError, CodeExtendedEchoReplyMalformedQuery, CodeExtendedEchoReplyNoSuchInterface, CodeExtendedEchoReplyNoSuchTableEntry, CodeExtendedEchoReplyMultiplyInterfacesSatisfyQuery}
	}

	return nil
}

// TypeToString returns a string for a type
func TypeToString(typ uint8) string {
	switch typ {
	case 0:
		return "Echo Reply"
	case TypeDestinationUnreachable:
		return "Destination Unreachable"
	case 4:
		return "Source Quench"
	case TypeRedirectMessage:
		return "Redirect Message"
	case 6:
		return "Alternate Host Address"
	case 8:
		return "Echo Request"
	case 9:
		return "Router Advertisement"
	case 10:
		return "Router Solicitation"
	case TypeTimeExceeded:
		return "Time Exceeded"
	case TypeParameterProblem:
		return "Parameter Problem"
	case 13:
		return "Timestamp"
	case 14:
		return "Timestamp Reply"
	case 15:
		return "Information Request"
	case 16:
		return "Information Reply"
	case 17:
		return "Address Mask Request"
	case 18:
		return "Address Mask Reply"
	case 30:
		return "Traceroute"
	case 31:
		return "Datagram Conversion Error"
	case 32:
		return "Mobile Host Redirect"
	case 33:
		return "Where-Are-You"
	case 34:
		return "Here-I-Am"
	case 35:
		return "Mobile Registration Request"
	case 36:
		return "Mobile Registration Reply"
	case 37:
		return "Domain Name Request"
	case 38:
		return "Domain Name Reply"
	case 39:
		return "Skip Algorithm"
	case 40:
		return "Photuris"
	case 41:
		return "Icmp for experimental mobility protocols"
	case 42:
		return "Extended Echo Request"
	case TypeExtendedEchoReply:
		return "Extended Echo Reply"
	case 253:
		return "Rfc3692-style Experiment 1"
	case 254:
		return "Rfc3692-style Experiment 2"
	}

	return "Reserved"
}

// IsReservedtype return true if a type is reserved
func IsReservedtype(typ uint8) bool {
	return typ == 1 || typ == 2 || typ == 7 || (typ > 18 && typ < 30) || (typ > 43 && typ < 253) || typ == 255
}

// IsCodeRelevantForType returns true if a type has sub codes.
func IsCodeRelevantForType(typ uint8) bool {
	return typ == 3 || typ == 5 || typ == 11 || typ == 12 || typ == 43
}

// CodeToString returns a string corresponding to a type and a code
func CodeToString(typ, code uint8) string {
	switch typ {
	case TypeDestinationUnreachable:
		switch code {
		case 0:
			return "Destination network unreachable"
		case 1:
			return "Destination host unreachable"
		case 2:
			return "Destination protocol unreachable"
		case 3:
			return "Destination port unreachable"
		case 4:
			return "Fragmentation required, and Df flag set"
		case 5:
			return "Source route failed"
		case 6:
			return "Destination network unknown"
		case 7:
			return "Destination host unknown"
		case 8:
			return "Source host isolated"
		case 9:
			return "Network administratively prohibited"
		case 10:
			return "Host administratively prohibited"
		case 11:
			return "Network unreachable for ToS"
		case 12:
			return "Host unreachable for ToS"
		case 13:
			return "Communication administratively prohibited"
		case 14:
			return "Host Precedence Violation"
		case 15:
			return "Precedence cutoff in effect"
		default:
			return fmt.Sprintf("%s: unknown code %d", TypeToString(typ), code)
		}
	case TypeRedirectMessage:
		switch code {
		case 0:
			return "Redirect Datagram for the Network"
		case 1:
			return "Redirect Datagram for the Host"
		case 2:
			return "Redirect Datagram for the ToS & network"
		case 3:
			return "Redirect Datagram for the ToS & host"
		default:
			return fmt.Sprintf("%s: unknown code %d", TypeToString(typ), code)
		}
	case TypeTimeExceeded:
		switch code {
		case 0:
			return "Ttl expired in transit"
		case 1:
			return "Fragment reassembly time exceeded"
		default:
			return fmt.Sprintf("%s: unknown code %d", TypeToString(typ), code)
		}
	case TypeParameterProblem:
		switch code {
		case 0:
			return "Pointer indicates the error"
		case 1:
			return "Missing a required option"
		case 2:
			return "Bad length"
		default:
			return fmt.Sprintf("%s: unknown code %d", TypeToString(typ), code)
		}
	case TypeExtendedEchoReply:
		switch code {
		case 0:
			return "No Error"
		case 1:
			return "Malformed Query"
		case 2:
			return "No Such Interface"
		case 3:
			return "No Such Table Entry"
		case 4:
			return "Multiple Interfaces Satisfy Query"
		default:
			return fmt.Sprintf("%s: unknown code %d", TypeToString(typ), code)
		}
	}

	return TypeToString(typ)
}

package icmp

import (
	"bytes"
	"encoding/binary"
	"io"
)

// Header is a structure that contains ICMP header
type Header struct {
	Type     uint8
	Code     uint8
	Checksum uint16
}

// NewHeader function creates a new ICMP header
func NewHeader(typ, code uint8) *Header {
	return &Header{
		Type: typ,
		Code: code,
	}
}

// Serialize allows to convert object to []byte
func (h *Header) Serialize(writer io.Writer) error {
	return binary.Write(writer, binary.BigEndian, h)
}

// Deserialize allows to convert []byte into an icmp object
func (h *Header) Deserialize(data []byte) error {
	buf := bytes.NewReader(data)
	return binary.Read(buf, binary.BigEndian, h)
}

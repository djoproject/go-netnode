package icmp_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-netnode/process/l3/icmp"
)

func TestTypeToString(t *testing.T) {
	for i := 0; i < 256; i++ {
		assert.NotEmpty(t, icmp.TypeToString(uint8(i)))
	}
}

func TestIsReservedtype(t *testing.T) {
	assert.True(t, icmp.IsReservedtype(255))
	assert.False(t, icmp.IsReservedtype(3))
}

func TestIsCodeRelevantForType(t *testing.T) {
	assert.True(t, icmp.IsCodeRelevantForType(5))
	assert.False(t, icmp.IsCodeRelevantForType(6))
}

func TestCodeToString(t *testing.T) {
	for typ := 0; typ < 256; typ++ {
		codes := icmp.GetCodeForType(uint8(typ))

		if !icmp.IsCodeRelevantForType(uint8(typ)) {
			assert.Empty(t, codes)
			// 33 is a random code
			assert.Equal(t, icmp.TypeToString(uint8(typ)), icmp.CodeToString(uint8(typ), 33))
			continue
		}

		assert.NotEmpty(t, icmp.GetCodeForType(uint8(typ)))
		typeString := icmp.TypeToString(uint8(typ))
		for _, code := range codes {
			codeString := icmp.CodeToString(uint8(typ), code)
			assert.NotEmpty(t, codeString)
			assert.NotEqual(t, typeString, codeString)
		}

		codeString := icmp.CodeToString(uint8(typ), 33)
		assert.NotEmpty(t, codeString)
		assert.NotEqual(t, typeString, codeString)
		assert.Equal(t, fmt.Sprintf("%s: unknown code 33", typeString), codeString)
	}
}

func TestDeclaredType(t *testing.T) {
	for _, typ := range icmp.TypeList {
		typeString := icmp.TypeToString(typ)
		assert.NotEmpty(t, typeString)
		assert.NotEqual(t, "Reserved", typeString)
	}
}

func TestTypeWithCode(t *testing.T) {
	for _, typ := range icmp.TypeWithCodeList {
		codes := icmp.GetCodeForType(typ)

		typeString := icmp.TypeToString(typ)
		for _, code := range codes {
			codeString := icmp.CodeToString(typ, code)
			assert.NotEmpty(t, codeString)
			assert.NotEqual(t, "Reserved", codeString)
			assert.NotEqual(t, typeString, codeString)
		}
	}
}

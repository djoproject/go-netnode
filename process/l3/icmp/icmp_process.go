package icmp

import (
	"errors"
	"fmt"

	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l3/ipv4"
	"gitlab.com/go-netnode/utils/levellog"
	"gitlab.com/go-netnode/utils/pool"
)

// Process represents an ICMP process
type Process struct {
	enabledType map[uint8]func(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error)
}

// NewProcess function create a new ICMP process
func NewProcess() *Process {
	return &Process{
		enabledType: make(map[uint8]func(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error)),
	}
}

// EnableType method enables the management of the ICMP type
func (p *Process) EnableType(typ uint8) error {
	if typ == TypeEchoRequest {
		p.enabledType[typ] = processEchoRequest
		return nil
	}

	if typ == TypeEchoReply {
		p.enabledType[typ] = processEchoReply
		return nil
	}

	return fmt.Errorf("unsurported ICMP type: %d", typ)
}

// Process is the common method to process an ICMP request
func (p Process) Process(system *process.System, data []byte, meta process.Meta) (*pool.ReusableBuffer, error) {
	// not enough data to work
	if len(data)-meta.Start < 4 {
		return nil, errors.New("icmp.Process: Not enough data to be an ICMP datagram")
	}

	// no ICMP type enabled, nothing to do
	if len(p.enabledType) == 0 {
		return nil, nil
	}

	// check the checksum
	checker := ipv4.Checksum{}
	checker.AddData(data[meta.Start:])

	if !checker.Verify() {
		levellog.Debugln("icmp.Process: invalid checksum")
		return nil, nil
	}

	// Process the message
	var err error
	ic := Header{}
	err = ic.Deserialize(data[meta.Start : meta.Start+4])

	if err != nil {
		return nil, fmt.Errorf("icmp.Process: Failed to deserialize ARP reply: %w", err)
	}

	var reBuff *pool.ReusableBuffer
	fun, exist := p.enabledType[ic.Type]

	if !exist {
		return nil, nil
	}

	reBuff, err = fun(system, data, meta)

	if err != nil {
		return nil, err
	}

	if reBuff == nil {
		return nil, nil
	}

	// Compute then write checksum
	bytes := reBuff.GetBuffer().Bytes()
	checker.Reset()
	checker.AddData(bytes)
	sum := checker.Compute()
	bytes[2] = byte(sum >> 8)
	bytes[3] = byte(sum & 0x00FF)

	return reBuff, nil
}

func processEchoRequest(system *process.System, data []byte, meta process.Meta) (*pool.ReusableBuffer, error) {
	icmpReply := NewHeader(TypeEchoReply, 0)

	reBuff := system.Device.GetBufferPool().GetBuffer()
	reply := reBuff.GetBuffer()

	// Write header and data answer
	_, err := reply.GetPrepend().Write(data[meta.Start+4:])

	if err != nil {
		return nil, fmt.Errorf("icmp.Process: error while writting data to reply: %w", err)
	}

	err = icmpReply.Serialize(reply.GetPrepend())

	if err != nil {
		return nil, fmt.Errorf("icmp.Process: error while serializing header to reply: %w", err)
	}

	return reBuff, err
}

func processEchoReply(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error) {
	// So far, do nothing, but going to need to store/process those received data
	return nil, nil
}

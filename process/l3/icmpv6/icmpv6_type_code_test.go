package icmpv6_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-netnode/process/l3/icmpv6"
)

func TestTypeToString(t *testing.T) {
	for i := 0; i < 256; i++ {
		assert.NotEmpty(t, icmpv6.TypeToString(uint8(i)))
	}
}

func TestIsReservedtype(t *testing.T) {
	assert.True(t, icmpv6.IsReservedtype(255))
	assert.False(t, icmpv6.IsReservedtype(3))
}

func TestIsCodeRelevantForType(t *testing.T) {
	assert.True(t, icmpv6.IsCodeRelevantForType(1))
}

func TestIsCodeRelevantForTypeNot(t *testing.T) {
	assert.False(t, icmpv6.IsCodeRelevantForType(128))
}

func TestCodeToString(t *testing.T) {
	for typ := 0; typ < 256; typ++ {
		codes := icmpv6.GetCodeForType(uint8(typ))

		if !icmpv6.IsCodeRelevantForType(uint8(typ)) {
			assert.Empty(t, codes)
			// 33 is a random code
			assert.Equal(t, icmpv6.TypeToString(uint8(typ)), icmpv6.CodeToString(uint8(typ), 33))
			continue
		}

		assert.NotEmpty(t, icmpv6.GetCodeForType(uint8(typ)))
		typeString := icmpv6.TypeToString(uint8(typ))
		for _, code := range codes {
			codeString := icmpv6.CodeToString(uint8(typ), code)
			assert.NotEmpty(t, codeString)
			assert.NotEqual(t, typeString, codeString)
		}

		codeString := icmpv6.CodeToString(uint8(typ), 33)
		assert.NotEmpty(t, codeString)
		assert.NotEqual(t, typeString, codeString)
		assert.Equal(t, fmt.Sprintf("%s: unknown code 33", typeString), codeString)
	}
}

func TestDeclaredType(t *testing.T) {
	for _, typ := range icmpv6.TypeList {
		typeString := icmpv6.TypeToString(typ)
		assert.NotEmpty(t, typeString)
		assert.NotEqual(t, "Reserved", typeString)
	}
}

func TestTypeWithCode(t *testing.T) {
	for _, typ := range icmpv6.TypeWithCodeList {
		codes := icmpv6.GetCodeForType(typ)

		typeString := icmpv6.TypeToString(typ)
		for _, code := range codes {
			codeString := icmpv6.CodeToString(typ, code)
			assert.NotEmpty(t, codeString)
			assert.NotEqual(t, "Reserved", codeString)
			assert.NotEqual(t, typeString, codeString)
		}
	}
}

package icmpv6

import "fmt"

const (
	// TypeDestinationUnreachable is the ICMP type for a destination unreachable
	TypeDestinationUnreachable uint8 = 1
	// CodeDestinationUnreachableNoRouteToDestination is the ICMP code for no route to host
	CodeDestinationUnreachableNoRouteToDestination uint8 = 0
	// CodeDestinationUnreachableCommunicationWithDestinationAdministrativelyProhibited is the ICMP code for
	// a communication administratively prohibited
	CodeDestinationUnreachableCommunicationWithDestinationAdministrativelyProhibited uint8 = 1
	// CodeDestinationUnreachableBeyondScopeOfSourceSddress is the ICMP code for a source address beyond
	// scope
	CodeDestinationUnreachableBeyondScopeOfSourceSddress uint8 = 2
	// CodeDestinationUnreachableAddressUnreachable is the ICMP code for an address unreachable
	CodeDestinationUnreachableAddressUnreachable uint8 = 3
	// CodeDestinationUnreachablePortUnreachable is the ICMP code for port unreachable
	CodeDestinationUnreachablePortUnreachable uint8 = 4
	// CodeDestinationUnreachableSourceAddressFailedIngressEgressPolicy is the ICMP code for an address
	// that does not match an ingress or egress policy
	CodeDestinationUnreachableSourceAddressFailedIngressEgressPolicy uint8 = 5
	// CodeDestinationUnreachableRejectRouteToDestination is the ICMP code for a rejected route to
	// destination
	CodeDestinationUnreachableRejectRouteToDestination uint8 = 6
	// CodeDestinationUnreachableErrorInSourceRoutingHeader is the ICMP code for an error in source
	// routing header
	CodeDestinationUnreachableErrorInSourceRoutingHeader uint8 = 7

	// TypePacketTooBig is the ICMP type for a packet too big
	TypePacketTooBig uint8 = 2
	// TypeTimeExceeded is the ICMP type for a time exceeded
	TypeTimeExceeded uint8 = 3
	// CodeTimeExceededHopLimitExceededInTransit is the ICMP code for hop limit exceeded
	CodeTimeExceededHopLimitExceededInTransit uint8 = 0
	// CodeTimeExceededFragmentReassemblyTimeExceeded is the ICMP code for time reassembly exceeded
	CodeTimeExceededFragmentReassemblyTimeExceeded uint8 = 1

	// TypeParameterProblem is the ICMP type for a parameter problem
	TypeParameterProblem uint8 = 4
	// CodeParameterProblemErroneousHeaderFieldEncountered is the ICMP code for an erroneous header field
	CodeParameterProblemErroneousHeaderFieldEncountered uint8 = 0
	// CodeParameterProblemUnrecognizedNextHeaderTypeEncountered is the ICMP code for unrecognized next
	// header field
	CodeParameterProblemUnrecognizedNextHeaderTypeEncountered uint8 = 1
	// CodeParameterProblemUnrecognizedIPv6OptionEncountered is the ICMP code for an unrecognized
	// IPv6 option
	CodeParameterProblemUnrecognizedIPv6OptionEncountered uint8 = 2

	// TypePrivateExperimentation1 is the ICMP type for a private experimentation
	TypePrivateExperimentation1 uint8 = 100
	// TypePrivateExperimentation2 is the ICMP type for a private experimentation
	TypePrivateExperimentation2 uint8 = 101

	// Reserved for expansion of ICMPv6 error messages 127

	// TypeEchoRequest is the ICMP type for an echo request
	TypeEchoRequest uint8 = 128
	// TypeEchoReply is the ICMP type for an echo reply
	TypeEchoReply uint8 = 129

	// MLD

	//TypeMulticastListenerQuery is the ICMP type for a multicast listener query
	TypeMulticastListenerQuery uint8 = 130
	// TypeMulticastListenerReport is the ICMP type for a multicast listener report
	TypeMulticastListenerReport uint8 = 131
	// TypeMulticastListenerDone is the ICMP type for a multicast listener done
	TypeMulticastListenerDone uint8 = 132

	// NDP

	// TypeRouterSolicitation is the ICMP type for a router solicitation
	TypeRouterSolicitation uint8 = 133
	// TypeRouterAdvertisement is the ICMP type for a router advertisement
	TypeRouterAdvertisement uint8 = 134
	// TypeNeighborSolicitation is the ICMP type for a neighbor solicitation
	TypeNeighborSolicitation uint8 = 135
	// TypeNeighborAdvertisement is the ICMP type for a neighbir advsertisement
	TypeNeighborAdvertisement uint8 = 136
	// TypeRedirectMessage is the ICMP type for a redirect message
	TypeRedirectMessage uint8 = 137

	// TypeRouterRenumbering is the ICMP type for a route reunmbering
	TypeRouterRenumbering uint8 = 138
	// CodeRouterRenumberingRouterRenumberingCommand is the ICMP code for
	CodeRouterRenumberingRouterRenumberingCommand uint8 = 0
	// CodeRouterRenumberingRouterRenumberingResult is the ICMP code for
	CodeRouterRenumberingRouterRenumberingResult uint8 = 1
	// CodeRouterRenumberingSequenceNumberReset is the ICMP code for
	CodeRouterRenumberingSequenceNumberReset uint8 = 255

	// TypeICMPNodeInformationQuery is the ICMP type for a node information query
	TypeICMPNodeInformationQuery uint8 = 139
	// CodeICMPNodeInformationQueryContainsAnIPv6 is the ICMP code to query an IPv6
	CodeICMPNodeInformationQueryContainsAnIPv6 uint8 = 0
	// CodeICMPNodeInformationQueryContainsAName is the ICMP code to query a name
	CodeICMPNodeInformationQueryContainsAName uint8 = 1
	// CodeICMPNodeInformationQueryContainsAnIPv4 is the ICMP code to query an IPv4
	CodeICMPNodeInformationQueryContainsAnIPv4 uint8 = 2

	// TypeICMPNodeInformationResponse is the ICMP type for a node information response
	TypeICMPNodeInformationResponse uint8 = 140
	// CodeICMPNodeInformationResponseASuccessfulReply is the ICMP code for sucessful response
	CodeICMPNodeInformationResponseASuccessfulReply uint8 = 0
	// CodeICMPNodeInformationResponseTheResponderRefusesToSupplyTheAnswer is the ICMP code for
	// responder that refuses to provide the answer
	CodeICMPNodeInformationResponseTheResponderRefusesToSupplyTheAnswer uint8 = 1
	// CodeICMPNodeInformationResponseTheQtypeOfTheQueryIsUnknownToTheResponder is the ICMP code for
	// an unknown query type
	CodeICMPNodeInformationResponseTheQtypeOfTheQueryIsUnknownToTheResponder uint8 = 2

	// TypeInverseNeighborDiscoverySolicitationMessage is the ICMP type for a NDP solicitation message
	TypeInverseNeighborDiscoverySolicitationMessage uint8 = 141
	// TypeInverseNeighborDiscoveryAdvertisementMessage is the ICMP type for a NDP advertisement message
	TypeInverseNeighborDiscoveryAdvertisementMessage uint8 = 142
	// TypeMulticastListenerDiscovery is the ICMP type for a multicast listener discovery
	TypeMulticastListenerDiscovery uint8 = 143
	// TypeHomeAgentAddressDiscoveryRequestMessage is the ICMP type for a home agent discovery request
	TypeHomeAgentAddressDiscoveryRequestMessage uint8 = 144
	// TypeHomeAgentAddressDiscoveryReplyMessage is the ICMP type for a home agent discovery reply
	TypeHomeAgentAddressDiscoveryReplyMessage uint8 = 145
	// TypeMobilePrefixSolicitation is the ICMP type for a mobile prefix solicitation
	TypeMobilePrefixSolicitation uint8 = 146
	// TypeMobilePrefixAdvertisement is the ICMP type for a mobile prefix advertisement
	TypeMobilePrefixAdvertisement uint8 = 147

	// SEND

	// TypeCertificationPathSolicitation is the ICMP type for a SEND path solicitation
	TypeCertificationPathSolicitation uint8 = 148
	// TypeCertificationPathAdvertisement is the ICMP type for a SEND path advertisament
	TypeCertificationPathAdvertisement uint8 = 149

	// MRD

	// TypeMulticastRouterAdvertisement is the ICMP type for a multicast router advertisement
	TypeMulticastRouterAdvertisement uint8 = 151
	// TypeMulticastRouterSolicitation is the ICMP type for a multicast router solicitation
	TypeMulticastRouterSolicitation uint8 = 152
	// TypeMulticastRouterTermination is the ICMP type for a multicast router termination
	TypeMulticastRouterTermination uint8 = 153

	// TypeRPLControlMessage is the ICMP type for a RPL control message
	TypeRPLControlMessage uint8 = 155
	// TypePrivateExperimentation3 is the ICMP type for a private experimentation
	TypePrivateExperimentation3 uint8 = 200
	// TypePrivateExperimentation4 is the ICMP type for a private experimentation
	TypePrivateExperimentation4 uint8 = 201
	// Reserved for expansion of ICMPv6 informational messages 255
)

// TypeList variable contains the list of every available type
var TypeList = []uint8{TypeDestinationUnreachable, TypePacketTooBig, TypeTimeExceeded, TypeParameterProblem, TypePrivateExperimentation1, TypePrivateExperimentation2, TypeEchoRequest, TypeEchoReply, TypeMulticastListenerQuery, TypeMulticastListenerReport, TypeMulticastListenerDone, TypeRouterSolicitation, TypeRouterAdvertisement, TypeNeighborSolicitation, TypeNeighborAdvertisement, TypeRedirectMessage, TypeRouterRenumbering, TypeICMPNodeInformationQuery, TypeICMPNodeInformationResponse, TypeInverseNeighborDiscoverySolicitationMessage, TypeInverseNeighborDiscoveryAdvertisementMessage, TypeMulticastListenerDiscovery, TypeHomeAgentAddressDiscoveryRequestMessage, TypeHomeAgentAddressDiscoveryReplyMessage, TypeMobilePrefixSolicitation, TypeMobilePrefixAdvertisement, TypeCertificationPathSolicitation, TypeCertificationPathAdvertisement, TypeMulticastRouterAdvertisement, TypeMulticastRouterSolicitation, TypeMulticastRouterTermination, TypeRPLControlMessage, TypePrivateExperimentation3, TypePrivateExperimentation4}

// TypeWithCodeList variable contains the list of Type that have relevant codes
var TypeWithCodeList = []uint8{TypeDestinationUnreachable, TypeTimeExceeded, TypeParameterProblem, TypeRouterRenumbering, TypeICMPNodeInformationQuery, TypeICMPNodeInformationResponse}

// GetCodeForType function returns the revelant codes for a type
func GetCodeForType(typ uint8) []uint8 {
	switch typ {
	case TypeDestinationUnreachable:
		return []uint8{CodeDestinationUnreachableNoRouteToDestination, CodeDestinationUnreachableCommunicationWithDestinationAdministrativelyProhibited, CodeDestinationUnreachableBeyondScopeOfSourceSddress, CodeDestinationUnreachableAddressUnreachable, CodeDestinationUnreachablePortUnreachable, CodeDestinationUnreachableSourceAddressFailedIngressEgressPolicy, CodeDestinationUnreachableRejectRouteToDestination, CodeDestinationUnreachableErrorInSourceRoutingHeader}
	case TypeTimeExceeded:
		return []uint8{CodeTimeExceededHopLimitExceededInTransit, CodeTimeExceededFragmentReassemblyTimeExceeded}
	case TypeParameterProblem:
		return []uint8{CodeParameterProblemErroneousHeaderFieldEncountered, CodeParameterProblemUnrecognizedNextHeaderTypeEncountered, CodeParameterProblemUnrecognizedIPv6OptionEncountered}
	case TypeRouterRenumbering:
		return []uint8{CodeRouterRenumberingRouterRenumberingCommand, CodeRouterRenumberingRouterRenumberingResult, CodeRouterRenumberingSequenceNumberReset}
	case TypeICMPNodeInformationQuery:
		return []uint8{CodeICMPNodeInformationQueryContainsAnIPv6, CodeICMPNodeInformationQueryContainsAName, CodeICMPNodeInformationQueryContainsAnIPv4}
	case TypeICMPNodeInformationResponse:
		return []uint8{CodeICMPNodeInformationResponseASuccessfulReply, CodeICMPNodeInformationResponseTheResponderRefusesToSupplyTheAnswer, CodeICMPNodeInformationResponseTheQtypeOfTheQueryIsUnknownToTheResponder}
	}

	return nil
}

// TypeToString returns a string for a type
func TypeToString(typ uint8) string {
	switch typ {
	case TypeDestinationUnreachable:
		return "Destination unreachable"
	case 2:
		return "Packet too big"
	case TypeTimeExceeded:
		return "Time exceeded"
	case TypeParameterProblem:
		return "Parameter problem "
	case 100:
		return "Private experimentation"
	case 101:
		return "Private experimentation"
	case 128:
		return "Echo Request"
	case 129:
		return "Echo Reply"
	case 130:
		return "Multicast Listener Query"
	case 131:
		return "Multicast Listener Report"
	case 132:
		return "Multicast Listener Done"
	case 133:
		return "Router Solicitation"
	case 134:
		return "Router Advertisement"
	case 135:
		return "Neighbor Solicitation"
	case 136:
		return "Neighbor Advertisement"
	case 137:
		return "Redirect Message"
	case TypeRouterRenumbering:
		return "Router Renumbering "
	case TypeICMPNodeInformationQuery:
		return "ICMP Node Information Query "
	case TypeICMPNodeInformationResponse:
		return "ICMP Node Information Response "
	case 141:
		return "Inverse Neighbor Discovery Solicitation Message"
	case 142:
		return "Inverse Neighbor Discovery Advertisement Message"
	case 143:
		return "Multicast Listener Discovery (MLDv2) reports (RFC 3810)"
	case 144:
		return "Home Agent Address Discovery Request Message"
	case 145:
		return "Home Agent Address Discovery Reply Message"
	case 146:
		return "Mobile Prefix Solicitation"
	case 147:
		return "Mobile Prefix Advertisement"
	case 148:
		return "Certification Path Solicitation"
	case 149:
		return "Certification Path Advertisement"
	case 151:
		return "Multicast Router Advertisement"
	case 152:
		return "Multicast Router Solicitation"
	case 153:
		return "Multicast Router Termination"
	case 155:
		return "RPL Control Message"
	case 200:
		return "Private experimentation"
	case 201:
		return "Private experimentation"
	}

	return "Reserved"
}

// IsReservedtype return true if a type is reserved
func IsReservedtype(typ uint8) bool {
	return (typ > 4 && typ < 100) || (typ > 101 && typ < 128) || typ == 150 || typ == 154 || (typ > 155 && typ < 200) || typ > 201
}

// IsCodeRelevantForType returns true if a type has sub codes.
func IsCodeRelevantForType(typ uint8) bool {
	return typ == TypeDestinationUnreachable || typ == TypeTimeExceeded || typ == TypeParameterProblem || typ == TypeRouterRenumbering || typ == TypeICMPNodeInformationQuery || typ == TypeICMPNodeInformationResponse
}

// CodeToString returns a string corresponding to a type and a code
func CodeToString(typ, code uint8) string {
	switch typ {
	case TypeDestinationUnreachable:
		switch code {
		case 0:
			return "no route to destination "
		case 1:
			return "communication with destination administratively prohibited "
		case 2:
			return "beyond scope of source address "
		case 3:
			return "address unreachable "
		case 4:
			return "port unreachable "
		case 5:
			return "source address failed ingress/egress policy "
		case 6:
			return "reject route to destination "
		case 7:
			return "Error in Source Routing Header "
		default:
			return fmt.Sprintf("%s: unknown code %d", TypeToString(typ), code)
		}
	case TypeTimeExceeded:
		switch code {
		case 0:
			return "hop limit exceeded in transit "
		case 1:
			return "fragment reassembly time exceeded "
		default:
			return fmt.Sprintf("%s: unknown code %d", TypeToString(typ), code)
		}
	case TypeParameterProblem:
		switch code {
		case 0:
			return "erroneous header field encountered "
		case 1:
			return "unrecognized Next Header type encountered "
		case 2:
			return "unrecognized IPv6 option encountered "
		default:
			return fmt.Sprintf("%s: unknown code %d", TypeToString(typ), code)
		}
	case TypeRouterRenumbering:
		switch code {
		case 0:
			return "Router Renumbering Command "
		case 1:
			return "Router Renumbering Result "
		case 255:
			return "Sequence Number Reset "
		default:
			return fmt.Sprintf("%s: unknown code %d", TypeToString(typ), code)
		}
	case TypeICMPNodeInformationQuery:
		switch code {
		case 0:
			return "The Data field contains an IPv6 address which is the Subject of this Query. "
		case 1:
			return "The Data field contains a name which is the Subject of this Query, or is empty, as in the case of a NOOP. "
		case 2:
			return "The Data field contains an IPv4 address which is the Subject of this Query. "
		default:
			return fmt.Sprintf("%s: unknown code %d", TypeToString(typ), code)
		}
	case TypeICMPNodeInformationResponse:
		switch code {
		case 0:
			return "A successful reply. The Reply Data field may or may not be empty. "
		case 1:
			return "The Responder refuses to supply the answer. The Reply Data field will be empty. "
		case 2:
			return "The Qtype of the Query is unknown to the Responder. The Reply Data field will be empty. "
		default:
			return fmt.Sprintf("%s: unknown code %d", TypeToString(typ), code)
		}
	}

	return TypeToString(typ)
}

package icmpv6_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l3/icmp"
	"gitlab.com/go-netnode/process/l3/icmpv6"
	"gitlab.com/go-netnode/process/l3/ipv4"
	"gitlab.com/go-netnode/process/l3/ipv6"
	"gitlab.com/go-netnode/process/processtest"
)

func TestNotEnoughData(t *testing.T) {
	icmpProcess := icmpv6.NewProcess()
	buf, err := icmpProcess.Process(nil, []byte{}, process.Meta{})
	assert.EqualError(t, err, "icmpv6.Process: Not enough data to be an IPv6+ICMP datagram, need at least 44 bytes, got 0")
	assert.Nil(t, buf)
}

func TestInvalidChecksum(t *testing.T) {
	icmpProcess := icmpv6.NewProcess()
	data := [44]byte{}
	buf, err := icmpProcess.Process(nil, data[:], process.Meta{Start: 40, Previous: 0})
	assert.Nil(t, err)
	assert.Nil(t, buf)
}

func TestUnsuportedType(t *testing.T) {
	icmpProcess := icmpv6.NewProcess()
	err := icmpProcess.EnableType(0)
	assert.EqualError(t, err, "unsurported ICMP type: 0")
}

func buildData(t *testing.T, typ uint8, validChecksum bool) []byte {
	var binBuf bytes.Buffer

	// build IPv6 header
	src, err := ipv6.CreateAddress([]byte{0xFE, 0x80}, []byte{0x01})
	assert.NoError(t, err, "ipv6.CreateAddress(1)")
	dst, err := ipv6.CreateAddress([]byte{0xFE, 0x80}, []byte{0x02})
	assert.NoError(t, err, "ipv6.CreateAddress(2)")
	ip6 := ipv6.NewHeader(ipv4.ProtocolIpv6Icmp, src, dst)
	ip6.PayloadLength = 0x40

	err = ip6.Serialize(&binBuf)
	assert.NoError(t, err, "ip.Serialize")

	// build ICMPv6 header
	ic6 := icmp.NewHeader(typ, 0)

	err = ic6.Serialize(&binBuf)
	assert.NoError(t, err, "ic6.Serialize")

	// write identifier & sequence
	binBuf.Write([]byte{0x00, 0x03, 0x00, 0x01})

	// append 56 bytes of data
	var i uint8
	for i = 0; i < 56; i++ {
		err := binBuf.WriteByte(i)
		require.NoError(t, err, "binBuf.WriteByte")
	}

	// Compute icmpv6 checksum
	bytes := binBuf.Bytes()
	checker := ipv4.Checksum{}
	checker.AddData(bytes[8:40])
	checker.AddData([]byte{
		0x00, 0x00, 0x00, 0x40, // length uint32
		0x00, 0x00, 0x00, 0x3A, // 3 padding byte 0x00 + ICMPv6 procole number
	})
	checker.AddData(bytes[40:])
	sum := checker.Compute()
	bytes[42] = byte(sum >> 8)
	bytes[43] = byte(sum & 0x00FF)

	if !validChecksum {
		bytes[43]--
	}

	return bytes
}

var datas = map[string]struct {
	typeToEnable  []uint8
	typeToSend    uint8
	validChecksum bool
	bufferSize    int
	expectedError string
	expectedData  bool
	expectedType  uint8
}{
	"BufferTooSmallToWriteICMPHeader": {
		typeToEnable:  []byte{icmpv6.TypeEchoRequest},
		typeToSend:    icmpv6.TypeEchoRequest,
		validChecksum: true,
		bufferSize:    60,
		expectedError: "icmpv6.Process: error while serializing header to reply: Buffer: not enough space",
		expectedData:  false,
		expectedType:  0,
	},
	"TooSmallToWriteData": {
		typeToEnable:  []byte{icmpv6.TypeEchoRequest},
		typeToSend:    icmpv6.TypeEchoRequest,
		validChecksum: true,
		bufferSize:    3,
		expectedError: "icmpv6.Process: error while writting data to reply: Buffer: not enough space",
		expectedData:  false,
		expectedType:  0,
	},
	"ProcessAnswer": {
		typeToEnable:  []byte{icmpv6.TypeEchoRequest},
		typeToSend:    icmpv6.TypeEchoRequest,
		validChecksum: true,
		bufferSize:    100,
		expectedError: "",
		expectedData:  true,
		expectedType:  icmpv6.TypeEchoReply,
	},
	"ProcessAnswerNotEnabled": {
		typeToEnable:  []byte{icmpv6.TypeEchoReply},
		typeToSend:    icmpv6.TypeEchoRequest,
		validChecksum: true,
		bufferSize:    100,
		expectedError: "",
		expectedData:  false,
		expectedType:  0,
	},
	"ProcessAnswerReply": {
		typeToEnable:  []byte{icmpv6.TypeEchoReply},
		typeToSend:    icmpv6.TypeEchoReply,
		validChecksum: true,
		bufferSize:    100,
		expectedError: "",
		expectedData:  false,
		expectedType:  0,
	},
	"ProcessAnswerInvalidChecksum": {
		typeToEnable:  []byte{icmpv6.TypeEchoRequest},
		typeToSend:    icmpv6.TypeEchoRequest,
		validChecksum: false,
		bufferSize:    100,
		expectedError: "",
		expectedData:  false,
		expectedType:  0,
	},
}

func TestICMPProcess(t *testing.T) {
	for name, data := range datas {
		t.Run(name, func(t *testing.T) {
			icmpProcess := icmpv6.NewProcess()
			for _, typ := range data.typeToEnable {
				err := icmpProcess.EnableType(typ)
				assert.NoError(t, err)
			}

			dataToSend := buildData(t, data.typeToSend, data.validChecksum)
			system := processtest.ProduceProcessData(t, data.bufferSize)
			reply, err := icmpProcess.Process(system, dataToSend, process.Meta{Start: 40, Previous: 0})

			if len(data.expectedError) > 0 {
				assert.EqualError(t, err, data.expectedError)
			} else {
				assert.Nil(t, err)
			}

			if data.expectedData {
				require.NotNil(t, reply)
				bytes := reply.GetBuffer().Bytes()
				checker := ipv4.Checksum{}

				// ICMP Process does not answer the IPv6 header, need to build
				// the pseudo header from data sent.
				checker.AddData(dataToSend[24:40])
				checker.AddData(dataToSend[8:24])
				checker.AddData([]byte{
					0x00, 0x00, 0x00, byte(len(bytes)),
					0x00, 0x00, 0x00, 0x3A,
				})

				checker.AddData(bytes)

				assert.True(t, checker.Verify())
				assert.Equal(t, data.expectedType, bytes[0])
			} else {
				assert.Nil(t, reply)
			}
		})
	}
}

package icmpv6

import (
	"fmt"

	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l3/icmp"
	"gitlab.com/go-netnode/process/l3/ipv4"
	"gitlab.com/go-netnode/utils/levellog"
	"gitlab.com/go-netnode/utils/pool"
)

// Process represents an ICMP process
type Process struct {
	enabledType map[uint8]func(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error)
}

// NewProcess function create a new ICMP process
func NewProcess() *Process {
	return &Process{
		enabledType: make(map[uint8]func(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error)),
	}
}

// EnableType method enables the management of the ICMP type
func (p *Process) EnableType(typ uint8) error {
	if typ == TypeEchoRequest {
		p.enabledType[typ] = processEchoRequest
		return nil
	}

	if typ == TypeEchoReply {
		p.enabledType[typ] = processEchoReply
		return nil
	}

	return fmt.Errorf("unsurported ICMP type: %d", typ)
}

// Process is the common method to process an ICMP request
func (p Process) Process(system *process.System, data []byte, meta process.Meta) (*pool.ReusableBuffer, error) {
	if len(data)-meta.Previous < 44 {
		return nil, fmt.Errorf("icmpv6.Process: Not enough data to be an IPv6+ICMP datagram, need at least 44 bytes, got %d", len(data)-meta.Previous)
	}

	// no ICMP type enabled, nothing to do
	if len(p.enabledType) == 0 {
		return nil, nil
	}

	checker := ipv4.Checksum{}
	// Add ipv6 pseudo header
	checker.AddData(data[meta.Previous+8 : meta.Previous+40])
	checker.AddData([]byte{
		0x00, 0x00, data[meta.Previous+4], data[meta.Previous+5],
		0x00, 0x00, 0x00, 0x3A,
	})

	// Add icmpv6 payload
	checker.AddData(data[meta.Start:])

	// Verify checksum
	if !checker.Verify() {
		levellog.Debugln("icmpv6.Process: invalid checksum")
		return nil, nil
	}

	icmpv6 := icmp.Header{}
	err := icmpv6.Deserialize(data[meta.Start : meta.Start+4])

	if err != nil {
		return nil, fmt.Errorf("icmpv6.Process: Failed to deserialize ARP reply: %w", err)
	}

	var reBuff *pool.ReusableBuffer
	fun, exist := p.enabledType[icmpv6.Type]

	if !exist {
		return nil, nil
	}

	reBuff, err = fun(system, data, meta)

	if err != nil {
		return nil, err
	}

	if reBuff == nil {
		return nil, nil
	}

	// Checksum preparation
	bytes := reBuff.GetBuffer().Bytes()
	checker.Reset()

	// Add ipv6 pseudo header
	checker.AddData(data[meta.Previous+24 : meta.Previous+40])

	// TODO if dest address is multicast, get the interface address
	//	Attention: the dest address choice here must be the same as in IPv6 process
	//
	//	Where is that computation supposed to happen ? only ipv6_process know the addresses
	//	But piv6_process does not know ranges, onLink ranges and other stuff.  This is needed to pick the
	//  best src address (lla, ula, gua, etc.)
	//
	//	It is a bit annoying to have to pick the address twice.
	//		IDEA0: just pick it twice
	//		IDEA1: provide to IPv6 process the address picked in ICMPv6 process
	//		IDEA2: pre-pick the address in IPv6 process and provide it to ICMPv6
	//		IDEA3:
	checker.AddData(data[meta.Previous+8 : meta.Previous+24])

	payloadLength := uint32(len(bytes))
	checker.AddData([]byte{
		byte(payloadLength >> 24), byte(payloadLength >> 16), byte(payloadLength >> 8), byte(payloadLength),
		0x00, 0x00, 0x00, 0x3A,
	})

	// Add icmpv6 payload
	checker.AddData(bytes)

	// Compute then store checksum
	sum := checker.Compute()
	bytes[2] = byte(sum >> 8)
	bytes[3] = byte(sum & 0x00FF)

	return reBuff, nil
}

func processEchoRequest(system *process.System, data []byte, meta process.Meta) (*pool.ReusableBuffer, error) {
	icmpv6Reply := icmp.NewHeader(TypeEchoReply, 0)
	reBuff := system.Device.GetBufferPool().GetBuffer()
	reply := reBuff.GetBuffer()

	// Write header and data answer
	_, err := reply.GetPrepend().Write(data[meta.Start+4:])

	if err != nil {
		return nil, fmt.Errorf("icmpv6.Process: error while writting data to reply: %w", err)
	}

	err = icmpv6Reply.Serialize(reply.GetPrepend())

	if err != nil {
		return nil, fmt.Errorf("icmpv6.Process: error while serializing header to reply: %w", err)
	}

	return reBuff, err
}

func processEchoReply(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error) {
	// So far, do nothing, but going to need to store/process those received data
	return nil, nil
}

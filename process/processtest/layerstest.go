package processtest

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/utils/buffer"
	"gitlab.com/go-netnode/utils/pool"
)

type fakeDevice struct {
	Pool *pool.Pool
}

func (f fakeDevice) GetBufferPool() *pool.Pool {
	return f.Pool
}

// ProduceProcessData function create a valid process.System usable by unit test
// TODO rename this method everywhere by ProduceSystemData
func ProduceProcessData(t *testing.T, bufferSize int) *process.System {
	bpool, err := pool.NewPool(1)
	require.NoError(t, err, "pool.NewPool")

	buffer, err := buffer.NewSimpleBuffer(bufferSize)
	require.NoError(t, err, "buffer.NewSimpleBuffer")

	err = bpool.AddBuffer(buffer)
	require.NoError(t, err, "bpool.AddBuffer")

	return &process.System{
		Device: fakeDevice{
			Pool: bpool,
		},
	}
}

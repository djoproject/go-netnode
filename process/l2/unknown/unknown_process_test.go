package unknown_test

import (
	"bytes"
	"log"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l2/unknown"
)

var llcCiscoFrame = []byte{0x01, 0x00, 0x0c, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x00, 0x00}
var llcSTPFrame = []byte{0x01, 0x80, 0xC2, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x00, 0x00}
var llcOtherFrame = []byte{0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x00, 0x00}
var etherTypeFrame = []byte{0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x08, 0x00}

func TestFrameShorterThan12(t *testing.T) {
	u := unknown.Process{}
	frame := make([]byte, 11)

	var buf bytes.Buffer
	log.SetOutput(&buf)
	_, err := u.Process(nil, frame, process.Meta{})
	log.SetOutput(os.Stderr)

	assert.NoError(t, err)
	assert.True(t, strings.HasSuffix(buf.String(), "Frame shorter than 12 bytes: 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00\n"))
}

func TestFrameShorterThan14(t *testing.T) {
	u := unknown.Process{}
	frame := make([]byte, 13)

	var buf bytes.Buffer
	log.SetOutput(&buf)
	_, err := u.Process(nil, frame, process.Meta{})
	log.SetOutput(os.Stderr)

	assert.NoError(t, err)
	assert.True(t, strings.HasSuffix(buf.String(), "Dst: 00:00:00:00:00:00, Src: 00:00:00:00:00:00, Len: 13, data: 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00\n"), buf.String())
}

func TestFrameLLCCIsco(t *testing.T) {
	u := unknown.Process{}

	var buf bytes.Buffer
	log.SetOutput(&buf)
	_, err := u.Process(nil, llcCiscoFrame, process.Meta{})
	log.SetOutput(os.Stderr)

	assert.NoError(t, err)
	assert.True(t, strings.HasSuffix(buf.String(), "Dst: 01:00:0c:01:02:03 (Cisco), Src: 04:05:06:07:08:09, Len: 14, LLC\n"), buf.String())
}

func TestFrameLLCStp(t *testing.T) {
	u := unknown.Process{}

	var buf bytes.Buffer
	log.SetOutput(&buf)
	_, err := u.Process(nil, llcSTPFrame, process.Meta{})
	log.SetOutput(os.Stderr)

	assert.NoError(t, err)
	assert.True(t, strings.HasSuffix(buf.String(), "Dst: 01:80:c2:01:02:03 (Spanning Tree), Src: 04:05:06:07:08:09, Len: 14, LLC\n"), buf.String())
}

func TestFrameLLCOther(t *testing.T) {
	u := unknown.Process{}

	var buf bytes.Buffer
	log.SetOutput(&buf)
	_, err := u.Process(nil, llcOtherFrame, process.Meta{})
	log.SetOutput(os.Stderr)

	assert.NoError(t, err)
	assert.True(t, strings.HasSuffix(buf.String(), "Dst: 01:02:03:04:05:06, Src: 07:08:09:0a:0b:0c, Len: 14, LLC\n"), buf.String())
}

func TestFrameLLCOtherWithBytes(t *testing.T) {
	u := unknown.Process{
		PrintBytes: true,
	}

	var buf bytes.Buffer
	log.SetOutput(&buf)
	_, err := u.Process(nil, llcOtherFrame, process.Meta{})
	log.SetOutput(os.Stderr)

	assert.NoError(t, err)

	splitted := strings.Split(buf.String(), "\n")
	require.Equal(t, 3, len(splitted))

	assert.True(t, strings.HasSuffix(splitted[0], "Dst: 01:02:03:04:05:06, Src: 07:08:09:0a:0b:0c, Len: 14, LLC"), splitted[0])
	assert.True(t, strings.HasSuffix(splitted[1], "0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09 0x0a 0x0b 0x0c 0x00 0x00"), splitted[1])
}

func TestFrameEtherType(t *testing.T) {
	u := unknown.Process{}

	var buf bytes.Buffer
	log.SetOutput(&buf)
	_, err := u.Process(nil, etherTypeFrame, process.Meta{})
	log.SetOutput(os.Stderr)

	assert.NoError(t, err)
	assert.True(t, strings.HasSuffix(buf.String(), "Dst: 01:02:03:04:05:06, Src: 07:08:09:0a:0b:0c, Len: 14, ethertype: IPv4\n"), buf.String())
}

func TestFrameEtherTypeWithBytes(t *testing.T) {
	u := unknown.Process{
		PrintBytes: true,
	}

	var buf bytes.Buffer
	log.SetOutput(&buf)
	_, err := u.Process(nil, etherTypeFrame, process.Meta{})
	log.SetOutput(os.Stderr)

	assert.NoError(t, err)

	splitted := strings.Split(buf.String(), "\n")
	require.Equal(t, 3, len(splitted))

	assert.True(t, strings.HasSuffix(splitted[0], "Dst: 01:02:03:04:05:06, Src: 07:08:09:0a:0b:0c, Len: 14, ethertype: IPv4"), splitted[0])
	assert.True(t, strings.HasSuffix(splitted[1], "0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09 0x0a 0x0b 0x0c 0x08 0x00"), splitted[1])
}

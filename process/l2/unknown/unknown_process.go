package unknown

import (
	"log"
	"net"

	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l2/ethernet"
	"gitlab.com/go-netnode/utils/pool"
)

// Process is use to print unknown frame
type Process struct {
	PrintBytes bool
}

// Process processes a frame
func (u Process) Process(_ *process.System, data []byte, meta process.Meta) (*pool.ReusableBuffer, error) {
	// Frame too short to contain Destination and Source address
	if len(data)-meta.Start < 12 {
		log.Printf("Frame shorter than 12 bytes: %# x", data[meta.Start:])
		return nil, nil
	}

	destination := net.HardwareAddr(data[meta.Start : meta.Start+6])
	source := net.HardwareAddr(data[meta.Start+6 : meta.Start+12])

	// Frame too short to contain ethertype or length
	if len(data)-meta.Start < 14 {
		formatStr := "Dst: %s, Src: %s, Len: %d, data: %# x\n"
		log.Printf(formatStr, destination, source, len(data)-meta.Start, data[meta.Start:])
		return nil, nil
	}

	// To be an ethertype, etypeOrLength must be bigger than or equal to 0x600(1536)
	// To be a length, etypeOrLength must be lower than or equal to 0x5dc(1500)
	// Other values are "undefined"
	etypeOrLength := uint16(data[meta.Start+12])<<8 + uint16(data[meta.Start+13])

	// LLC case
	if etypeOrLength < 0x600 {
		var formatStr string

		if *(*[3]byte)(data[meta.Start:]) == ethernet.OuiCisco {
			formatStr = "Dst: %s (Cisco), Src: %s, Len: %d, LLC\n"
		} else if *(*[3]byte)(data[meta.Start:]) == ethernet.OuiSpanningTree {
			formatStr = "Dst: %s (Spanning Tree), Src: %s, Len: %d, LLC\n"
		} else {
			formatStr = "Dst: %s, Src: %s, Len: %d, LLC\n"
		}

		log.Printf(formatStr, destination, source, len(data)-meta.Start)

		if u.PrintBytes {
			log.Printf("%# x", data[meta.Start:])
		}

		return nil, nil
	}

	// EtherType case
	etherTypeStr := ethernet.EtherTypeToString(etypeOrLength)
	formatStr := "Dst: %s, Src: %s, Len: %d, ethertype: %s\n"
	log.Printf(formatStr, destination, source, len(data)-meta.Start, etherTypeStr)

	if u.PrintBytes {
		log.Printf("%# x", data[meta.Start:])
	}

	return nil, nil
}

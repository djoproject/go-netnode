package dot1q_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process/l2/dot1q"
	"gitlab.com/go-netnode/process/l2/ethernet"
)

var serializedEthernetWithSimpleTagging = []byte{0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0x81, 0x0, 0x0, 0x1, 0x8, 0x0}

func TestNewEthernetTag(t *testing.T) {
	tag, err := dot1q.NewEthernetTag(ethernet.EtypeVlan, dot1q.PcpBestEffort, false, 1)
	require.NoError(t, err)

	assert.Equal(t, ethernet.EtypeVlan, tag.TPID)
	assert.Equal(t, uint8(0), tag.GetPriorityCodePoint())
	assert.False(t, tag.GetDropEligibleIndicator())
	assert.Equal(t, uint16(1), tag.GetVlanIdentifier())
}

func TestNewEthernetTagWithEligibleDrop(t *testing.T) {
	tag, err := dot1q.NewEthernetTag(ethernet.EtypeVlan, dot1q.PcpBestEffort, true, 1)
	require.NoError(t, err)

	assert.Equal(t, ethernet.EtypeVlan, tag.TPID)
	assert.Equal(t, uint8(0), tag.GetPriorityCodePoint())
	assert.True(t, tag.GetDropEligibleIndicator())
	assert.Equal(t, uint16(1), tag.GetVlanIdentifier())
}

func TestNewEthernetTagInvalidPriority(t *testing.T) {
	_, err := dot1q.NewEthernetTag(ethernet.EtypeVlan, 45, false, 1)
	require.EqualError(t, err, "EthernetTag: pcp can not be higher than 7")
}

func TestNewEthernetTagInvalidVlan(t *testing.T) {
	_, err := dot1q.NewEthernetTag(ethernet.EtypeVlan, dot1q.PcpBestEffort, false, 5000)
	require.EqualError(t, err, "EthernetTag: vlan ID can not be higher than 4095")
}

func TestSetVlanIdentifier(t *testing.T) {
	tag, err := dot1q.NewEthernetTag(ethernet.EtypeVlan, dot1q.PcpBestEffort, false, 1)
	require.NoError(t, err)

	err = tag.SetVlanIdentifier(1000)
	require.NoError(t, err)

	assert.Equal(t, uint16(1000), tag.GetVlanIdentifier())
	assert.Equal(t, uint8(0), tag.GetPriorityCodePoint())
	assert.False(t, tag.GetDropEligibleIndicator())
}

func TestSetVlanIdentifierInvalid(t *testing.T) {
	tag, err := dot1q.NewEthernetTag(ethernet.EtypeVlan, dot1q.PcpBestEffort, false, 1)
	require.NoError(t, err)

	err = tag.SetVlanIdentifier(65000)
	assert.EqualError(t, err, "EthernetTag: vlan ID can not be higher than 4095")
}

func TestSetPriorityCodePoint(t *testing.T) {
	tag, err := dot1q.NewEthernetTag(ethernet.EtypeVlan, dot1q.PcpBestEffort, false, 1)
	require.NoError(t, err)

	err = tag.SetPriorityCodePoint(dot1q.PcpVoice)
	require.NoError(t, err)

	assert.Equal(t, uint16(1), tag.GetVlanIdentifier())
	assert.Equal(t, uint8(dot1q.PcpVoice), tag.GetPriorityCodePoint())
	assert.False(t, tag.GetDropEligibleIndicator())
}

func TestSetPriorityCodePointInvalid(t *testing.T) {
	tag, err := dot1q.NewEthernetTag(ethernet.EtypeVlan, dot1q.PcpBestEffort, false, 1)
	require.NoError(t, err)

	err = tag.SetPriorityCodePoint(32)
	assert.EqualError(t, err, "EthernetTag: pcp can not be higher than 7")
}

func TestSetDropEligibleIndicator(t *testing.T) {
	tag, err := dot1q.NewEthernetTag(ethernet.EtypeVlan, dot1q.PcpBestEffort, false, 1)
	require.NoError(t, err)

	tag.SetDropEligibleIndicator(true)

	assert.Equal(t, uint16(1), tag.GetVlanIdentifier())
	assert.Equal(t, uint8(dot1q.PcpBestEffort), tag.GetPriorityCodePoint())
	assert.True(t, tag.GetDropEligibleIndicator())

	tag.SetDropEligibleIndicator(false)

	assert.Equal(t, uint16(1), tag.GetVlanIdentifier())
	assert.Equal(t, uint8(dot1q.PcpBestEffort), tag.GetPriorityCodePoint())
	assert.False(t, tag.GetDropEligibleIndicator())
}

func TestEthernetWithSimpleTaggingSerialize(t *testing.T) {
	tag, err := dot1q.NewEthernetTag(ethernet.EtypeVlan, dot1q.PcpBestEffort, false, 1)
	require.NoError(t, err)

	e := dot1q.EthernetWithSimpleTagging{
		Destination: [6]byte{1, 2, 3, 4, 5, 6},
		Source:      [6]byte{7, 8, 9, 10, 11, 12},
		Tag:         *tag,
		EtherType:   ethernet.EtypeIPv4,
	}

	var binBuf bytes.Buffer
	err = e.Serialize(&binBuf)
	require.NoError(t, err, "EthernetWithSimpleTagging.Serialize")
	assert.Equal(t, serializedEthernetWithSimpleTagging, binBuf.Bytes())
}

func TestEthernetWithSimpleTaggingDeserialize(t *testing.T) {
	e := dot1q.EthernetWithSimpleTagging{}
	err := e.Deserialize(serializedEthernetWithSimpleTagging)
	require.NoError(t, err, "EthernetWithSimpleTagging.Deserialize")

	assert.Equal(t, [6]byte{1, 2, 3, 4, 5, 6}, e.Destination)
	assert.Equal(t, [6]byte{7, 8, 9, 10, 11, 12}, e.Source)
	assert.Equal(t, uint16(0x8100), e.Tag.TPID)
	assert.Equal(t, uint16(0x0001), e.Tag.TCI)
	assert.Equal(t, ethernet.EtypeIPv4, e.EtherType)
}

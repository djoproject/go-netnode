package dot1q

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"
)

// ----

const (
	// PcpBestEffort : Best effort (default)
	PcpBestEffort uint8 = iota

	// PcpBackground : Background
	PcpBackground

	// PcpExcelentEffort : Excellent effort
	PcpExcelentEffort

	// PcpCritical : Critical applications
	PcpCritical

	// PcpVideo : Video, < 100 ms latency and jitter
	PcpVideo

	// PcpVoice : Voice, < 10 ms latency and jitter
	PcpVoice

	// PcpInternetworkControl : Internetwork control
	PcpInternetworkControl

	// PcpNetworkControl : Network control
	PcpNetworkControl
)

// EthernetTag 802.1p containing TPID, eligible drop bit, pcp, and vlan id.
type EthernetTag struct {
	TPID uint16
	TCI  uint16
}

// NewEthernetTag instanciates a new EthernetTag object
func NewEthernetTag(tpid uint16, priority uint8, DropEligible bool, vlanID uint16) (*EthernetTag, error) {
	if priority > 7 {
		return nil, errors.New("EthernetTag: pcp can not be higher than 7")
	}

	if vlanID > 0x0FFF {
		return nil, errors.New("EthernetTag: vlan ID can not be higher than 4095")
	}

	var eligibleDrop uint16
	if DropEligible {
		eligibleDrop = 0x1000
	}

	return &EthernetTag{
		TPID: tpid,
		TCI:  uint16(priority)<<13 + eligibleDrop + vlanID&0x0FFF,
	}, nil
}

// GetPriorityCodePoint returns the PCP value
func (t EthernetTag) GetPriorityCodePoint() uint8 {
	return uint8(t.TCI >> 13)
}

// SetPriorityCodePoint sets the PCP value
func (t *EthernetTag) SetPriorityCodePoint(priority uint8) error {
	if priority > 7 {
		return errors.New("EthernetTag: pcp can not be higher than 7")
	}

	t.TCI = uint16(priority)<<13 + t.TCI&0x1FFF

	return nil
}

// GetDropEligibleIndicator returns the drop elibible bit on a bool form
func (t EthernetTag) GetDropEligibleIndicator() bool {
	return t.TCI&0x1000 == 0x1000
}

// SetDropEligibleIndicator allows to set or unset the drop elibible bit
func (t *EthernetTag) SetDropEligibleIndicator(state bool) {
	if state {
		t.TCI = t.TCI | 0x1000
	} else {
		t.TCI = t.TCI & 0xEFFF
	}
}

// GetVlanIdentifier returns the vlan ID of the tag
func (t EthernetTag) GetVlanIdentifier() uint16 {
	return t.TCI & 0x0FFF
}

// SetVlanIdentifier allows to set the vlan ID of this tag
func (t *EthernetTag) SetVlanIdentifier(vlanID uint16) error {
	if vlanID > 0x0FFF {
		return errors.New("EthernetTag: vlan ID can not be higher than 4095")
	}

	t.TCI = t.TCI&0xF000 + vlanID&0x0FFF
	return nil
}

// ----

// EthernetWithSimpleTagging is an ethernet header with 802.1q tagging (vlan)
type EthernetWithSimpleTagging struct {
	Destination [6]byte
	Source      [6]byte
	Tag         EthernetTag
	EtherType   uint16
}

// Serialize allows to convert object to []byte
func (s *EthernetWithSimpleTagging) Serialize(writer io.Writer) error {
	return binary.Write(writer, binary.BigEndian, s)
}

// Deserialize allows to convert []byte into a EthernetWithSimpleTagging object
func (s *EthernetWithSimpleTagging) Deserialize(data []byte) error {
	buf := bytes.NewReader(data)
	return binary.Read(buf, binary.BigEndian, s)
}

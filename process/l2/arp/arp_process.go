package arp

import (
	"fmt"

	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l2/ethernet"
	"gitlab.com/go-netnode/utils/levellog"
	"gitlab.com/go-netnode/utils/pool"
)

// Process is used to answer basic arp request
type Process struct {
	ArpTable map[[4]byte][6]byte
}

// SetIP allows to associate an IP address to a MAC address
func (a *Process) SetIP(ip [4]byte, mac [6]byte) {
	if a.ArpTable == nil {
		a.ArpTable = make(map[[4]byte][6]byte)
	}

	a.ArpTable[ip] = mac
}

// UnsetIP allows to remove an IP from the process
func (a *Process) UnsetIP(ip [4]byte) {
	delete(a.ArpTable, ip)
}

// Process is the common method to process a frame
func (a Process) Process(system *process.System, data []byte, meta process.Meta) (*pool.ReusableBuffer, error) {
	request := Packet{}
	err := request.Deserialize(data[meta.Start:])

	if err != nil {
		return nil, fmt.Errorf("Fail to deserialize ARP request: %w", err)
	}

	if request.Htype != 1 {
		levellog.Debugln("Arp request discarded, not an ethernet request")
		return nil, nil
	}

	if request.Ptype != ethernet.EtypeIPv4 {
		levellog.Debugln("Arp request discarded, not an IPv4 request")
		return nil, nil
	}

	if request.Hlen != 6 {
		levellog.Debugln("Arp request discarded, hardware length is not 6")
		return nil, nil
	}

	if request.Plen != 4 {
		levellog.Debugln("Arp request discarded, protocol length is not 4")
		return nil, nil
	}

	ip := request.TPA
	mac, exists := a.ArpTable[ip]

	if !exists {
		levellog.Debugf("Arp request discarded, address %d.%d.%d.%d not in table\n", ip[0], ip[1], ip[2], ip[3])
		return nil, nil
	}

	request.Oper = 2
	request.THA = request.SHA
	request.TPA = request.SPA
	request.SHA = mac
	request.SPA = ip

	reply := system.Device.GetBufferPool().GetBuffer()

	// write arp data
	err = request.Serialize(reply.GetBuffer().GetPrepend())
	if err != nil {
		reply.Release()
		return nil, fmt.Errorf("Failed to serialize ARP reply: %w", err)
	}

	return reply, nil
}

package arp

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
)

// Packet contains an arp full packet data
type Packet struct {
	Htype uint16 // 1=ethernet
	Ptype uint16 // same as ethertype
	Hlen  uint8
	Plen  uint8
	Oper  uint16 // 1=request, 2=reply
	SHA   [6]byte
	SPA   [4]byte
	THA   [6]byte
	TPA   [4]byte
}

// Serialize allows to convert object to []byte
func (s *Packet) Serialize(writer io.Writer) error {
	return binary.Write(writer, binary.BigEndian, s)
}

// Deserialize allows to convert []byte into a Packet
func (s *Packet) Deserialize(data []byte) error {
	if len(data) < 28 {
		return fmt.Errorf("Expecting at least a 28 bytes data, got %d", len(data))
	}

	buf := bytes.NewReader(data)
	return binary.Read(buf, binary.BigEndian, s)
}

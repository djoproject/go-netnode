package arp_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l2/arp"
	"gitlab.com/go-netnode/process/processtest"
)

var ArpRequest = []byte{0x00, 0x01, 0x08, 0x00, 0x06, 0x04, 0x00, 0x01, 0x0c, 0xd6, 0xd5, 0xdc, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x02}

func TestSetIP(t *testing.T) {
	p := arp.Process{}
	p.SetIP([4]byte{1, 2, 3, 4}, [6]byte{1, 2, 3, 4, 5, 6})
	value, exists := p.ArpTable[[4]byte{1, 2, 3, 4}]
	assert.True(t, exists)
	assert.Equal(t, [6]byte{1, 2, 3, 4, 5, 6}, value)
}

func TestUnsetIP(t *testing.T) {
	p := arp.Process{}
	p.SetIP([4]byte{1, 2, 3, 4}, [6]byte{1, 2, 3, 4, 5, 6})
	p.UnsetIP([4]byte{1, 2, 3, 4})
	assert.Equal(t, 0, len(p.ArpTable))
}

// TODO DDT from here.

func TestProcessFrameTooShort(t *testing.T) {
	frame := make([]byte, 27)
	p := arp.Process{}
	reply, err := p.Process(nil, frame, process.Meta{})
	assert.EqualError(t, err, "Fail to deserialize ARP request: Expecting at least a 28 bytes data, got 27")
	assert.Nil(t, reply)
}

func TestProcessHardwareTypeNotEthernet(t *testing.T) {
	frame := make([]byte, 28)
	copy(frame, ArpRequest)
	frame[1] = 2

	p := arp.Process{}
	reply, err := p.Process(nil, frame, process.Meta{})
	require.NoError(t, err)
	assert.Nil(t, reply)
}

func TestProcessProtocolTypeNotIPv4(t *testing.T) {
	frame := make([]byte, 28)
	copy(frame, ArpRequest)
	frame[3] = 6

	p := arp.Process{}
	reply, err := p.Process(nil, frame, process.Meta{})
	require.NoError(t, err)
	assert.Nil(t, reply)
}

func TestProcessHardwareLengthNotSix(t *testing.T) {
	frame := make([]byte, 28)
	copy(frame, ArpRequest)
	frame[4] = 4

	p := arp.Process{}
	reply, err := p.Process(nil, frame, process.Meta{})
	require.NoError(t, err)
	assert.Nil(t, reply)
}

func TestProcessProtocolLengthNotFour(t *testing.T) {
	frame := make([]byte, 28)
	copy(frame, ArpRequest)
	frame[5] = 6

	p := arp.Process{}
	reply, err := p.Process(nil, frame, process.Meta{})
	require.NoError(t, err)
	assert.Nil(t, reply)
}

func TestProcessIpNotInList(t *testing.T) {
	frame := make([]byte, 28)
	copy(frame, ArpRequest)

	p := arp.Process{}
	reply, err := p.Process(nil, frame, process.Meta{})
	require.NoError(t, err)
	assert.Nil(t, reply)
}

func TestProcessBufferTooShort(t *testing.T) {
	p := arp.Process{}
	p.SetIP([4]byte{1, 1, 1, 2}, [6]byte{1, 2, 3, 4, 5, 6})

	pdata := processtest.ProduceProcessData(t, 27)

	reply, err := p.Process(pdata, ArpRequest, process.Meta{})
	assert.EqualError(t, err, "Failed to serialize ARP reply: Buffer: not enough space")
	assert.Nil(t, reply)
}

func TestProcessOk(t *testing.T) {
	p := arp.Process{}
	p.SetIP([4]byte{1, 1, 1, 2}, [6]byte{1, 2, 3, 4, 5, 6})

	pdata := processtest.ProduceProcessData(t, 100)

	reply, err := p.Process(pdata, ArpRequest, process.Meta{})
	require.NoError(t, err)
	require.NotNil(t, reply)

	require.Equal(t, 28, reply.GetBuffer().Len())

	assert.Equal(t, []byte{0x00, 0x01, 0x08, 0x00, 0x06, 0x04, 0x00, 0x02, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x01, 0x01, 0x01, 0x02, 0x0c, 0xd6, 0xd5, 0xdc, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01}, reply.GetBuffer().Bytes())
}

package arp_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process/l2/arp"
)

var SerializedArp = []byte{0x00, 0x01, 0x08, 0x00, 0x06, 0x04, 0x00, 0x01, 0x0c, 0xd6, 0xd5, 0xdc, 0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x02}

func TestSerialize(t *testing.T) {
	arp := arp.Packet{}
	arp.Htype = 0x0001
	arp.Ptype = 0x0800
	arp.Hlen = 0x06
	arp.Plen = 0x04
	arp.Oper = 0x0001
	arp.SHA = [6]byte{0x0c, 0xd6, 0xd5, 0xdc, 0x00, 0x00}
	arp.SPA = [4]byte{0x01, 0x01, 0x01, 0x01}
	arp.THA = [6]byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
	arp.TPA = [4]byte{0x01, 0x01, 0x01, 0x02}

	var binBuf bytes.Buffer
	err := arp.Serialize(&binBuf)
	require.NoError(t, err, "arp.Serialize")
	assert.Equal(t, SerializedArp, binBuf.Bytes())
}

func TestDeserialize(t *testing.T) {
	arp := arp.Packet{}
	err := arp.Deserialize(SerializedArp)
	require.NoError(t, err, "arp.Deserialize")

	assert.Equal(t, uint16(0x0001), arp.Htype)
	assert.Equal(t, uint16(0x0800), arp.Ptype)
	assert.Equal(t, uint8(0x06), arp.Hlen)
	assert.Equal(t, uint8(0x04), arp.Plen)
	assert.Equal(t, uint16(0x0001), arp.Oper)
	assert.Equal(t, [6]byte{0x0c, 0xd6, 0xd5, 0xdc, 0x00, 0x00}, arp.SHA)
	assert.Equal(t, [4]byte{0x01, 0x01, 0x01, 0x01}, arp.SPA)
	assert.Equal(t, [6]byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, arp.THA)
	assert.Equal(t, [4]byte{0x01, 0x01, 0x01, 0x02}, arp.TPA)
}

func TestDeserializeError(t *testing.T) {
	arp := arp.Packet{}
	err := arp.Deserialize([]byte{})
	require.EqualError(t, err, "Expecting at least a 28 bytes data, got 0")
}

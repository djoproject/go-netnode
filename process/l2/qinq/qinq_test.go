package qinq_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process/l2/dot1q"
	"gitlab.com/go-netnode/process/l2/ethernet"
	"gitlab.com/go-netnode/process/l2/qinq"
)

var serializedQinQ = []byte{0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x88, 0xa8, 0x00, 0x1, 0x81, 0x00, 0x00, 0x01, 0x08, 0x00}

func TestSerialize(t *testing.T) {
	tag1, err := dot1q.NewEthernetTag(ethernet.EtypeQINQ, dot1q.PcpBestEffort, false, 1)
	require.NoError(t, err)

	tag2, err := dot1q.NewEthernetTag(ethernet.EtypeVlan, dot1q.PcpBestEffort, false, 1)
	require.NoError(t, err)

	e := qinq.EthernetWithDoubleTagging{
		Destination: [6]byte{1, 2, 3, 4, 5, 6},
		Source:      [6]byte{7, 8, 9, 10, 11, 12},
		FirstTag:    *tag1,
		SecondTag:   *tag2,
		EtherType:   ethernet.EtypeIPv4,
	}

	var binBuf bytes.Buffer
	err = e.Serialize(&binBuf)
	require.NoError(t, err, "EthernetWithDoubleTagging.Serialize")
	assert.Equal(t, serializedQinQ, binBuf.Bytes())
}

func TestDeserialize(t *testing.T) {
	e := qinq.EthernetWithDoubleTagging{}
	err := e.Deserialize(serializedQinQ)
	require.NoError(t, err, "EthernetWithDoubleTagging.Deserialize")

	assert.Equal(t, [6]byte{1, 2, 3, 4, 5, 6}, e.Destination)
	assert.Equal(t, [6]byte{7, 8, 9, 10, 11, 12}, e.Source)
	assert.Equal(t, ethernet.EtypeQINQ, e.FirstTag.TPID)
	assert.Equal(t, uint16(0x0001), e.FirstTag.TCI)
	assert.Equal(t, ethernet.EtypeVlan, e.SecondTag.TPID)
	assert.Equal(t, uint16(0x0001), e.SecondTag.TCI)
	assert.Equal(t, ethernet.EtypeIPv4, e.EtherType)
}

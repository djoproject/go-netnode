package qinq

import (
	"bytes"
	"encoding/binary"
	"io"

	"gitlab.com/go-netnode/process/l2/dot1q"
)

// EthernetWithDoubleTagging is an ethernet header with 802.1ad tagging (qinq)
type EthernetWithDoubleTagging struct {
	Destination [6]byte
	Source      [6]byte
	FirstTag    dot1q.EthernetTag
	SecondTag   dot1q.EthernetTag
	EtherType   uint16
}

// Serialize allows to convert object to []byte
func (s *EthernetWithDoubleTagging) Serialize(writer io.Writer) error {
	return binary.Write(writer, binary.BigEndian, s)
}

// Deserialize allows to convert []byte into a EthernetWithDoubleTagging object
func (s *EthernetWithDoubleTagging) Deserialize(data []byte) error {
	buf := bytes.NewReader(data)
	return binary.Read(buf, binary.BigEndian, s)
}

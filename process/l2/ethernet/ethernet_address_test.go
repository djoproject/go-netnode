package ethernet_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-netnode/process/l2/ethernet"
)

func TestIsUniversalAddress(t *testing.T) {
	assert.False(t, ethernet.IsUniversalAddress(ethernet.MacBroadcast))
	assert.True(t, ethernet.IsUniversalAddress([6]byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01}))
}

func TestIsLocalAddress(t *testing.T) {
	assert.False(t, ethernet.IsLocalAddress([6]byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01}))
	assert.True(t, ethernet.IsLocalAddress(ethernet.MacBroadcast))
}

func TestIsBroadcastAddress(t *testing.T) {
	assert.False(t, ethernet.IsBroadcastAddress([6]byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01}))
	assert.True(t, ethernet.IsBroadcastAddress(ethernet.MacBroadcast))
}

func TestIsUnicastAddress(t *testing.T) {
	assert.False(t, ethernet.IsUnicastAddress(ethernet.MacBroadcast))
	assert.True(t, ethernet.IsUnicastAddress([6]byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01}))
}

func TestIsMulticastAddress(t *testing.T) {
	assert.False(t, ethernet.IsMulticastAddress([6]byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01}))
	assert.True(t, ethernet.IsMulticastAddress(ethernet.MacBroadcast))
}

package ethernet

import (
	"bytes"
	"encoding/binary"
	"io"
)

// XXX right now, these structures are not used by process.  should them be deleted ?

// SimpleHeader represents a basic ethernet header with just destination and source addresses
type SimpleHeader struct {
	Destination [6]byte
	Source      [6]byte
}

// Serialize allows to convert object to []byte
func (s *SimpleHeader) Serialize(writer io.Writer) error {
	return binary.Write(writer, binary.BigEndian, s)
}

// Deserialize allows to convert []byte into an object
func (s *SimpleHeader) Deserialize(data []byte) error {
	buf := bytes.NewReader(data)
	return binary.Read(buf, binary.BigEndian, s)
}

// ----

// HeaderWithEtherType represents an ethernet header with destination and sources addresses, and an ethertype
type HeaderWithEtherType struct {
	Destination [6]byte
	Source      [6]byte
	EtherType   uint16
}

// Serialize allows to convert object to []byte
func (s *HeaderWithEtherType) Serialize(writer io.Writer) error {
	return binary.Write(writer, binary.BigEndian, s)
}

// Deserialize allows to convert []byte into an object
func (s *HeaderWithEtherType) Deserialize(data []byte) error {
	buf := bytes.NewReader(data)
	return binary.Read(buf, binary.BigEndian, s)
}

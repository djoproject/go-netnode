package ethernet

// because this tools run in userland and not on a real wire, there is no:
//  * preamble (7 bytes) => because that's layer 1
//  * SFD (1 bytes) => because that's layer 1
//  * FCS (4 bytes) => it is layer 2, but still managed by NIC
//  * IFG (12 bytes) => because that's layer 1

const (
	// EthernetSize = destination address (2 bytes) + source address (2 bytes) + ethertype or length (2bytes) + payload(1500 bytes)
	EthernetSize = 1514

	// EthernetWithVlanSize = same as EthernetSize + Dot1Q (4 bytes)
	EthernetWithVlanSize = 1518

	// EthernetWithQinQSize = same EthernetWithVlanSize + second Dot1Q (4 bytes)
	EthernetWithQinQSize = 1522
)

package ethernet_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-netnode/process/l2/ethernet"
)

var serializedEthernetHeader = []byte{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x5e, 0x00, 0x53, 0x01, 0x08, 0x00}

func TestEthernetSerialize(t *testing.T) {
	e := ethernet.SimpleHeader{}
	err := e.Deserialize(serializedEthernetHeader)
	assert.NoError(t, err, "ethernet.SimpleHeader")
	assert.Equal(t, e.Destination, ethernet.MacBroadcast)
	assert.Equal(t, e.Source, [6]byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01})
}

func TestEthernetDeserialize(t *testing.T) {
	e := ethernet.SimpleHeader{
		Destination: ethernet.MacBroadcast,
		Source:      [6]byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01},
	}

	var binBuf bytes.Buffer
	err := e.Serialize(&binBuf)
	assert.NoError(t, err, "ethernet.Serialize")
	assert.Equal(t, serializedEthernetHeader[:12], binBuf.Bytes())
}

func TestEthernetWithEtherTypeSerialize(t *testing.T) {
	e := ethernet.HeaderWithEtherType{}
	err := e.Deserialize(serializedEthernetHeader)
	assert.NoError(t, err, "ethernet.SimpleHeader")
	assert.Equal(t, e.Destination, ethernet.MacBroadcast)
	assert.Equal(t, e.Source, [6]byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01})
	assert.Equal(t, e.EtherType, ethernet.EtypeIPv4)
}

func TestEthernetWithEtherTypeDeserialize(t *testing.T) {
	e := ethernet.HeaderWithEtherType{
		Destination: ethernet.MacBroadcast,
		Source:      [6]byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01},
		EtherType:   ethernet.EtypeIPv4,
	}

	var binBuf bytes.Buffer
	err := e.Serialize(&binBuf)
	assert.NoError(t, err, "ethernet.Serialize")
	assert.Equal(t, serializedEthernetHeader, binBuf.Bytes())
}

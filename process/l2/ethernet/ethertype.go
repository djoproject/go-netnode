package ethernet

// TODO update list from https://www.cavebear.com/archive/cavebear/Ethernet/type.html

const (
	// EtypeUndefined is used when etype is not defined.
	EtypeUndefined uint16 = 0x0000
	// EtypeIPv4 is IPv4 ethertype
	EtypeIPv4 uint16 = 0x0800
	// EtypeARP is arp ethertype
	EtypeARP uint16 = 0x0806
	// EtypeWakeOnLAN is wake on lan ethertype
	EtypeWakeOnLAN uint16 = 0x0842
	// EtypeAVTP is avtp ethertype
	EtypeAVTP uint16 = 0x22F0
	// EtypeTRILL is trill ethertype
	EtypeTRILL uint16 = 0x22F3
	// EtypeDECMOPRC is DECMOPRC ethertype
	EtypeDECMOPRC uint16 = 0x6002
	// EtypeDECnetPhase4 is DEC net phase 4 ethertype
	EtypeDECnetPhase4 uint16 = 0x6003
	// EtypeDECLAT is DEC LAT ethertype
	EtypeDECLAT uint16 = 0x6004
	// EtypeRARP is RARP ethertype
	EtypeRARP uint16 = 0x8035
	// EtypeAppleTalk is Apple Talk ethertype
	EtypeAppleTalk uint16 = 0x809B
	// EtypeAARP is AARp ethertype
	EtypeAARP uint16 = 0x80F3
	// EtypeVlan is Vlan ethertype
	EtypeVlan uint16 = 0x8100
	// EtypeSLPP is SLPP ethertype
	EtypeSLPP uint16 = 0x8102
	// EtypeVLACP is VLACP ethertype
	EtypeVLACP uint16 = 0x8103
	// EtypeIPX1 is IPX1 ethertype
	EtypeIPX1 uint16 = 0x8137
	// EtypeIPX2 is IPX2 ethertype
	EtypeIPX2 uint16 = 0x8138
	// EtypeQNXQnet is QNXQ net ethertype
	EtypeQNXQnet uint16 = 0x8204
	// EtypeIPv6 is IPv6 ethertype
	EtypeIPv6 uint16 = 0x86DD
	// EtypeEthernetFlowControl is ethernet flow control ethertype
	EtypeEthernetFlowControl uint16 = 0x8808
	// EtypeLACP is LACP ethertype
	EtypeLACP uint16 = 0x8809
	// EtypeCobraNet is Cobra net ethertype
	EtypeCobraNet uint16 = 0x8819
	// EtypeMPLSUnicast is MPLS unicast ethertype
	EtypeMPLSUnicast uint16 = 0x8847
	// EtypeMPLSMulticast is MPLS multicast ethertype
	EtypeMPLSMulticast uint16 = 0x8848
	// EtypePPPoEDiscovery is PPPoE Discovery ethertype
	EtypePPPoEDiscovery uint16 = 0x8863
	// EtypePPPoESession is PPPoE Session ethertype
	EtypePPPoESession uint16 = 0x8864
	// EtypeJumboFrames is Jumbo Frames ethertype
	EtypeJumboFrames uint16 = 0x8870
	// EtypeHomePlug1_0MME is Home Plug 1 ethertype
	EtypeHomePlug1_0MME uint16 = 0x887B
	// EtypeIEEE802_1X is IEEE802_1X ethertype
	EtypeIEEE802_1X uint16 = 0x888E
	// EtypePROFINET is Profinet ethertype
	EtypePROFINET uint16 = 0x8892
	// EtypeHyperSCSI is SCSI ethertype
	EtypeHyperSCSI uint16 = 0x889A
	// EtypeAoE is AoE ethertype
	EtypeAoE uint16 = 0x88A2
	// EtypeEtherCAT is Ether CAT ethertype
	EtypeEtherCAT uint16 = 0x88A4
	// EtypeQINQ is QinQ ethertype
	EtypeQINQ uint16 = 0x88A8
	// EtypeEthernetPowerlink is Ethernet Power lin ethertype
	EtypeEthernetPowerlink uint16 = 0x88AB
	// EtypeGOOSE is GOOSE ethertype
	EtypeGOOSE uint16 = 0x88B8
	// EtypeGSE is GSE ethertype
	EtypeGSE uint16 = 0x88B9
	// EtypeSV is SV ethertype
	EtypeSV uint16 = 0x88BA
	// EtypeMikrotikRoMON is Mikrotik RoMON ethertype
	EtypeMikrotikRoMON uint16 = 0x88BF
	// EtypeLLDP is LLDP ethertype
	EtypeLLDP uint16 = 0x88CC
	// EtypeSERCOS3 is SERCOS3 ethertype
	EtypeSERCOS3 uint16 = 0x88CD
	// EtypeWSMP is WSMP ethertype
	EtypeWSMP uint16 = 0x88DC
	// EtypeHomePlugAVMME is Home Plug AVMME ethertype
	EtypeHomePlugAVMME uint16 = 0x88E1
	// EtypeMRP is MRP ethertype
	EtypeMRP uint16 = 0x88E3
	// EtypeIEEE802_1AE is IEEE802_1AE ethertype
	EtypeIEEE802_1AE uint16 = 0x88E5
	// EtypePTP is PTP ethertype
	EtypePTP uint16 = 0x88F7
	// EtypeNCSI is NCSI ethertype
	EtypeNCSI uint16 = 0x88F8
	// EtypePRP is PRP ethertype
	EtypePRP uint16 = 0x88FB
	// EtypeIEEE802_1ag is IEEE802_1ag ethertype
	EtypeIEEE802_1ag uint16 = 0x8902
	// EtypeFCoE is FCoE ethertype
	EtypeFCoE uint16 = 0x8906
	// EtypeFCoEInit is FCoE Init ethertype
	EtypeFCoEInit uint16 = 0x8914
	// EtypeRoCE is RoCE ethertype
	EtypeRoCE uint16 = 0x8915
	// EtypeTTE is TTE ethertype
	EtypeTTE uint16 = 0x891D
	// EtypeIEEE1905_1 is IEEE1905_1 ethertype
	EtypeIEEE1905_1 uint16 = 0x893a
	// EtypeHSR is HSR ethertype
	EtypeHSR uint16 = 0x892F
	// EtypeEthernetTesting is Ethernet Testing ethertype
	EtypeEthernetTesting uint16 = 0x9000
	// EtypeQINQOld is QinQ old ethertype
	EtypeQINQOld uint16 = 0x9100
	// EtypeVeritasLLT is Veritas LLT ethertype
	EtypeVeritasLLT uint16 = 0xCAFE
	// EtypeRedundancyTag is Redundancy tag ethertype
	EtypeRedundancyTag uint16 = 0xF1C1
)

// AllEtype contains every known ethernet type.
var AllEtype = []uint16{EtypeUndefined, EtypeIPv4, EtypeARP, EtypeWakeOnLAN, EtypeAVTP, EtypeTRILL, EtypeDECMOPRC, EtypeDECnetPhase4, EtypeDECLAT, EtypeRARP, EtypeAppleTalk, EtypeAARP, EtypeVlan, EtypeSLPP, EtypeVLACP, EtypeIPX1, EtypeIPX2, EtypeQNXQnet, EtypeIPv6, EtypeEthernetFlowControl, EtypeLACP, EtypeCobraNet, EtypeMPLSUnicast, EtypeMPLSMulticast, EtypePPPoEDiscovery, EtypePPPoESession, EtypeJumboFrames, EtypeHomePlug1_0MME, EtypeIEEE802_1X, EtypePROFINET, EtypeHyperSCSI, EtypeAoE, EtypeEtherCAT, EtypeQINQ, EtypeEthernetPowerlink, EtypeGOOSE, EtypeGSE, EtypeSV, EtypeMikrotikRoMON, EtypeLLDP, EtypeSERCOS3, EtypeWSMP, EtypeHomePlugAVMME, EtypeMRP, EtypeIEEE802_1AE, EtypePTP, EtypeNCSI, EtypePRP, EtypeIEEE802_1ag, EtypeFCoE, EtypeFCoEInit, EtypeRoCE, EtypeTTE, EtypeIEEE1905_1, EtypeHSR, EtypeEthernetTesting, EtypeQINQOld, EtypeVeritasLLT, EtypeRedundancyTag}

// EtherTypeToString returns a string representing an etype.
func EtherTypeToString(ethertype uint16) string {
	switch ethertype {
	case 0x0000:
		return "not defined"
	case 0x0800:
		return "IPv4"
	case 0x0806:
		return "ARP"
	case 0x0842:
		return "WakeOnLAN"
	case 0x22F0:
		return "AVTP"
	case 0x22F3:
		return "TRILL"
	case 0x6002:
		return "DECMOPRC"
	case 0x6003:
		return "DECnetPhase4"
	case 0x6004:
		return "DECLAT"
	case 0x8035:
		return "RARP"
	case 0x809B:
		return "AppleTalk"
	case 0x80F3:
		return "AARP"
	case 0x8100:
		return "Vlan"
	case 0x8102:
		return "SLPP"
	case 0x8103:
		return "VLACP"
	case 0x8137:
		return "IPX1"
	case 0x8138:
		return "IPX2"
	case 0x8204:
		return "QNXQnet"
	case 0x86DD:
		return "IPv6"
	case 0x8808:
		return "EthernetFlowControl"
	case 0x8809:
		return "LACP"
	case 0x8819:
		return "CobraNet"
	case 0x8847:
		return "MPLSUnicast"
	case 0x8848:
		return "MPLSMulticast"
	case 0x8863:
		return "PPPoEDiscovery"
	case 0x8864:
		return "PPPoESession"
	case 0x8870:
		return "JumboFrames"
	case 0x887B:
		return "HomePlug1_0MME"
	case 0x888E:
		return "IEEE802_1X"
	case 0x8892:
		return "PROFINET"
	case 0x889A:
		return "HyperSCSI"
	case 0x88A2:
		return "AoE"
	case 0x88A4:
		return "EtherCAT"
	case 0x88A8:
		return "QinQ"
	case 0x88AB:
		return "EthernetPowerlink"
	case 0x88B8:
		return "GOOSE"
	case 0x88B9:
		return "GSE"
	case 0x88BA:
		return "SV"
	case 0x88BF:
		return "MikrotikRoMON"
	case 0x88CC:
		return "LLDP"
	case 0x88CD:
		return "SERCOS3"
	case 0x88DC:
		return "WSMP"
	case 0x88E1:
		return "HomePlugAVMME"
	case 0x88E3:
		return "MRP"
	case 0x88E5:
		return "IEEE802_1AE"
	case 0x88F7:
		return "PTP"
	case 0x88F8:
		return "NCSI"
	case 0x88FB:
		return "PRP"
	case 0x8902:
		return "IEEE802_1ag"
	case 0x8906:
		return "FCoE"
	case 0x8914:
		return "FCoEInit"
	case 0x8915:
		return "RoCE"
	case 0x891D:
		return "TTE"
	case 0x893a:
		return "IEEE1905_1"
	case 0x892F:
		return "HSR"
	case 0x9000:
		return "EthernetTesting"
	case 0x9100:
		return "QinQ (old)"
	case 0xCAFE:
		return "VeritasLLT"
	case 0xF1C1:
		return "RedundancyTag"
	}

	return "Unknown"
}

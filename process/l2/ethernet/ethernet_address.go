package ethernet

var (
	// MacBroadcast is the ethernet broadcast address
	MacBroadcast = [6]byte{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}

	// OuiSpanningTree is the OUI used by spanning tree algorithms
	OuiSpanningTree = [3]byte{0x01, 0x80, 0xC2}

	// OuiCisco for Protocols: CDP/VTP/DTP/PAgP/UDLD
	OuiCisco = [3]byte{0x01, 0x00, 0x0c}
)

// IsUniversalAddress returns true is the address is universal
func IsUniversalAddress(address [6]byte) bool {
	return address[0]&0x2 == 0
}

// IsLocalAddress returns true if the address is local
func IsLocalAddress(address [6]byte) bool {
	return address[0]&0x2 == 2
}

// IsBroadcastAddress returns true if the address is broadcast
func IsBroadcastAddress(address [6]byte) bool {
	return address == MacBroadcast
}

// IsUnicastAddress return true if the address is unicast
func IsUnicastAddress(address [6]byte) bool {
	return address[0]&0x1 == 0
}

// IsMulticastAddress return true if the address is multicast
func IsMulticastAddress(address [6]byte) bool {
	return address[0]&0x1 == 1
}

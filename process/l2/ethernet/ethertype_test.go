package ethernet_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-netnode/process/l2/ethernet"
)

func TestEtherTypeToString(t *testing.T) {
	for _, value := range ethernet.AllEtype {
		str := ethernet.EtherTypeToString(value)
		assert.NotEmpty(t, str)
		assert.NotEqual(t, "Unknown", str)
	}
}

func TestEtherTypeToStringUnknown(t *testing.T) {
	str := ethernet.EtherTypeToString(0xFFFF)
	assert.NotEmpty(t, str)
	assert.Equal(t, "Unknown", str)
}

package ethernet_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/process/l2/ethernet"
	"gitlab.com/go-netnode/process/processtest"
	"gitlab.com/go-netnode/utils/pool"
)

var interfaceAddress = [6]byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01}
var interfaceOUI = [3]byte{0x00, 0x00, 0x5e}
var frame = []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01, 0x00, 0x00, 0x5e, 0x00, 0x53, 0x02}
var frameETYPE = []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01, 0x00, 0x00, 0x5e, 0x00, 0x53, 0x02, 0x08, 0x00}
var frameLLC = []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01, 0x00, 0x00, 0x5e, 0x00, 0x53, 0x02, 0x00, 0x00}

type dumbProcess struct{}

func (d dumbProcess) Process(system *process.System, _ []byte, _ process.Meta) (*pool.ReusableBuffer, error) {
	buff := system.Device.GetBufferPool().GetBuffer()
	_, err := buff.GetBuffer().GetPrepend().Write([]byte{0x44})
	return buff, err
}

type errorProcess struct{}

func (e errorProcess) Process(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error) {
	return nil, errors.New("boom")
}

type emptyProcess struct{}

func (e emptyProcess) Process(*process.System, []byte, process.Meta) (*pool.ReusableBuffer, error) {
	return nil, nil
}

func TestNewProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	assert.NotNil(t, e)
}

func TestRegisterDestProcessNotInitialized(t *testing.T) {
	e := ethernet.Process{}

	err := e.RegisterDestProcess(interfaceAddress, dumbProcess{}, true)
	assert.EqualError(t, err, "Ethernet.Process: Object not initialized, use NewProcess function")
}

func TestRegisterDestProcessOverrideFail(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDestProcess(interfaceAddress, dumbProcess{}, true)
	require.NoError(t, err)

	err = e.RegisterDestProcess(interfaceAddress, dumbProcess{}, false)
	assert.EqualError(t, err, "Ethernet.Process: A process already exists")
}

func TestRegisterDestProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDestProcess(interfaceAddress, dumbProcess{}, true)
	require.NoError(t, err)

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frame, process.Meta{})
	require.NoError(t, err)

	answer := reply.GetBuffer().Bytes()
	require.Equal(t, 13, len(answer))

	assert.Equal(t, answer[0:6], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x02})
	assert.Equal(t, answer[6:12], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01})
	assert.Equal(t, answer[12], byte(0x44))
}

func TestRegisterDestProcessError(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDestProcess(interfaceAddress, errorProcess{}, true)
	require.NoError(t, err)

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frame, process.Meta{})
	require.EqualError(t, err, "Ethernet.Process: Fail to process frame: boom")
	require.Nil(t, reply)
}

func TestRegisterDestAndEthertypeProcessNotInitialized(t *testing.T) {
	e := ethernet.Process{}

	err := e.RegisterDestAndEthertypeProcess(interfaceAddress, ethernet.EtypeIPv4, dumbProcess{}, true)
	assert.EqualError(t, err, "Ethernet.Process: Object not initialized, use NewProcess function")
}

func TestRegisterDestAndEthertypeProcessOverrideFail(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDestAndEthertypeProcess(interfaceAddress, ethernet.EtypeIPv4, dumbProcess{}, true)
	require.NoError(t, err, "RegisterDestAndEthertypeProcess")

	err = e.RegisterDestAndEthertypeProcess(interfaceAddress, ethernet.EtypeIPv4, dumbProcess{}, false)
	assert.EqualError(t, err, "Ethernet.Process: A process already exists")
}

func TestRegisterDestAndEthertypeProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDestAndEthertypeProcess(interfaceAddress, ethernet.EtypeIPv4, dumbProcess{}, true)
	require.NoError(t, err, "RegisterDestAndEthertypeProcess")

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frameETYPE, process.Meta{})
	require.NoError(t, err)
	require.NotNil(t, reply)

	answer := reply.GetBuffer().Bytes()
	require.Equal(t, 15, len(answer))

	assert.Equal(t, answer[0:6], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x02})
	assert.Equal(t, answer[6:12], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01})
	assert.Equal(t, answer[12:15], []byte{0x08, 0x00, 0x44})
}

func TestRegisterLLCProcessNotInitialized(t *testing.T) {
	e := ethernet.Process{}

	err := e.RegisterLLCProcess(interfaceAddress, dumbProcess{}, true)
	assert.EqualError(t, err, "Ethernet.Process: Object not initialized, use NewProcess function")
}

func TestRegisterLLCProcessProcessOverrideFail(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterLLCProcess(interfaceAddress, dumbProcess{}, true)
	require.NoError(t, err)

	err = e.RegisterLLCProcess(interfaceAddress, dumbProcess{}, false)
	assert.EqualError(t, err, "Ethernet.Process: A process already exists")
}

func TestRegisterLLCProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterLLCProcess(interfaceAddress, dumbProcess{}, true)
	require.NoError(t, err)

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frameLLC, process.Meta{})
	require.NoError(t, err)
	require.NotNil(t, reply)

	answer := reply.GetBuffer().Bytes()
	require.Equal(t, 15, len(answer))

	assert.Equal(t, answer[0:6], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x02})
	assert.Equal(t, answer[6:12], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01})
	assert.Equal(t, answer[12:15], []byte{0x00, 0x01, 0x44})
}

// ---

type dumbOUIProcess struct{}

func (d dumbOUIProcess) Process(system *process.System, data []byte, _ process.Meta) (*pool.ReusableBuffer, error) {
	reBuff := system.Device.GetBufferPool().GetBuffer()
	buff := reBuff.GetBuffer()
	_, err := buff.GetPrepend().Write([]byte{0x44})

	if err != nil {
		return nil, err
	}

	// TODO that's a bit annoying to have to manage ethernet header in a sublayer in fact...
	//	it should have access to ethernet header but not having to generate it for the answers
	if len(data) > 12 {
		var err error
		if data[12] == 0x00 && data[13] == 0x00 {
			// LLC
			_, err = buff.GetPrepend().Write([]byte{0x00, 0x01})
		} else {
			// ethertype
			_, err = buff.GetPrepend().Write(data[12:14])
		}

		if err != nil {
			return nil, err
		}
	}
	_, err = buff.GetPrepend().Write(data[0:6])
	if err != nil {
		return nil, err
	}

	_, err = buff.GetPrepend().Write(data[6:12])

	return reBuff, err
}

func TestRegisterOUIProcessNotInitialized(t *testing.T) {
	e := ethernet.Process{}

	err := e.RegisterOUIProcess(interfaceOUI, dumbOUIProcess{}, true)
	assert.EqualError(t, err, "Ethernet.Process: Object not initialized, use NewProcess function")
}

func TestRegisterOUIProcessOverrideFail(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterOUIProcess(interfaceOUI, dumbOUIProcess{}, true)
	require.NoError(t, err)

	err = e.RegisterOUIProcess(interfaceOUI, dumbOUIProcess{}, false)
	assert.EqualError(t, err, "Ethernet.Process: A process already exists")
}

func TestRegisterOUIProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterOUIProcess(interfaceOUI, dumbOUIProcess{}, true)
	require.NoError(t, err)

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frame, process.Meta{})
	require.NoError(t, err)
	require.NotNil(t, reply)

	answer := reply.GetBuffer().Bytes()
	require.Equal(t, 13, len(answer))

	assert.Equal(t, answer[0:6], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x02})
	assert.Equal(t, answer[6:12], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01})
	assert.Equal(t, answer[12], byte(0x44))
}

func TestRegisterOUIProcessError(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterOUIProcess(interfaceOUI, errorProcess{}, true)
	require.NoError(t, err)

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frame, process.Meta{})
	require.EqualError(t, err, "Ethernet.Process: Fail to process frame: boom")
	require.Nil(t, reply)
}

func TestRegisterOUIAndEthertypeProcessNotInitialized(t *testing.T) {
	e := ethernet.Process{}

	err := e.RegisterOUIAndEthertypeProcess(interfaceOUI, ethernet.EtypeIPv4, dumbOUIProcess{}, true)
	assert.EqualError(t, err, "Ethernet.Process: Object not initialized, use NewProcess function")
}

func TestRegisterOUIAndEthertypeProcessOverrideFail(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterOUIAndEthertypeProcess(interfaceOUI, ethernet.EtypeIPv4, dumbOUIProcess{}, true)
	require.NoError(t, err, "RegisterOUIAndEthertypeProcess")

	err = e.RegisterOUIAndEthertypeProcess(interfaceOUI, ethernet.EtypeIPv4, dumbOUIProcess{}, false)
	assert.EqualError(t, err, "Ethernet.Process: A process already exists")
}

func TestRegisterOUIAndEthertypeProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterOUIAndEthertypeProcess(interfaceOUI, ethernet.EtypeIPv4, dumbOUIProcess{}, true)
	require.NoError(t, err, "RegisterOUIAndEthertypeProcess")

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frameETYPE, process.Meta{})
	require.NoError(t, err)
	require.NotNil(t, reply)

	answer := reply.GetBuffer().Bytes()
	require.Equal(t, 15, len(answer))

	assert.Equal(t, answer[0:6], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x02})
	assert.Equal(t, answer[6:12], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01})
	assert.Equal(t, answer[12:15], []byte{0x08, 0x00, 0x44})
}

func TestRegisterOUILLCProcessNotInitialized(t *testing.T) {
	e := ethernet.Process{}

	err := e.RegisterOUILLCProcess(interfaceOUI, dumbOUIProcess{}, true)
	assert.EqualError(t, err, "Ethernet.Process: Object not initialized, use NewProcess function")
}

func TestRegisterOUILLCProcessProcessOverrideFail(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterOUILLCProcess(interfaceOUI, dumbOUIProcess{}, true)
	require.NoError(t, err)

	err = e.RegisterOUILLCProcess(interfaceOUI, dumbOUIProcess{}, false)
	assert.EqualError(t, err, "Ethernet.Process: A process already exists")
}

func TestRegisterOUILLCProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterOUILLCProcess(interfaceOUI, dumbOUIProcess{}, true)
	require.NoError(t, err)

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frameLLC, process.Meta{})
	require.NoError(t, err)
	require.NotNil(t, reply)

	answer := reply.GetBuffer().Bytes()
	require.Equal(t, 15, len(answer))

	assert.Equal(t, answer[0:6], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x02})
	assert.Equal(t, answer[6:12], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01})
	assert.Equal(t, answer[12:15], []byte{0x00, 0x01, 0x44})
}

// ---
func TestRegisterDefaultProcessNotInitialized(t *testing.T) {
	e := ethernet.Process{}

	err := e.RegisterDefaultProcess(dumbOUIProcess{}, true)
	assert.EqualError(t, err, "Ethernet.Process: Object not initialized, use NewProcess function")
}

func TestRegisterDefaultProcessOverrideFail(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDefaultProcess(dumbOUIProcess{}, true)
	require.NoError(t, err)

	err = e.RegisterDefaultProcess(dumbOUIProcess{}, false)
	assert.EqualError(t, err, "Ethernet.Process: A process already exists")
}

func TestRegisterDefaultProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDefaultProcess(dumbOUIProcess{}, true)
	require.NoError(t, err)

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frame, process.Meta{})
	require.NoError(t, err)
	require.NotNil(t, reply)

	answer := reply.GetBuffer().Bytes()
	require.Equal(t, 13, len(answer))

	assert.Equal(t, answer[0:6], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x02})
	assert.Equal(t, answer[6:12], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01})
	assert.Equal(t, answer[12], byte(0x44))
}

func TestRegisterDefaultProcessError(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDefaultProcess(errorProcess{}, true)
	require.NoError(t, err)

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frame, process.Meta{})
	require.EqualError(t, err, "Ethernet.Process: Fail to process frame: boom")
	require.Nil(t, reply)
}

func TestRegisterDefaultEthertypeProcessNotInitialized(t *testing.T) {
	e := ethernet.Process{}

	err := e.RegisterDefaultEthertypeProcess(ethernet.EtypeIPv4, dumbOUIProcess{}, true)
	assert.EqualError(t, err, "Ethernet.Process: Object not initialized, use NewProcess function")
}

func TestRegisterDefaultEthertypeProcessOverrideFail(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDefaultEthertypeProcess(ethernet.EtypeIPv4, dumbOUIProcess{}, true)
	require.NoError(t, err, "RegisterDefaultEthertypeProcess")

	err = e.RegisterDefaultEthertypeProcess(ethernet.EtypeIPv4, dumbOUIProcess{}, false)
	assert.EqualError(t, err, "Ethernet.Process: A process already exists")
}

func TestRegisterDefaultEthertypeProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDefaultEthertypeProcess(ethernet.EtypeIPv4, dumbOUIProcess{}, true)
	require.NoError(t, err, "RegisterDefaultEthertypeProcess")

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frameETYPE, process.Meta{})
	require.NoError(t, err)
	require.NotNil(t, reply)

	answer := reply.GetBuffer().Bytes()
	require.Equal(t, 15, len(answer))

	assert.Equal(t, answer[0:6], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x02})
	assert.Equal(t, answer[6:12], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01})
	assert.Equal(t, answer[12:15], []byte{0x08, 0x00, 0x44})
}

func TestRegisterDefaultLLCProcessNotInitialized(t *testing.T) {
	e := ethernet.Process{}

	err := e.RegisterDefaultLLCProcess(dumbOUIProcess{}, true)
	assert.EqualError(t, err, "Ethernet.Process: Object not initialized, use NewProcess function")
}

func TestRegisterDefaultLLCProcessProcessOverrideFail(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDefaultLLCProcess(dumbOUIProcess{}, true)
	require.NoError(t, err)

	err = e.RegisterDefaultLLCProcess(dumbOUIProcess{}, false)
	assert.EqualError(t, err, "Ethernet.Process: A process already exists")
}

func TestRegisterDefaultLLCProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDefaultLLCProcess(dumbOUIProcess{}, true)
	require.NoError(t, err)

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frameLLC, process.Meta{})
	require.NoError(t, err)
	require.NotNil(t, reply)

	answer := reply.GetBuffer().Bytes()
	require.Equal(t, 15, len(answer))

	assert.Equal(t, answer[0:6], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x02})
	assert.Equal(t, answer[6:12], []byte{0x00, 0x00, 0x5e, 0x00, 0x53, 0x01})
	assert.Equal(t, answer[12:15], []byte{0x00, 0x01, 0x44})
}

// ---

func TestTooShortFrameProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDefaultLLCProcess(dumbOUIProcess{}, true)
	require.NoError(t, err)

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, []byte{0x01}, process.Meta{})
	require.EqualError(t, err, "Ethernet.Process: Expecting at least a 12 bytes data, got 1")
	require.Nil(t, reply)
}

func TestNothingToDoProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDestAndEthertypeProcess(interfaceAddress, ethernet.EtypeIPv6, dumbProcess{}, true)
	require.NoError(t, err, "RegisterDestAndEthertypeProcess")

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frameETYPE, process.Meta{})
	require.NoError(t, err)
	assert.Nil(t, reply)
}

func TestNothingToDoProcessAtAll(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	reply, err := e.Process(nil, frameETYPE, process.Meta{})
	require.NoError(t, err)
	assert.Nil(t, reply)
}

func TestEmptyProcess(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDestAndEthertypeProcess(interfaceAddress, ethernet.EtypeIPv4, emptyProcess{}, true)
	require.NoError(t, err, "RegisterDestAndEthertypeProcess")

	system := processtest.ProduceProcessData(t, 100)
	reply, err := e.Process(system, frameETYPE, process.Meta{})
	require.NoError(t, err)
	assert.Nil(t, reply)
}

func TestNotEnoughSpaceToWriteEthertype(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDestAndEthertypeProcess(interfaceAddress, ethernet.EtypeIPv4, dumbProcess{}, true)
	require.NoError(t, err, "RegisterDestAndEthertypeProcess")

	system := processtest.ProduceProcessData(t, 1)
	reply, err := e.Process(system, frameETYPE, process.Meta{})
	require.EqualError(t, err, "Ethernet.Process: failed to write ethertype: Buffer: not enough space")
	assert.Nil(t, reply)
}

func TestNotEnoughSpaceToWriteLength(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterLLCProcess(interfaceAddress, dumbProcess{}, true)
	require.NoError(t, err, "RegisterLLCProcess")

	system := processtest.ProduceProcessData(t, 1)
	reply, err := e.Process(system, frameLLC, process.Meta{})
	require.EqualError(t, err, "Ethernet.Process: failed to write length: Buffer: not enough space")
	assert.Nil(t, reply)
}

func TestNotEnoughSpaceToWriteSrcAddress(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDestAndEthertypeProcess(interfaceAddress, ethernet.EtypeIPv4, dumbProcess{}, true)
	require.NoError(t, err, "RegisterDestAndEthertypeProcess")

	system := processtest.ProduceProcessData(t, 3)
	reply, err := e.Process(system, frameETYPE, process.Meta{})
	require.EqualError(t, err, "Ethernet.Process: failed to write source address: Buffer: not enough space")
	assert.Nil(t, reply)
}

func TestNotEnoughSpaceToWriteDestAddres(t *testing.T) {
	e := ethernet.NewProcess(interfaceAddress)
	require.NotNil(t, e)

	err := e.RegisterDestAndEthertypeProcess(interfaceAddress, ethernet.EtypeIPv4, dumbProcess{}, true)
	require.NoError(t, err, "RegisterDestAndEthertypeProcess")

	system := processtest.ProduceProcessData(t, 9)
	reply, err := e.Process(system, frameETYPE, process.Meta{})
	require.EqualError(t, err, "Ethernet.Process: failed to write destination address: Buffer: not enough space")
	assert.Nil(t, reply)
}

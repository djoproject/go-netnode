package ethernet

import (
	"errors"
	"fmt"

	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/utils/pool"
)

// TODO does ethertype/llc management have this place here ? isn't it too soon ?
//	A switch should only care about dest and src address
//	maybe move it into its own Process object ?

type ethernetProcessEntry struct {
	process          process.Object            // default process
	etherTypeProcess map[uint16]process.Object // process based on EtherType
	llcProcess       process.Object            // when a length is detected in place of ethertype, try llc
}

// Process is used to answer Ethernet frame
type Process struct {
	hardwareAddress [6]byte

	destMac        map[[6]byte]*ethernetProcessEntry
	destOUI        map[[3]byte]*ethernetProcessEntry
	defaultProcess *ethernetProcessEntry
}

// NewProcess create a new instance of Process
func NewProcess(address [6]byte) *Process {
	toRet := Process{
		hardwareAddress: address,
		destMac:         make(map[[6]byte]*ethernetProcessEntry),
		destOUI:         make(map[[3]byte]*ethernetProcessEntry),
		defaultProcess:  nil,
	}

	return &toRet
}

// --
// -- REGISTER METHODS
// --

// TODO it starts to be a lot of registering process (9), would it be possible to reduce the list ?
//	IDEA 1: take a slice as dest, and check if it is 0, 3, or 6 length, il would merge 3 methods in one, so only 3 methods
//		drawback, while using registering, most of the time, it uses array and not slice
//  IDEA 2: pass ethertype as pointer, if nil, do not care of it.
//		drawback, add pointer management on the user side

// RegisterDestProcess registers a process for a specific MAC address
func (e Process) RegisterDestProcess(dest [6]byte, process process.Object, override bool) error {
	entry, err := e.getDestMacEntry(&dest)

	if err != nil {
		return err
	}

	return register(entry, nil, process, override)
}

// RegisterDestAndEthertypeProcess registers a process for a specific MAC address and an ethertype
func (e Process) RegisterDestAndEthertypeProcess(dest [6]byte, ethertype uint16, process process.Object, override bool) error {
	entry, err := e.getDestMacEntry(&dest)

	if err != nil {
		return err
	}

	return register(entry, &ethertype, process, override)
}

// RegisterLLCProcess registers a process for LLC frame for a specific MAC
func (e *Process) RegisterLLCProcess(dest [6]byte, process process.Object, override bool) error {
	entry, err := e.getDestMacEntry(&dest)

	if err != nil {
		return err
	}

	return registerLLC(entry, process, override)
}

// --

// RegisterOUIProcess registers a process for a specific OUI
func (e Process) RegisterOUIProcess(oui [3]byte, process process.Object, override bool) error {
	entry, err := e.getDestOUIEntry(&oui)

	if err != nil {
		return err
	}

	return register(entry, nil, process, override)
}

// RegisterOUIAndEthertypeProcess registers a process for a specific OUI and an ethertype
func (e Process) RegisterOUIAndEthertypeProcess(oui [3]byte, ethertype uint16, process process.Object, override bool) error {
	entry, err := e.getDestOUIEntry(&oui)

	if err != nil {
		return err
	}

	return register(entry, &ethertype, process, override)
}

// RegisterOUILLCProcess registers a process for LLC frame for a specific OUI
func (e *Process) RegisterOUILLCProcess(oui [3]byte, process process.Object, override bool) error {
	entry, err := e.getDestOUIEntry(&oui)

	if err != nil {
		return err
	}

	return registerLLC(entry, process, override)
}

// --

// RegisterDefaultProcess registers a default process
func (e *Process) RegisterDefaultProcess(process process.Object, override bool) error {
	defaultEntry, err := e.getDefault()

	if err != nil {
		return err
	}

	return register(defaultEntry, nil, process, override)
}

// RegisterDefaultEthertypeProcess registers a default process for an ethertype
func (e *Process) RegisterDefaultEthertypeProcess(ethertype uint16, process process.Object, override bool) error {
	defaultEntry, err := e.getDefault()

	if err != nil {
		return err
	}

	return register(defaultEntry, &ethertype, process, override)
}

// RegisterDefaultLLCProcess registers a process for LLC Frame
func (e *Process) RegisterDefaultLLCProcess(process process.Object, override bool) error {
	defaultEntry, err := e.getDefault()

	if err != nil {
		return err
	}

	return registerLLC(defaultEntry, process, override)
}

// --
// -- REGISTER HELPER METHODS
// --

func (e Process) getDestMacEntry(key *[6]byte) (*ethernetProcessEntry, error) {
	if e.destMac == nil {
		return nil, errors.New("Ethernet.Process: Object not initialized, use NewProcess function")
	}

	entry, exists := e.destMac[*key]

	if !exists {
		entry = &ethernetProcessEntry{
			etherTypeProcess: make(map[uint16]process.Object),
		}
		e.destMac[*key] = entry
	}

	return entry, nil
}

func (e Process) getDestOUIEntry(key *[3]byte) (*ethernetProcessEntry, error) {
	if e.destOUI == nil {
		return nil, errors.New("Ethernet.Process: Object not initialized, use NewProcess function")
	}

	entry, exists := e.destOUI[*key]

	if !exists {
		entry = &ethernetProcessEntry{
			etherTypeProcess: make(map[uint16]process.Object),
		}
		e.destOUI[*key] = entry
	}

	return entry, nil
}

func (e *Process) getDefault() (*ethernetProcessEntry, error) {
	// NOTE: even if the method does not use destMac, the goal is to have a similar
	// behaviour than getDestOUIEntry and getDestMacEntry when Process
	// is wrongly initialized.
	if e.destMac == nil {
		return nil, errors.New("Ethernet.Process: Object not initialized, use NewProcess function")
	}

	if e.defaultProcess == nil {
		e.defaultProcess = &ethernetProcessEntry{
			etherTypeProcess: make(map[uint16]process.Object),
		}
	}
	return e.defaultProcess, nil
}

func register(entry *ethernetProcessEntry, ethertype *uint16, process process.Object, override bool) error {
	if ethertype == nil {
		if !override && entry.process != nil {
			return errors.New("Ethernet.Process: A process already exists")
		}

		entry.process = process
	} else {
		if !override {
			_, exists := entry.etherTypeProcess[*ethertype]

			if exists {
				return errors.New("Ethernet.Process: A process already exists")
			}
		}

		entry.etherTypeProcess[*ethertype] = process
	}

	return nil
}

func registerLLC(entry *ethernetProcessEntry, process process.Object, override bool) error {
	if entry.llcProcess != nil && !override {
		return errors.New("Ethernet.Process: A process already exists")
	}

	entry.llcProcess = process
	return nil
}

// --
// -- PROCESS METHODS
// --

// Process processes an ethernet frame
func (e Process) Process(system *process.System, data []byte, meta process.Meta) (*pool.ReusableBuffer, error) {
	if len(data)-meta.Start < 12 {
		return nil, fmt.Errorf("Ethernet.Process: Expecting at least a 12 bytes data, got %d", len(data)-meta.Start)
	}

	// Check for precise address
	dispatchEntry, exists := e.destMac[*(*[6]byte)(data[meta.Start:])]

	if exists {
		// check for specific mac match
		return e.process(dispatchEntry, system, data, meta.Start, true)
	}

	// check for OUI match
	dispatchEntry, exists = e.destOUI[*(*[3]byte)(data[meta.Start:])]

	if exists {
		return e.process(dispatchEntry, system, data, meta.Start, false)
	}

	// Check for default
	if e.defaultProcess != nil {
		return e.process(e.defaultProcess, system, data, meta.Start, false)
	}

	return nil, nil
}

const (
	ethernetProcessAddNothing = iota
	ethernetProcessAddEtype
	ethernetProcessAddLength
)

func (e Process) process(dispatchEntry *ethernetProcessEntry, system *process.System, data []byte, start int, manageHeader bool) (*pool.ReusableBuffer, error) {
	headerLength := 12
	var processObject process.Object
	headerProcess := ethernetProcessAddNothing

	if len(data)-start >= 14 {
		headerLength = 14

		// To be an ethertype, etypeOrLength must be bigger than or equal to 0x600(1536)
		// To be a length, etypeOrLength must be lower than or equal to 0x5dc(1500)
		// Other values are "undefined"
		etherTypeOrLength := uint16(data[start+12]) << 8
		etherTypeOrLength += uint16(data[start+13])

		if etherTypeOrLength < 0x600 {
			processObject = dispatchEntry.llcProcess
			headerProcess = ethernetProcessAddLength
		} else {
			var exists bool
			processObject, exists = dispatchEntry.etherTypeProcess[etherTypeOrLength]

			if exists {
				headerProcess = ethernetProcessAddEtype
			}
		}
	}

	if processObject == nil {
		// no ethertype match, no llc, no default => nothing to do.
		if dispatchEntry.process == nil {
			return nil, nil
		}

		processObject = dispatchEntry.process
	}

	meta := process.Meta{Start: start + headerLength, Previous: start}
	reply, err := processObject.Process(system, data, meta)

	if err != nil {
		return nil, fmt.Errorf("Ethernet.Process: Fail to process frame: %w", err)
	}

	// is there something to send coming from an upper layer ?
	if reply == nil {
		return nil, nil
	}

	if !manageHeader {
		// The encapsulated layer has managed ethernet if there is any
		return reply, nil
	}

	buffer := reply.GetBuffer()

	if headerProcess == ethernetProcessAddEtype {
		_, err = buffer.GetPrepend().Write(data[start+12 : start+14])
		if err != nil {
			return nil, fmt.Errorf("Ethernet.Process: failed to write ethertype: %w", err)
		}
	} else if headerProcess == ethernetProcessAddLength {
		length := uint16(buffer.Len())
		_, err = buffer.GetPrepend().Write([]byte{byte(length >> 8), byte(length & 0xFF)})
		if err != nil {
			return nil, fmt.Errorf("Ethernet.Process: failed to write length: %w", err)
		}
	}

	// write source address
	// WARNING: can not use the dest address from the original frame as source,
	// it could be a broadcast or multicast address different from the MAC address of this interface
	_, err = buffer.GetPrepend().Write(e.hardwareAddress[:])
	if err != nil {
		return nil, fmt.Errorf("Ethernet.Process: failed to write source address: %w", err)
	}

	// write destination address
	_, err = buffer.GetPrepend().Write(data[start+6 : start+12])
	if err != nil {
		return nil, fmt.Errorf("Ethernet.Process: failed to write destination address: %w", err)
	}

	return reply, nil
}

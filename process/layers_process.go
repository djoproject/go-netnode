package process

import (
	"gitlab.com/go-netnode/utils/pool"
)

// DataDevice object list the needed methods that a device object must
// implements to be used during a process
type DataDevice interface {
	GetBufferPool() *pool.Pool
	// TODO get Mac Address Table
	// TODO get Routing Table
	// ...
}

// DataInterface object list the needed methods that an interface object must
// implement to be used during a process
type DataInterface interface {
	GetName() (string, error)
	Post(*pool.ReusableBuffer) error
}

// System object contains all the needed data a process could need to
// interract with the system (chassis, interface, routing table, etc.)
type System struct {
	// need device
	//	to get access to buffer pool
	//	to get access to mac address table
	//	etc.
	Device DataDevice

	// need the origin interface:
	//	to know where the data comes frome and not forwarding them to it
	//	to maybe answer something through that interface
	//	to have access to interfaces settings (mtu, mac, ip, etc.)
	Interface DataInterface

	// need a reusable buffer to forward it
	// provite a way to inform the receiver routine that the buffer had been
	// forwarded and should not be used anymore
	InputBuffer          *pool.ReusableBuffer
	InputBufferForwarded bool
}

// Meta object contains every meta data that follows the data byte array
type Meta struct {
	// Start variable indicates a process at which index are the data related
	// to it.
	Start int

	// Previous variable indicates a process at which index are the data
	// related to the parent process.  It is useful for:
	//	* icmpv6 that needs access to ipv6 header
	//	* tcp that needs access to ipv4/ipv6 header
	//  * ...
	Previous int
}

// Object interfaces list the needed methods to be a process object
type Object interface {
	Process(*System, []byte, Meta) (*pool.ReusableBuffer, error)
}

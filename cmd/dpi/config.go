package main

import (
	"gitlab.com/go-netnode/process/l2/ethernet"
	"gitlab.com/go-netnode/startup/device"
	"gitlab.com/go-netnode/startup/interfaces"
)

// NewConfig function returns the configuration of a DPI device
func NewConfig() *device.Config {
	config := &device.Config{
		InterfaceNameFormat:  "dpi%d",
		InterfaceBufferCount: 1,
		TotalBufferCount:     1,
		Interfaces:           make(map[string]*interfaces.Config),
	}

	ifaceConfg := interfaces.NewConfig(ethernet.EthernetWithQinQSize, interfaces.ConfigModeNone)
	config.Interfaces["dpi0"] = ifaceConfg

	return config
}

package main

import (
	"flag"
	"os"
	"time"

	"gitlab.com/go-netnode/device"
	config "gitlab.com/go-netnode/startup/device"
	"gitlab.com/go-netnode/utils/levellog"
)

func main() {
	// Flags
	configPath := flag.String("config", "", "Provide a configuration file")
	generate := flag.String("generate", "", "Generate a default configuration file then exit")
	flag.Parse()

	var cfg *config.Config

	// Generate config
	if len(*generate) > 0 {
		cfg = NewConfig()
		err := cfg.Save(*generate)

		if err != nil {
			levellog.Criticalln(err.Error())
			os.Exit(1)
		}

		os.Exit(0)
	}

	// Load config
	if len(*configPath) == 0 {
		cfg = NewConfig()
	} else {
		cfg = &config.Config{}
		err := cfg.Load(*configPath)

		if err != nil {
			levellog.Criticalln(err.Error())
			os.Exit(1)
		}
	}

	// Check config
	issues := make([]string, 0)
	cfg.Validate("dpi", &issues)

	if len(issues) > 0 {
		for _, issue := range issues {
			levellog.Errorln(issue)
		}
		os.Exit(1)
	}

	// Start device
	device, err := device.NewDevice(cfg)
	if err != nil {
		levellog.Criticalf("Error while creating DPI device: %s", err.Error())
		os.Exit(1)
	}

	if err = device.Start(); err != nil {
		levellog.Criticalf("Error while starting DPI device: %s", err.Error())
		os.Exit(1)
	}

	time.Sleep(40 * time.Second) // TODO make a wait group or something and also manage signals.

	// Stop device
	if err = device.Stop(); err != nil {
		levellog.Criticalf("Error while stopping DPI device: %s", err.Error())
		os.Exit(1)
	}
}

package main

import (
	ethernetProcess "gitlab.com/go-netnode/process/l2/ethernet"
	"gitlab.com/go-netnode/startup/device"
	"gitlab.com/go-netnode/startup/interfaces"
	"gitlab.com/go-netnode/startup/l2/ethernet"
	"gitlab.com/go-netnode/startup/l3/ipv4"
)

// NewConfig function returns the configuration of a VPC device
func NewConfig() *device.Config {
	cfg := &device.Config{
		InterfaceNameFormat:  "vpc%d",
		InterfaceBufferCount: 10,
		TotalBufferCount:     10,
		Interfaces:           make(map[string]*interfaces.Config),
	}

	ifaceConfg := interfaces.NewConfig(ethernetProcess.EthernetSize, interfaces.ConfigModeEthernet)
	ifaceConfg.L2Ethernet = ethernet.NewConfig([6]byte{0x6a, 0x6d, 0xb1, 0x38, 0xa5, 0x7b})
	ifaceConfg.L3IPv4 = ipv4.NewConfig(true, true, false, false)
	ifaceConfg.L3IPv4.SetPrimaryAddress("1.1.1.2/24")
	cfg.Interfaces["vpc0"] = ifaceConfg

	return cfg
}

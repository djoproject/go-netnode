#!/usr/bin/env sh
set -e

for path in $(find ./cmd -name main.go -type f); do
  cd $(dirname $path)
  go build
  cd - > /dev/null
done

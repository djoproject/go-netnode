package buffer

import (
	"errors"
	"fmt"
	"io"
)

// DoubleBuffer struct allow to prepend of append data in it
type DoubleBuffer struct {
	data              []byte
	head              int
	back              int
	trueSize          int
	reservedForAppend int
	prependWriter     io.Writer
	appendWriter      io.Writer
}

type innerBuffer struct {
	function func([]byte) (int, error)
}

func (b innerBuffer) Write(data []byte) (int, error) {
	return b.function(data)
}

// NewDoubleBuffer function generates a new buffer object
func NewDoubleBuffer(size int) (*DoubleBuffer, error) {
	if size < 1 {
		return nil, errors.New("Buffer: zero or negative size not allowed")
	}

	buffer := &DoubleBuffer{
		data:              make([]byte, size*2),
		head:              size,
		back:              size,
		trueSize:          size,
		reservedForAppend: 0,
	}

	buffer.prependWriter = innerBuffer{
		function: buffer.PreprendWrite,
	}

	buffer.appendWriter = innerBuffer{
		function: buffer.AppendWrite,
	}

	return buffer, nil
}

// GetPrepend method returns the writer needed to prepend data to the buffer
func (b DoubleBuffer) GetPrepend() io.Writer {
	return b.prependWriter
}

// GetAppend method returner the writer needed to append data to the buffer
func (b DoubleBuffer) GetAppend() io.Writer {
	return b.appendWriter
}

// Len method returns the amount of byte stored in the buffer
func (b DoubleBuffer) Len() int {
	return b.head - b.back
}

// Bytes method returns a slice of byte stored in the buffer
func (b DoubleBuffer) Bytes() []byte {
	return b.data[b.back:b.head]
}

// AppendWrite method writes data at the end of the buffer
func (b *DoubleBuffer) AppendWrite(data []byte) (int, error) {
	if b.head-b.back+len(data) > b.trueSize {
		return 0, errors.New("Buffer: not enough space")
	}

	for index := 0; index < len(data); index++ {
		b.data[b.head+index] = data[index]
	}

	b.head += len(data)

	return len(data), nil
}

// PreprendWrite method writes data at the beginning of the buffer
func (b *DoubleBuffer) PreprendWrite(data []byte) (int, error) {
	if b.head-b.back+len(data) > b.trueSize {
		return 0, errors.New("Buffer: not enough space")
	}

	b.back = b.back - len(data)

	for index := 0; index < len(data); index++ {
		b.data[b.back+index] = data[index]
	}

	return len(data), nil
}

// Reset method emtpies the buffer content
func (b *DoubleBuffer) Reset() {
	b.head = b.trueSize
	b.back = b.trueSize
	b.reservedForAppend = 0
}

// Slice method returns a slice of the buffer content
func (b DoubleBuffer) Slice(i, j int) ([]byte, error) {
	if i < 0 || j < 0 {
		return nil, errors.New("Buffer: negative index not managed")
	}

	if b.back+j < b.head {
		return b.data[b.back+i : b.back+j], nil
	}

	return b.data[b.back+i : b.head], nil
}

// ReserveForAppend method allow to reserve place at the end of the buffer for future appending
func (b *DoubleBuffer) ReserveForAppend(n int) error {
	if b.reservedForAppend+n > b.trueSize {
		return errors.New("Buffer: can not reserve more than the buffer size")
	}

	b.reservedForAppend += n
	return nil
}

// Expand method allow to augment the allocated part of the buffer
func (b *DoubleBuffer) Expand(n int) error {
	if b.Len() != 0 {
		return errors.New("Buffer: can not expand on not empty buffer")
	}

	if n > b.trueSize {
		return errors.New("Buffer: not enough space")
	}

	part := n / 2

	b.back -= part
	b.head += part

	if n%2 == 1 {
		b.back--
	}

	return nil
}

// Shrink method allow to reduce the allocated part of the buffer
func (b *DoubleBuffer) Shrink(i, j int) error {
	if i < 0 || j < 0 {
		return errors.New("Buffer: negative index not managed")
	}

	if j < i {
		return fmt.Errorf("Buffer: invalid slice index: %d > %d", i, j)
	}

	if b.back+i >= b.head {
		return errors.New("Buffer: first index out of range")
	}

	if b.back+j > b.head {
		return errors.New("Buffer: second index out of range")
	}

	b.head = b.back + j
	b.back += i

	return nil
}

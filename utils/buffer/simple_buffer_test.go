package buffer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/utils/buffer"
)

func TestSimpleBufferMatchBufferInterface(t *testing.T) {
	var buff buffer.Buffer
	var err error
	buff, err = buffer.NewSimpleBuffer(1)
	assert.NoError(t, err)
	assert.NotNil(t, buff)
}

func TestSimpleBufferEmptyInit(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(0)
	assert.EqualError(t, err, "Buffer: zero or negative size not allowed")
	assert.Nil(t, b)
}

func TestSimpleBufferPrependAdd(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(5)
	require.NoError(t, err)

	n, err := b.GetPrepend().Write([]byte{3, 4})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetPrepend().Write([]byte{1, 2})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	assert.Equal(t, []byte{1, 2, 3, 4}, b.Bytes())
	assert.Equal(t, 4, b.Len())
}

func TestSimpleBufferPrependAddTooMuch(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(2)
	require.NoError(t, err)

	n, err := b.GetPrepend().Write([]byte{3, 4, 5})
	assert.EqualError(t, err, "Buffer: not enough space")
	assert.Equal(t, 0, n)
}

func TestSimpleBufferReset(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(3)
	require.NoError(t, err)

	n, err := b.GetPrepend().Write([]byte{1, 2})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	assert.Equal(t, 2, b.Len())
	b.Reset()
	assert.Equal(t, 0, b.Len())
}

func TestSimpleBufferReserveForAppendAfterPrepend(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(5)
	require.NoError(t, err)

	n, err := b.GetPrepend().Write([]byte{3, 4})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	err = b.ReserveForAppend(1)
	assert.EqualError(t, err, "Buffer: can not reserve anymore, data have been written in buffer")
}

func TestSimpleBufferReserveForAppendNegativeSize(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(5)
	require.NoError(t, err)

	err = b.ReserveForAppend(-1)
	assert.EqualError(t, err, "Buffer: zero or negative size not allowed")
}

func TestSimpleBufferReserveForAppendNotEnoughSpace(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(5)
	require.NoError(t, err)

	err = b.ReserveForAppend(10)
	assert.EqualError(t, err, "Buffer: can not reserve more than the buffer size")
}

func TestSimpleBufferReserveForAppendMultipleReserve(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	err = b.ReserveForAppend(5)
	require.NoError(t, err)

	err = b.ReserveForAppend(5)
	require.NoError(t, err)
}

func TestSimpleBufferAppendAdd(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(5)
	require.NoError(t, err)

	err = b.ReserveForAppend(5)
	require.NoError(t, err)

	n, err := b.GetAppend().Write([]byte{1, 2})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetAppend().Write([]byte{3, 4})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	assert.Equal(t, []byte{1, 2, 3, 4}, b.Bytes())
	assert.Equal(t, 4, b.Len())
}

func TestSimpleBufferAppendAddTooMuch(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(2)
	require.NoError(t, err)

	n, err := b.GetAppend().Write([]byte{3, 4, 5})
	assert.EqualError(t, err, "Buffer: not enough space")
	assert.Equal(t, 0, n)
}

func TestSimpleBufferAdd(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	err = b.ReserveForAppend(5)
	require.NoError(t, err)

	n, err := b.GetAppend().Write([]byte{5, 6})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetPrepend().Write([]byte{3, 4})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetAppend().Write([]byte{7, 8})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetPrepend().Write([]byte{1, 2})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	assert.Equal(t, []byte{1, 2, 3, 4, 5, 6, 7, 8}, b.Bytes())
	assert.Equal(t, 8, b.Len())
}

func TestSimpleBufferAdd2(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	err = b.ReserveForAppend(5)
	require.NoError(t, err)

	n, err := b.GetPrepend().Write([]byte{3, 4})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetAppend().Write([]byte{5, 6})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetPrepend().Write([]byte{1, 2})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetAppend().Write([]byte{7, 8})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	assert.Equal(t, []byte{1, 2, 3, 4, 5, 6, 7, 8}, b.Bytes())
	assert.Equal(t, 8, b.Len())
}

func TestSimpleBufferSliceNegativeIndex(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	_, err = b.Slice(0, -5)
	assert.EqualError(t, err, "Buffer: negative index not managed")
}

func TestSimpleBufferSliceInvertedIndex(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	_, err = b.Slice(5, 3)
	assert.EqualError(t, err, "Buffer: invalid slice index: 5 > 3")
}

func TestSimpleBufferSliceEmptyBuffer(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	f := func() {
		_, err := b.Slice(0, 10)
		assert.NoError(t, err)
	}

	assert.PanicsWithValue(t, "runtime error: slice bounds out of range [:10] with capacity 0", f)
}

func TestSimpleBufferSlice(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	err = b.ReserveForAppend(5)
	require.NoError(t, err)

	_, err = b.GetAppend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	s, err := b.Slice(0, 2)
	require.NoError(t, err)
	assert.Equal(t, 2, len(s))
	assert.Equal(t, []byte{5, 6}, s)
}

func TestSimpleBufferExpandNotEmpty(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Expand(5)
	assert.EqualError(t, err, "Buffer: can not expand on not empty buffer")
}

func TestSimpleBufferExpandTooSmall(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	err = b.Expand(11)
	assert.EqualError(t, err, "Buffer: not enough space")
}

func TestSimpleBufferExpand(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	err = b.Expand(10)
	require.NoError(t, err)

	assert.Equal(t, 10, b.Len())
}

func TestSimpleBufferShrinkNegativeIndex(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(-5, -10)
	assert.EqualError(t, err, "Buffer: negative index not managed")
}

func TestSimpleBufferShrinkInvalidIndex(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(2, 0)
	assert.EqualError(t, err, "Buffer: invalid slice index: 2 > 0")
}

func TestSimpleBufferShrinkInvalidFirstIndex(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(4, 6)
	assert.EqualError(t, err, "Buffer: first index out of range")
}

func TestSimpleBufferShrinkInvalidSecondIndex(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(1, 6)
	assert.EqualError(t, err, "Buffer: second index out of range")
}

func TestSimpleBufferShrink(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(1, 3)
	require.NoError(t, err)

	assert.Equal(t, 2, b.Len())
	assert.Equal(t, []byte{6, 7}, b.Bytes())
}

func TestSimpleBufferShrink2(t *testing.T) {
	b, err := buffer.NewSimpleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(0, 4)
	require.NoError(t, err)

	assert.Equal(t, 4, b.Len())
	assert.Equal(t, []byte{5, 6, 7, 8}, b.Bytes())
}

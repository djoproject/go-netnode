package buffer

import (
	"errors"
	"fmt"
	"io"
)

// SimpleBuffer struct allow to prepend or append data in it
type SimpleBuffer struct {
	data              []byte
	head              int
	reservedForAppend int
	prependWriter     io.Writer
	appendWriter      io.Writer
}

// NewSimpleBuffer function generates a new SimpleBuffer object
func NewSimpleBuffer(size int) (*SimpleBuffer, error) {
	if size <= 0 {
		return nil, errors.New("Buffer: zero or negative size not allowed")
	}

	buffer := &SimpleBuffer{
		data:              make([]byte, size),
		head:              size,
		reservedForAppend: 0,
	}

	buffer.prependWriter = innerBuffer{
		function: buffer.PrependWrite,
	}

	buffer.appendWriter = innerBuffer{
		function: buffer.AppendWrite,
	}

	return buffer, nil
}

// GetPrepend method returns the writer needed to prepend data to the buffer
func (b SimpleBuffer) GetPrepend() io.Writer {
	return b.prependWriter
}

// GetAppend method returner the writer needed to append data to the buffer
func (b SimpleBuffer) GetAppend() io.Writer {
	return b.appendWriter
}

// Len method returns the amount of byte stored in the buffer
func (b SimpleBuffer) Len() int {
	return len(b.data) - b.head - b.reservedForAppend
}

// Bytes method returns a slice of byte stored in the buffer
func (b SimpleBuffer) Bytes() []byte {
	return b.data[b.head : len(b.data)-b.reservedForAppend]
}

// PrependWrite method writes data at the beginning of the buffer
func (b *SimpleBuffer) PrependWrite(data []byte) (int, error) {
	if len(data) > b.head {
		return 0, errors.New("Buffer: not enough space")
	}

	b.head -= len(data)
	for index := 0; index < len(data); index++ {
		b.data[b.head+index] = data[index]
	}

	return len(data), nil
}

// Reset method emtpies the buffer content
func (b *SimpleBuffer) Reset() {
	b.head = len(b.data)
	b.reservedForAppend = 0
}

// Slice method returns a slice of the buffer content
func (b SimpleBuffer) Slice(i, j int) ([]byte, error) {
	if i < 0 || j < 0 {
		return nil, errors.New("Buffer: negative index not managed")
	}

	if j < i {
		return nil, fmt.Errorf("Buffer: invalid slice index: %d > %d", i, j)
	}

	if b.head+j > len(b.data)-b.reservedForAppend {
		// not a huge fan of doing a panic, but this is what GO slice does in that case.
		panic(fmt.Sprintf("runtime error: slice bounds out of range [:%d] with capacity %d", j, b.Len()))
	}

	return b.data[b.head+i : b.head+j], nil
}

// ReserveForAppend method allow to reserve place at the end of the buffer for future appending
func (b *SimpleBuffer) ReserveForAppend(n int) error {
	if b.head != len(b.data)-b.reservedForAppend {
		return errors.New("Buffer: can not reserve anymore, data have been written in buffer")
	}

	if n <= 0 {
		return errors.New("Buffer: zero or negative size not allowed")
	}

	if b.reservedForAppend+n > len(b.data) {
		return errors.New("Buffer: can not reserve more than the buffer size")
	}

	b.head -= n
	b.reservedForAppend += n
	return nil
}

// AppendWrite method writes data at the end of the buffer
func (b *SimpleBuffer) AppendWrite(data []byte) (int, error) {
	if len(data) > b.reservedForAppend {
		return 0, errors.New("Buffer: not enough space")
	}

	for index := 0; index < len(data); index++ {
		b.data[len(b.data)-b.reservedForAppend+index] = data[index]
	}
	b.reservedForAppend -= len(data)

	return len(data), nil
}

// Expand method allow to augment the allocated part of the buffer
func (b *SimpleBuffer) Expand(n int) error {
	if b.Len() != 0 {
		return errors.New("Buffer: can not expand on not empty buffer")
	}

	if b.head < n {
		return errors.New("Buffer: not enough space")
	}

	b.head -= n
	return nil
}

// Shrink method allow to reduce the allocated part of the buffer
func (b *SimpleBuffer) Shrink(i, j int) error {
	if i < 0 || j < 0 {
		return errors.New("Buffer: negative index not managed")
	}

	if j < i {
		return fmt.Errorf("Buffer: invalid slice index: %d > %d", i, j)
	}

	if b.head+i >= len(b.data)-b.reservedForAppend {
		return errors.New("Buffer: first index out of range")
	}

	if b.head+j > len(b.data)-b.reservedForAppend {
		return errors.New("Buffer: second index out of range")
	}

	b.reservedForAppend = len(b.data) - b.head - j
	b.head += i

	return nil
}

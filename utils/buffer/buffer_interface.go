package buffer

import "io"

// Buffer interface contains the needed method to be a buffer
type Buffer interface {
	Len() int
	Bytes() []byte
	Reset()
	Slice(i, j int) ([]byte, error)
	ReserveForAppend(n int) error
	GetPrepend() io.Writer
	GetAppend() io.Writer
	Expand(n int) error
	Shrink(i, j int) error
}

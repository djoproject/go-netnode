package buffer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-netnode/utils/buffer"
)

func TestDoubleBufferMatchBufferInterface(t *testing.T) {
	var buff buffer.Buffer
	var err error
	buff, err = buffer.NewDoubleBuffer(1)
	assert.NoError(t, err)
	assert.NotNil(t, buff)
}

func TestDoubleBufferEmptyInit(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(0)
	assert.EqualError(t, err, "Buffer: zero or negative size not allowed")
	assert.Nil(t, b)
}

func TestDoubleBufferPrependAdd(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(5)
	require.NoError(t, err)

	n, err := b.GetPrepend().Write([]byte{3, 4})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetPrepend().Write([]byte{1, 2})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	assert.Equal(t, []byte{1, 2, 3, 4}, b.Bytes())
	assert.Equal(t, 4, b.Len())
}

func TestDoubleBufferPrependAddTooMuch(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(2)
	require.NoError(t, err)

	n, err := b.GetPrepend().Write([]byte{3, 4, 5})
	assert.EqualError(t, err, "Buffer: not enough space")
	assert.Equal(t, 0, n)
}

func TestDoubleBufferReset(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(3)
	require.NoError(t, err)

	n, err := b.GetPrepend().Write([]byte{1, 2})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	assert.Equal(t, 2, b.Len())
	b.Reset()
	assert.Equal(t, 0, b.Len())
}

func TestDoubleBufferAppendAdd(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(5)
	require.NoError(t, err)

	n, err := b.GetAppend().Write([]byte{1, 2})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetAppend().Write([]byte{3, 4})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	assert.Equal(t, []byte{1, 2, 3, 4}, b.Bytes())
	assert.Equal(t, 4, b.Len())
}

func TestDoubleBufferAppendAddTooMuch(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(2)
	require.NoError(t, err)

	n, err := b.GetAppend().Write([]byte{3, 4, 5})
	assert.EqualError(t, err, "Buffer: not enough space")
	assert.Equal(t, 0, n)
}

func TestDoubleBufferAdd(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	n, err := b.GetAppend().Write([]byte{5, 6})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetPrepend().Write([]byte{3, 4})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetAppend().Write([]byte{7, 8})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetPrepend().Write([]byte{1, 2})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	assert.Equal(t, []byte{1, 2, 3, 4, 5, 6, 7, 8}, b.Bytes())
	assert.Equal(t, 8, b.Len())
}

func TestDoubleBufferAddi2(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	n, err := b.GetPrepend().Write([]byte{3, 4})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetAppend().Write([]byte{5, 6})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetPrepend().Write([]byte{1, 2})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	n, err = b.GetAppend().Write([]byte{7, 8})
	require.NoError(t, err, "b.Write")
	assert.Equal(t, 2, n)

	assert.Equal(t, []byte{1, 2, 3, 4, 5, 6, 7, 8}, b.Bytes())
	assert.Equal(t, 8, b.Len())
}

func TestDoubleBufferSliceNegativeIndex(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	_, err = b.Slice(0, -5)
	assert.EqualError(t, err, "Buffer: negative index not managed")
}

func TestDoubleBufferSliceEmptyBuffer(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	s, err := b.Slice(0, 10)
	require.NoError(t, err)
	assert.Equal(t, 0, len(s))
}

func TestDoubleBufferSlice(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetAppend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	s, err := b.Slice(0, 2)
	require.NoError(t, err)
	assert.Equal(t, 2, len(s))
	assert.Equal(t, []byte{5, 6}, s)
}

func TestDoubleBufferReserveForAppend(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	err = b.ReserveForAppend(5)
	require.NoError(t, err, "b.ReserveForAppend")
}

func TestDoubleBufferReserveTooMuchForAppend(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	err = b.ReserveForAppend(15)
	assert.EqualError(t, err, "Buffer: can not reserve more than the buffer size")
}

func TestDoubleBufferExpandNotEmpty(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Expand(5)
	assert.EqualError(t, err, "Buffer: can not expand on not empty buffer")
}

func TestDoubleBufferExpandTooSmall(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	err = b.Expand(11)
	assert.EqualError(t, err, "Buffer: not enough space")
}

func TestDoubleBufferExpandOddSize(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	err = b.Expand(9)
	require.NoError(t, err)

	assert.Equal(t, 9, b.Len())
}

func TestDoubleBufferExpand(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	err = b.Expand(10)
	require.NoError(t, err)

	assert.Equal(t, 10, b.Len())
}

func TestDoubleBufferShrinkNegativeIndex(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(-5, -10)
	assert.EqualError(t, err, "Buffer: negative index not managed")
}

func TestDoubleBufferShrinkInvalidIndex(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(2, 0)
	assert.EqualError(t, err, "Buffer: invalid slice index: 2 > 0")
}

func TestDoubleBufferShrinkInvalidFirstIndex(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(4, 6)
	assert.EqualError(t, err, "Buffer: first index out of range")
}

func TestDoubleBufferShrinkInvalidSecondIndex(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(1, 6)
	assert.EqualError(t, err, "Buffer: second index out of range")
}

func TestDoubleBufferShrink(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(1, 3)
	require.NoError(t, err)

	assert.Equal(t, 2, b.Len())
	assert.Equal(t, []byte{6, 7}, b.Bytes())
}

func TestDoubleBufferShrink2(t *testing.T) {
	b, err := buffer.NewDoubleBuffer(10)
	require.NoError(t, err)

	_, err = b.GetPrepend().Write([]byte{5, 6, 7, 8})
	require.NoError(t, err, "b.Write")

	err = b.Shrink(0, 4)
	require.NoError(t, err)

	assert.Equal(t, 4, b.Len())
	assert.Equal(t, []byte{5, 6, 7, 8}, b.Bytes())
}

package pool_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-netnode/utils/buffer"
	"gitlab.com/go-netnode/utils/pool"
)

func TestNewPoolNegativeSize(t *testing.T) {
	p, err := pool.NewPool(-1)
	assert.EqualError(t, err, "Pool: size bellow 0 not allowed, got -1")
	assert.Nil(t, p)
}

func TestNewPool(t *testing.T) {
	p, err := pool.NewPool(1)
	assert.NoError(t, err)
	assert.NotNil(t, p)
}

func TestPoolAddBufferFull(t *testing.T) {
	p, err := pool.NewPool(1)
	assert.NoError(t, err)

	b, err := buffer.NewSimpleBuffer(10)
	assert.NoError(t, err)

	err = p.AddBuffer(b)
	assert.NoError(t, err)

	b, err = buffer.NewSimpleBuffer(10)
	assert.NoError(t, err)

	err = p.AddBuffer(b)
	assert.EqualError(t, err, "Pool: has reached full capacity")
}

func TestPoolGetBuffer(t *testing.T) {
	p, err := pool.NewPool(1)
	assert.NoError(t, err)

	b, err := buffer.NewSimpleBuffer(10)
	assert.NoError(t, err)

	err = p.AddBuffer(b)
	assert.NoError(t, err)

	b2 := p.GetBuffer()
	assert.Equal(t, b, b2.GetBuffer())

	b2.Release()
}

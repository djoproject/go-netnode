package pool

import (
	"errors"
	"fmt"
	"sync"

	"gitlab.com/go-netnode/utils/buffer"
)

// ReusableBuffer object represents the internal state of a reusable buffer
type ReusableBuffer struct {
	buffer    buffer.Buffer
	pool      *Pool
	broadcast bool
	wait      sync.WaitGroup
}

// SetBroadcast method set a reusable buffer in broacast mode
func (r *ReusableBuffer) SetBroadcast(count int) error {
	if count < 2 {
		return errors.New("ReusableBuffer: SetBroadcast, count inferior to 2 is not allowed")
	}

	if r.broadcast {
		return errors.New("ReusableBuffer: SetBroadcast, already in broadcast mode")
	}

	r.wait.Add(count)

	go func() {
		r.wait.Wait()
		r.broadcast = false
		r.Release()
	}()

	return nil
}

// Release method releases the reusable buffer
func (r *ReusableBuffer) Release() {
	if r.broadcast {
		r.wait.Done()
	} else {
		r.buffer.Reset()
		r.pool.buffers <- r
	}
}

// GetBuffer method allows to retrieve internal buffer
func (r *ReusableBuffer) GetBuffer() buffer.Buffer {
	return r.buffer
}

// Pool object represents a pool of reusable buffer
type Pool struct {
	buffers chan *ReusableBuffer
	added   int
}

// NewPool function create a new buffer pool
func NewPool(size int) (*Pool, error) {
	if size < 1 {
		return nil, fmt.Errorf("Pool: size bellow 0 not allowed, got %d", size)
	}

	return &Pool{
		buffers: make(chan *ReusableBuffer, size),
		added:   0,
	}, nil
}

// GetBuffer method returns the next available reusable buffer
func (p Pool) GetBuffer() *ReusableBuffer {
	return <-p.buffers
}

// AddBuffer method allow to add a buffer into the pool
func (p *Pool) AddBuffer(buffer buffer.Buffer) error {
	if p.added == cap(p.buffers) {
		return errors.New("Pool: has reached full capacity")
	}

	p.buffers <- &ReusableBuffer{
		buffer: buffer,
		pool:   p,
	}

	p.added++
	return nil
}

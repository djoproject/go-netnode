package tap

// Inspired by:
// https://github.com/songgao/water
// https://github.com/pkg/taptun

// Documentation/source:
// https://www.kernel.org/doc/Documentation/networking/tuntap.txt (old)
// https://github.com/torvalds/linux/blob/master/Documentation/networking/tuntap.rst
// https://github.com/torvalds/linux/blob/master/include/uapi/linux/if_tun.h
// https://github.com/torvalds/linux/blob/master/drivers/net/tap.c
// https://github.com/torvalds/linux/blob/master/drivers/net/tun.c

// Others:
// https://floating.io/2016/05/tuntap-demystified/4/ (no precise enough, contains mistakes)

// TODO (later) explore XDP socket
// TODO (later) explore zero copy

// TODO does it need multi queue if two different routine read/write the interface ?
//	need tests

// TODO need to be root to start the software => that's annoying
//	isn't there a group to add to the user to be able to avoid that ?

// TODO about group/user owner, because stuff is started with sudo, file descriptor is root owned
//
// Set owner
// if err = ioctl(fd, syscall.TUNSETOWNER, uintptr(os.Getuid())); err != nil {
//	return closeOnError(fdInt, err)
// }
//
// Set group
// if err = ioctl(fd, syscall.TUNSETGROUP, uintptr(os.Getgid())); err != nil {
// return closeOnError(fdInt, err)
// }

// TODO does consecutive open/close/open works ?
//	need tests

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"sync"
	"syscall"
	"unsafe"

	"gitlab.com/go-netnode/process"
	"gitlab.com/go-netnode/utils/levellog"
	"gitlab.com/go-netnode/utils/pool"
)

const (
	// cIFFTUN = 0x0001
	// flag in Linux kernel: IFF_TUN
	// Create a TUN interface (ip header only)

	cIFFTAP = 0x0002
	// flag in Linux kernel: IFF_TAP
	// Create a TAP interface (ethernet header)

	cIFFNOPI = 0x1000
	// flag in Linux kernel: IFF_NO_PI
	// Without it, it prepends 4 bytes before the frame.  Prepends occur on both TUN and TAP types.
	//	 2 bytes flags (there is only one flag possible)
	//		TUN_PKT_STRIP	0x0001
	//			https://elixir.bootlin.com/linux/latest/source/drivers/net/tun.c#L2000
	//	 2 bytes ethertype

	// cIFFMULTIQUEUE = 0x0100 // IFF_MULTI_QUEUE // Create a queue of multiqueue device (??)

	// See file if_tun.h for more flags
)

// TODO is this really needed to communicate with the ubridge made to gns3 ?
//	does it only enable interface on Linux side ?
//  NEED TEST
func setInterfaceState(name string, state bool) error {
	var action string

	if state {
		action = "up"
	} else {
		action = "down"
	}

	err := exec.Command("ip", "link", "set", name, action).Run()
	if err != nil {
		return err
	}

	return nil
}

func ioctl(fd uintptr, request uintptr, argp uintptr) error {
	_, _, errno := syscall.Syscall(syscall.SYS_IOCTL, fd, uintptr(request), argp)
	if errno != 0 {
		return os.NewSyscallError("ioctl", errno)
	}
	return nil
}

// this struct needs to be 40 bytes, why ?
type ifReq struct {
	Name  [16]byte
	Flags uint16
	pad   [22]byte //nolint:structcheck
}

// Interface object represent a physical tap interface
type Interface struct {
	// TODO improve private/public fields
	file           *os.File
	FormatName     string // should have a form like "tun%d"
	allocatedName  string // should be something like tun0, tun1, tun2, ...
	Dispatch       process.Object
	MTU            int
	opened         bool
	device         process.DataDevice
	outputChannel  chan *pool.ReusableBuffer
	openCloseMutex sync.Mutex
	bufferCount    int
	wg             sync.WaitGroup
}

// NewInterface function create a new Interface object
func NewInterface(formatName string, mtu int, dispatch process.Object, device process.DataDevice, buffCount int) *Interface {
	return &Interface{
		FormatName:    formatName,
		Dispatch:      dispatch,
		MTU:           mtu,
		opened:        false,
		device:        device,
		outputChannel: nil,
		bufferCount:   buffCount,
	}
}

// The opening process has failed in a way, try to close the file descriptor
func closeOnError(fdInt int, err error) error {
	err2 := syscall.Close(fdInt)
	if err2 != nil {
		return fmt.Errorf("got error '%s' while managing error '%w'", err2.Error(), err)
	}

	return err
}

// Open method open the interface
func (t *Interface) Open() error {
	if t.opened {
		return nil
	}

	t.openCloseMutex.Lock()
	defer t.openCloseMutex.Unlock()

	if t.opened {
		return nil
	}

	// Step 1: Open /dev/net/tun
	var fdInt int
	fdInt, err := syscall.Open("/dev/net/tun", os.O_RDWR|syscall.O_NONBLOCK, 0)
	if err != nil {
		return err
	}

	// Step 2: Create the tun/tap device with an ioctl call
	var req ifReq
	req.Flags = cIFFTAP | cIFFNOPI // | cIFFMULTIQUEUE
	copy(req.Name[:], t.FormatName)

	fd := uintptr(fdInt)
	err = ioctl(fd, syscall.TUNSETIFF, uintptr(unsafe.Pointer(&req)))
	if err != nil {
		return closeOnError(fdInt, err)
	}

	// Step 3: Indicate the interface should not persist when the program exit
	if err = ioctl(fd, syscall.TUNSETPERSIST, uintptr(0)); err != nil {
		return closeOnError(fdInt, err)
	}

	// Step 4: converting information from system to go
	t.allocatedName = strings.Trim(string(req.Name[:]), "\x00")
	t.file = os.NewFile(fd, t.allocatedName)

	// Step 5: enable interface on Linux side
	err = setInterfaceState(t.allocatedName, true)
	if err != nil {
		return closeOnError(fdInt, err)
	}

	// Step 6: Starting routines
	t.outputChannel = make(chan *pool.ReusableBuffer, t.bufferCount)
	t.wg.Add(2)
	go t.receiceRoutine()
	go t.sendRoutine()

	// end
	t.opened = true
	return nil
}

// GetName method returns the name allocated to the interface
func (t *Interface) GetName() (string, error) {
	if !t.opened {
		return "", errors.New("interface not opened, name not yet defined")
	}

	return t.allocatedName, nil
}

// Close method closes the interface
func (t *Interface) Close() error {
	if !t.opened {
		return nil
	}

	t.openCloseMutex.Lock()
	defer t.openCloseMutex.Unlock()

	if !t.opened {
		return nil
	}

	close(t.outputChannel)
	err1 := setInterfaceState(t.allocatedName, false)
	err2 := t.file.Close()

	t.allocatedName = ""
	t.file = nil
	t.opened = false

	if err1 != nil {
		if err2 != nil {
			return fmt.Errorf("Failed to disable interface '%s' and to close interface file: %w", err1.Error(), err2)
		}
		return fmt.Errorf("Failed to disable interface: %w", err1)
	} else if err2 != nil {
		return fmt.Errorf("Failed to close interface file: %w", err2)
	}
	return nil
}

// WaitClosing method wait until the interface has finished to close
func (t *Interface) WaitClosing() error {
	// TODO should return an error if not in a closing process

	t.wg.Wait()
	return nil
}

func (t *Interface) receiceRoutine() {
	defer t.wg.Done()

	systemData := &process.System{
		Device:               t.device,
		Interface:            t, // TODO
		InputBuffer:          nil,
		InputBufferForwarded: true,
	}

	for {
		if systemData.InputBufferForwarded {
			systemData.InputBuffer = t.device.GetBufferPool().GetBuffer()
			systemData.InputBufferForwarded = false
		}
		subBuffer := systemData.InputBuffer.GetBuffer()
		subBuffer.Reset()

		err := subBuffer.Expand(t.MTU)
		if err != nil {
			levellog.Errorf("Error while expanding buffer to MTU size: %s", err.Error())
			continue
		}

		inputFrame := subBuffer.Bytes()

		// wait for frame.  This call is blocking but returns when file is closed.
		n, err := t.file.Read(inputFrame)
		if err != nil {
			if errors.Is(err, os.ErrClosed) {
				break
			}

			levellog.Errorf("Error while reading from interface: %s", err.Error())
		}

		if n == 0 {
			continue
		}

		err = subBuffer.Shrink(0, n)
		if err != nil {
			levellog.Errorf("Error while shrinking buffer: %s", err.Error())
			continue
		}

		reply, err := t.Dispatch.Process(systemData, subBuffer.Bytes(), process.Meta{})
		if err != nil {
			levellog.Errorf("Error occurs while dispatching the frame: %s\n", err.Error())
			continue
		}

		if reply != nil {
			t.outputChannel <- reply
		}
	}

	if !systemData.InputBufferForwarded {
		systemData.InputBuffer.Release()
	}
}

func (t *Interface) sendRoutine() {
	defer t.wg.Done()

	for buffer := range t.outputChannel {
		content := buffer.GetBuffer()

		if content.Len() <= 0 {
			buffer.Release()
			continue
		}

		_, err := t.file.Write(content.Bytes())
		buffer.Release()

		if err != nil {
			levellog.Errorf("Error while writing frame: %s", err.Error())

			if errors.Is(err, os.ErrClosed) {
				break
			}
		}
	}
}

// Post method allows to post a new reusable buffer into the output queue
func (t *Interface) Post(*pool.ReusableBuffer) error {
	// TODO is the buffer content lower than this interface MTU ?

	// TODO post in the send channel

	return nil
}

// PostDispatch method post a new dispatch to use by the interface
func (t *Interface) PostDispatch(process.Object) {
	// TODO post the dispatch into a dedicated channel

	// TODO that channel must be read by the process channel ? don't think it is possible...
}

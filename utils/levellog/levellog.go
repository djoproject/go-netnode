package levellog

import (
	"io"
	"log"
)

const (
	// CriticalLevel only prints Critical messages
	CriticalLevel = 50
	// ErrorLevel prints Error and Critical messages
	ErrorLevel = 40
	// WarningLevel prints Error, Critical, and Warning messages
	WarningLevel = 30
	// InfoLevel prints Error, Critical, Warning, and Info messages
	InfoLevel = 20
	// DebugLevel prints every messages
	DebugLevel = 10
)

var (
	level = InfoLevel
	// CriticalLogger is the logger for Critical messages
	CriticalLogger *log.Logger
	// ErrorLogger is the logger for Error messages
	ErrorLogger *log.Logger
	// WarningLogger is the logger for Warning messages
	WarningLogger *log.Logger
	// InfoLogger is the logger for Info messages
	InfoLogger *log.Logger
	// DebugLogger is the logger for Debug messages
	DebugLogger *log.Logger
)

// TODO FIXME Flag Lshortfile does not work, it always print levellog.go ...

func init() {
	CriticalLogger = log.New(log.Writer(), "CRITICAL: ", log.Ldate|log.Ltime)
	ErrorLogger = log.New(log.Writer(), "ERROR: ", log.Ldate|log.Ltime)
	WarningLogger = log.New(log.Writer(), "WARNING: ", log.Ldate|log.Ltime)
	InfoLogger = log.New(log.Writer(), "INFO: ", log.Ldate|log.Ltime)
	DebugLogger = log.New(log.Writer(), "DEBUG: ", log.Ldate|log.Ltime)
}

// SetOutput function allows to set output writter for loggers
func SetOutput(w io.Writer) {
	log.SetOutput(w)
	CriticalLogger.SetOutput(log.Writer())
	ErrorLogger.SetOutput(log.Writer())
	WarningLogger.SetOutput(log.Writer())
	InfoLogger.SetOutput(log.Writer())
	DebugLogger.SetOutput(log.Writer())
}

// SetLevel function allows to set logger level
func SetLevel(l int) {
	level = l
}

// Critical method prints message with critical level
func Critical(v ...interface{}) {
	if level > 50 {
		return
	}

	CriticalLogger.Print(v...)
}

// Criticalf method prints message with critical level
func Criticalf(format string, v ...interface{}) {
	if level > 50 {
		return
	}

	CriticalLogger.Printf(format, v...)
}

// Criticalln method prints message with critical level
func Criticalln(v ...interface{}) {
	if level > 50 {
		return
	}

	CriticalLogger.Println(v...)
}

// Error method prints message with error level
func Error(v ...interface{}) {
	if level > 40 {
		return
	}

	ErrorLogger.Print(v...)
}

// Errorf method prints message with error level
func Errorf(format string, v ...interface{}) {
	if level > 40 {
		return
	}

	ErrorLogger.Printf(format, v...)
}

// Errorln method prints message with error level
func Errorln(v ...interface{}) {
	if level > 40 {
		return
	}

	ErrorLogger.Println(v...)
}

// Warning method prints message with warning level
func Warning(v ...interface{}) {
	if level > 30 {
		return
	}

	WarningLogger.Print(v...)
}

// Warningf method prints message with warning level
func Warningf(format string, v ...interface{}) {
	if level > 30 {
		return
	}

	WarningLogger.Printf(format, v...)
}

// Warningln method prints message with warning level
func Warningln(v ...interface{}) {
	if level > 30 {
		return
	}

	WarningLogger.Println(v...)
}

// Info method prints message with info level
func Info(v ...interface{}) {
	if level > 20 {
		return
	}

	InfoLogger.Print(v...)
}

// Infof method prints message with info level
func Infof(format string, v ...interface{}) {
	if level > 20 {
		return
	}

	InfoLogger.Printf(format, v...)
}

// Infoln method prints message with info level
func Infoln(v ...interface{}) {
	if level > 20 {
		return
	}

	InfoLogger.Println(v...)
}

// IsDebug function returns true if level is debug
func IsDebug() bool {
	return level > 10
}

// Debug method prints message with debug level
func Debug(v ...interface{}) {
	if level > 10 {
		return
	}

	DebugLogger.Print(v...)
}

// Debugf method prints message with debug level
func Debugf(format string, v ...interface{}) {
	if level > 10 {
		return
	}

	DebugLogger.Printf(format, v...)
}

// Debugln method prints message with debug level
func Debugln(v ...interface{}) {
	if level > 10 {
		return
	}

	DebugLogger.Println(v...)
}

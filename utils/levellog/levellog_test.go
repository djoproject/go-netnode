package levellog_test

import (
	"bytes"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-netnode/utils/levellog"
)

func TestCritical(t *testing.T) {
	levellog.SetLevel(60)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Critical("Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.CriticalLevel)
	levellog.Critical("Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

func TestCriticalf(t *testing.T) {
	levellog.SetLevel(60)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Criticalf("%s%d", "Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.CriticalLevel)
	levellog.Criticalf("%s%d", "Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

func TestCriticalln(t *testing.T) {
	levellog.SetLevel(60)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Criticalln("Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.CriticalLevel)
	levellog.Criticalln("Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello 55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

// ---

func TestError(t *testing.T) {
	levellog.SetLevel(levellog.CriticalLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Error("Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.ErrorLevel)
	levellog.Error("Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

func TestErrorf(t *testing.T) {
	levellog.SetLevel(levellog.CriticalLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Errorf("%s%d", "Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.ErrorLevel)
	levellog.Errorf("%s%d", "Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

func TestErrorln(t *testing.T) {
	levellog.SetLevel(levellog.CriticalLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Errorln("Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.ErrorLevel)
	levellog.Errorln("Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello 55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

// ---

func TestWarning(t *testing.T) {
	levellog.SetLevel(levellog.ErrorLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Warning("Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.WarningLevel)
	levellog.Warning("Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

func TestWarningf(t *testing.T) {
	levellog.SetLevel(levellog.ErrorLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Warningf("%s%d", "Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.WarningLevel)
	levellog.Warningf("%s%d", "Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

func TestWarningln(t *testing.T) {
	levellog.SetLevel(levellog.ErrorLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Warningln("Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.WarningLevel)
	levellog.Warningln("Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello 55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

// ---

func TestInfo(t *testing.T) {
	levellog.SetLevel(levellog.WarningLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Info("Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.InfoLevel)
	levellog.Info("Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

func TestInfof(t *testing.T) {
	levellog.SetLevel(levellog.WarningLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Infof("%s%d", "Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.InfoLevel)
	levellog.Infof("%s%d", "Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

func TestInfoln(t *testing.T) {
	levellog.SetLevel(levellog.WarningLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Infoln("Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.InfoLevel)
	levellog.Infoln("Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello 55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

// ---

func TestDebug(t *testing.T) {
	levellog.SetLevel(levellog.InfoLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Debug("Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.DebugLevel)
	levellog.Debug("Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

func TestDebugf(t *testing.T) {
	levellog.SetLevel(levellog.InfoLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Debugf("%s%d", "Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.DebugLevel)
	levellog.Debugf("%s%d", "Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}

func TestDebugln(t *testing.T) {
	levellog.SetLevel(levellog.InfoLevel)

	var buf bytes.Buffer
	levellog.SetOutput(&buf)
	levellog.Debugln("Hello", 55)
	assert.Empty(t, buf.String())

	levellog.SetLevel(levellog.DebugLevel)
	levellog.Debugln("Hello", 55)
	assert.True(t, strings.HasSuffix(buf.String(), "Hello 55\n"), buf.String())
	levellog.SetOutput(os.Stderr)
}
